openapi: 3.0.1
info:
  title: Smart Energy Management OpenAPI Service
  description: House Management Service
  version: 1.0.0
servers:
- url: http://house-management:8080
  description: Docker network server
- url: http://localhost:8081
  description: Localhost network server
tags:
- name: HouseManagementService
  description: API for house management
paths:
  /house-management/houses:
    put:
      tags:
      - HouseManagementService
      summary: update house.
      description: updates house.
      operationId: updateNewHouse
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/HouseUserDetailDTO'
        required: true
      responses:
        "200":
          description: house updated
          content:
            '*/*':
              schema:
                $ref: '#/components/schemas/HouseUserDetailDTO'
        "400":
          description: house update validation failed.
          content:
            '*/*':
              schema:
                $ref: '#/components/schemas/ApiError'
        "401":
          description: Unauthorized - access token not provided or not valid
        "403":
          description: Forbidden - access token does not have scope test_write
        "404":
          description: house not found
          content:
            '*/*':
              schema:
                $ref: '#/components/schemas/ApiError'
      security:
      - MUNI:
        - test_write
      - Bearer:
        - test_write
    post:
      tags:
      - HouseManagementService
      summary: register new house
      description: registers new house
      operationId: registerNewHouse
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/HouseUserDetailDTO'
        required: true
      responses:
        "201":
          description: house registered
          content:
            '*/*':
              schema:
                oneOf:
                - $ref: '#/components/schemas/HouseDetailDTO'
                - $ref: '#/components/schemas/HouseUserDetailDTO'
        "401":
          description: Unauthorized - access token not provided or not valid
        "403":
          description: Forbidden - access token does not have scope test_write
        "400":
          description: request validation failed
          content:
            '*/*':
              schema:
                $ref: '#/components/schemas/ApiError'
      security:
      - MUNI:
        - test_write
      - Bearer:
        - test_write
  /house-management/houses/{houseId}:
    get:
      tags:
      - HouseManagementService
      summary: returns house
      description: looks up a house by its id.
      operationId: findById
      parameters:
      - name: houseId
        in: path
        required: true
        schema:
          type: string
      responses:
        "200":
          description: "OK, house found"
          content:
            '*/*':
              schema:
                oneOf:
                - $ref: '#/components/schemas/HouseDTO'
                - $ref: '#/components/schemas/HouseDetailDTO'
        "401":
          description: Unauthorized - access token not provided or not valid
        "403":
          description: Forbidden - access token does not have scope test_read
        "404":
          description: house not found
          content:
            '*/*':
              schema:
                $ref: '#/components/schemas/ApiError'
      security:
      - MUNI:
        - test_read
      - Bearer:
        - test_read
    delete:
      tags:
      - HouseManagementService
      summary: unregister house.
      description: unregisters house with corresponding id
      operationId: unregisterHouse
      parameters:
      - name: houseId
        in: path
        required: true
        schema:
          type: string
      responses:
        "204":
          description: house successfully unregistered.
        "401":
          description: Unauthorized - access token not provided or not valid
        "403":
          description: Forbidden - access token does not have scope test_1
        "404":
          description: house not found
          content:
            '*/*':
              schema:
                $ref: '#/components/schemas/ApiError'
      security:
      - MUNI:
        - test_1
      - Bearer:
        - test_1
  /house-management/houses/{houseId}/togglePower:
    get:
      tags:
      - HouseManagementService
      summary: shutdown power
      description: shutdown all smart meters in the house
      operationId: togglePower
      parameters:
      - name: houseId
        in: path
        required: true
        schema:
          type: string
      - name: isPowerOn
        in: query
        required: true
        schema:
          type: boolean
      responses:
        "200":
          description: "OK, house found"
        "401":
          description: Unauthorized - access token not provided or not valid
        "403":
          description: Forbidden - access token does not have scope test_2
        "404":
          description: house not found/smart meter not not found
          content:
            '*/*':
              schema:
                $ref: '#/components/schemas/ApiError'
      security:
      - MUNI:
        - test_2
      - Bearer:
        - test_2
  /house-management/houses/list/{userId}:
    get:
      tags:
      - HouseManagementService
      summary: list all houses
      description: lists all houses
      operationId: listAllHouses
      parameters:
      - name: userId
        in: path
        required: true
        schema:
          type: string
      responses:
        "201":
          description: houses listed
          content:
            '*/*':
              schema:
                type: array
                items:
                  oneOf:
                  - $ref: '#/components/schemas/HouseDetailDTO'
                  - $ref: '#/components/schemas/HouseUserDetailDTO'
        "401":
          description: Unauthorized - access token not provided or not valid
        "403":
          description: Forbidden - access token does not have scope test_read
      security:
      - MUNI:
        - test_read
      - Bearer:
        - test_read
components:
  schemas:
    ApiError:
      type: object
      properties:
        timestamp:
          type: string
          format: date-time
        status:
          type: string
          enum:
          - 100 CONTINUE
          - 101 SWITCHING_PROTOCOLS
          - 102 PROCESSING
          - 103 EARLY_HINTS
          - 103 CHECKPOINT
          - 200 OK
          - 201 CREATED
          - 202 ACCEPTED
          - 203 NON_AUTHORITATIVE_INFORMATION
          - 204 NO_CONTENT
          - 205 RESET_CONTENT
          - 206 PARTIAL_CONTENT
          - 207 MULTI_STATUS
          - 208 ALREADY_REPORTED
          - 226 IM_USED
          - 300 MULTIPLE_CHOICES
          - 301 MOVED_PERMANENTLY
          - 302 FOUND
          - 302 MOVED_TEMPORARILY
          - 303 SEE_OTHER
          - 304 NOT_MODIFIED
          - 305 USE_PROXY
          - 307 TEMPORARY_REDIRECT
          - 308 PERMANENT_REDIRECT
          - 400 BAD_REQUEST
          - 401 UNAUTHORIZED
          - 402 PAYMENT_REQUIRED
          - 403 FORBIDDEN
          - 404 NOT_FOUND
          - 405 METHOD_NOT_ALLOWED
          - 406 NOT_ACCEPTABLE
          - 407 PROXY_AUTHENTICATION_REQUIRED
          - 408 REQUEST_TIMEOUT
          - 409 CONFLICT
          - 410 GONE
          - 411 LENGTH_REQUIRED
          - 412 PRECONDITION_FAILED
          - 413 PAYLOAD_TOO_LARGE
          - 413 REQUEST_ENTITY_TOO_LARGE
          - 414 URI_TOO_LONG
          - 414 REQUEST_URI_TOO_LONG
          - 415 UNSUPPORTED_MEDIA_TYPE
          - 416 REQUESTED_RANGE_NOT_SATISFIABLE
          - 417 EXPECTATION_FAILED
          - 418 I_AM_A_TEAPOT
          - 419 INSUFFICIENT_SPACE_ON_RESOURCE
          - 420 METHOD_FAILURE
          - 421 DESTINATION_LOCKED
          - 422 UNPROCESSABLE_ENTITY
          - 423 LOCKED
          - 424 FAILED_DEPENDENCY
          - 425 TOO_EARLY
          - 426 UPGRADE_REQUIRED
          - 428 PRECONDITION_REQUIRED
          - 429 TOO_MANY_REQUESTS
          - 431 REQUEST_HEADER_FIELDS_TOO_LARGE
          - 451 UNAVAILABLE_FOR_LEGAL_REASONS
          - 500 INTERNAL_SERVER_ERROR
          - 501 NOT_IMPLEMENTED
          - 502 BAD_GATEWAY
          - 503 SERVICE_UNAVAILABLE
          - 504 GATEWAY_TIMEOUT
          - 505 HTTP_VERSION_NOT_SUPPORTED
          - 506 VARIANT_ALSO_NEGOTIATES
          - 507 INSUFFICIENT_STORAGE
          - 508 LOOP_DETECTED
          - 509 BANDWIDTH_LIMIT_EXCEEDED
          - 510 NOT_EXTENDED
          - 511 NETWORK_AUTHENTICATION_REQUIRED
        message:
          type: string
        path:
          type: string
    HouseUserDetailDTO:
      required:
      - city
      - country
      - houseId
      - postalCode
      - street
      - userId
      type: object
      allOf:
      - $ref: '#/components/schemas/HouseDetailDTO'
      - type: object
        properties:
          userId:
            type: string
            description: ID of a user to whom the house belongs
            example: c2b15c16-1f83-4151-bf2c-54952ad5c7db
    HouseDetailDTO:
      required:
      - city
      - country
      - houseId
      - postalCode
      - street
      type: object
      discriminator:
        propertyName: houseType
      allOf:
      - $ref: '#/components/schemas/HouseDTO'
      - type: object
        properties:
          houseId:
            type: string
            description: House id
            example: c2b15c16-1f83-4151-bf2c-54952ad5c7db
    HouseDTO:
      title: House DTO object
      required:
      - city
      - country
      - postalCode
      - street
      type: object
      properties:
        country:
          type: string
          description: country
          example: Czech Republic
        city:
          type: string
          description: city
          example: Brno
        street:
          type: string
          description: street
          example: Botanicka
        postalCode:
          type: string
          description: postalCode
          example: "45315"
      description: Object that represents basic house information
      discriminator:
        propertyName: houseType
        mapping:
          cz.muni.fi.pa165.epsilon.api.HouseDetailDTO: '#/components/schemas/HouseDetailDTO'
          cz.muni.fi.pa165.epsilon.api.HouseUserDetailDTO: '#/components/schemas/HouseUserDetailDTO'
  securitySchemes:
    Bearer:
      type: http
      description: Provide a valid access token
      scheme: bearer
