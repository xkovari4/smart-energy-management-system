package cz.muni.fi.pa165.epsilon.housemanagement.api;

import io.swagger.v3.oas.annotations.media.DiscriminatorMapping;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Objects;

@Getter
@Setter
@ToString
@Schema(
        title = "House DTO object",
        description = "Object that represents basic house information",
        subTypes = HouseDetailDTO.class,
        discriminatorProperty = "houseType",
        discriminatorMapping = {
                @DiscriminatorMapping(
                        value = "cz.muni.fi.pa165.epsilon.api.HouseDetailDTO",
                        schema = HouseDetailDTO.class
                ),
                @DiscriminatorMapping(
                        value = "cz.muni.fi.pa165.epsilon.api.HouseUserDetailDTO"   ,
                        schema = HouseUserDetailDTO.class
                )
        }
)
public class HouseDTO {

    @NotBlank
    @Schema(description = "country", example = "Czech Republic")
    private String country;

    @NotBlank
    @Schema(description = "city", example = "Brno")
    private String city;

    @NotBlank
    @Schema(description = "street", example = "Botanicka")
    private String street;

    @NotBlank
    @Schema(description = "postalCode", example = "45315")
    private String postalCode;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HouseDTO houseDTO = (HouseDTO) o;
        return Objects.equals(country, houseDTO.country) && Objects.equals(city, houseDTO.city) && Objects.equals(street, houseDTO.street) && Objects.equals(postalCode, houseDTO.postalCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(country, city, street, postalCode);
    }
}
