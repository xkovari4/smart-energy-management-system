package cz.muni.fi.pa165.epsilon.housemanagement.api;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Objects;

@Getter
@Setter
@ToString
@Schema(discriminatorProperty = "houseType", subTypes = HouseUserDetailDTO.class)
public class HouseDetailDTO extends HouseDTO {

    @NotBlank
    @Schema(description = "House id", example = "c2b15c16-1f83-4151-bf2c-54952ad5c7db")
    private String houseId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        HouseDetailDTO that = (HouseDetailDTO) o;
        return Objects.equals(houseId, that.houseId) && super.equals(that);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), houseId);
    }
}
