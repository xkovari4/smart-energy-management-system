package cz.muni.fi.pa165.epsilon.housemanagement.api;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Schema
public class HouseUserDetailDTO extends HouseDetailDTO {

    @NotBlank
    @Schema(description = "ID of a user to whom the house belongs", example = "c2b15c16-1f83-4151-bf2c-54952ad5c7db")
    private String userId;
}
