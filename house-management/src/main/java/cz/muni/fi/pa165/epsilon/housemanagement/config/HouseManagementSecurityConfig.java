package cz.muni.fi.pa165.epsilon.housemanagement.config;

import cz.muni.fi.pa165.epsilon.housemanagement.HouseManagementApplication;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springdoc.core.customizers.OpenApiCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class HouseManagementSecurityConfig {

    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests(x -> x
                        .requestMatchers(HttpMethod.PUT, "/house-management/houses").hasAuthority("SCOPE_test_write")
                        .requestMatchers(HttpMethod.POST, "/house-management/houses/{userId}").hasAuthority("SCOPE_test_write")
                        .requestMatchers(HttpMethod.GET, "/house-management/houses/{houseId}").hasAuthority("SCOPE_test_read")
                        .requestMatchers(HttpMethod.DELETE, "/house-management/houses/{houseId}").hasAuthority("SCOPE_test_1")
                        .requestMatchers(HttpMethod.GET, "/house-management/houses/{houseId}/togglePower").hasAuthority("SCOPE_test_2")
                        .requestMatchers(HttpMethod.GET, "/house-management/houses/list/{userId}").hasAuthority("SCOPE_test_read")
                        .anyRequest().permitAll()
                )
                .oauth2ResourceServer(oauth2 -> oauth2.opaqueToken(Customizer.withDefaults()))
                .csrf().disable();
        return http.build();
    }

    @Bean
    public OpenApiCustomizer openAPICustomizer() {
        return openApi ->
                openApi.getComponents()
                        .addSecuritySchemes(HouseManagementApplication.SECURITY_SCHEME_BEARER,
                                new SecurityScheme()
                                        .type(SecurityScheme.Type.HTTP)
                                        .scheme("bearer")
                                        .description("Provide a valid access token")
                        );

    }
}