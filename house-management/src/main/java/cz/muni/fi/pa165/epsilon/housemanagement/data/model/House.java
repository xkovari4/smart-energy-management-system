package cz.muni.fi.pa165.epsilon.housemanagement.data.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Objects;

@Getter
@Setter
@ToString
@Entity
@Table(name = "house")
public class House implements Serializable {

    @Column(nullable = false)
    private String userId;
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(nullable = false)
    private String houseId;
    @Column(nullable = false)
    private String country;
    @Column(nullable = false)
    private String city;
    @Column(nullable = false)
    private String street;
    @Column(nullable = false)
    private String postalCode;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof House house)) {
            return false;
        }
        return Objects.equals(getHouseId(), house.getHouseId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getHouseId());
    }
}

