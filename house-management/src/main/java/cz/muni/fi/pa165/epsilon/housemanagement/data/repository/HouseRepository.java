package cz.muni.fi.pa165.epsilon.housemanagement.data.repository;

import cz.muni.fi.pa165.epsilon.housemanagement.data.model.House;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HouseRepository extends CrudRepository<House, String> {

    @Query("SELECT house FROM House house WHERE house.userId = ?1")
    List<House> listAllHouses(String userId);
}
