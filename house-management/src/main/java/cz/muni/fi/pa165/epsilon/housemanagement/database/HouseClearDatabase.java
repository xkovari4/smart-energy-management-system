package cz.muni.fi.pa165.epsilon.housemanagement.database;

import cz.muni.fi.pa165.epsilon.housemanagement.data.repository.HouseRepository;
import jakarta.annotation.PostConstruct;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Transactional
@AllArgsConstructor
@Profile("database-clear")
public class HouseClearDatabase {
    private final HouseRepository houseRepository;

    @PostConstruct
    public void init() {
        System.out.println("Initializing Database Clearer...");
        clearDatabase();
    }

    private void clearDatabase() {
        houseRepository.deleteAll();
        System.out.println("Database cleared");
    }
}
