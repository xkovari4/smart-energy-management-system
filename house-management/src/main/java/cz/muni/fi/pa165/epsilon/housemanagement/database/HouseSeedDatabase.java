package cz.muni.fi.pa165.epsilon.housemanagement.database;

import com.github.javafaker.Faker;
import cz.muni.fi.pa165.epsilon.housemanagement.data.model.House;
import cz.muni.fi.pa165.epsilon.housemanagement.data.repository.HouseRepository;
import jakarta.annotation.PostConstruct;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Transactional
@AllArgsConstructor
@Profile("database-start")
public class HouseSeedDatabase {

    private final HouseRepository houseRepository;
    private final Faker faker = new Faker();

    @PostConstruct
    public void init() {
        System.out.println("Initializing Database Seeder...");
        if (houseRepository.count() == 0) {
            seedHouse();
        } else {
            System.out.println("House database already seeded");
        }
    }

    private void seedHouse() {
        for (int i = 0; i < 100; i++) {
            House house = new House();
            house.setUserId(String.valueOf(i));
            house.setCountry(faker.country().name());
            house.setCity(faker.address().cityName());
            house.setStreet(faker.address().streetAddress());
            house.setPostalCode(faker.address().zipCode());
            houseRepository.save(house);
        }
        System.out.println("Database seeded");
    }
}
