package cz.muni.fi.pa165.epsilon.housemanagement.exceptions;

public class ValidationException extends RuntimeException {
    public ValidationException(String message) {
        super(message);
    }
}
