package cz.muni.fi.pa165.epsilon.housemanagement.facade;

import cz.muni.fi.pa165.epsilon.housemanagement.api.HouseDTO;
import cz.muni.fi.pa165.epsilon.housemanagement.api.HouseDetailDTO;
import cz.muni.fi.pa165.epsilon.housemanagement.api.HouseUserDetailDTO;
import cz.muni.fi.pa165.epsilon.housemanagement.mappers.HouseMapper;
import cz.muni.fi.pa165.epsilon.housemanagement.service.HouseManagementService;
import cz.muni.fi.pa165.epsilon.housemanagement.service.smartMeterApi.SmartMeterApiService;
import cz.muni.fi.pa165.epsilon.housemanagement.service.smartMeterApi.UserApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class HouseFacade {

    private final HouseManagementService houseManagementService;
    private final HouseMapper houseMapper;
    private final SmartMeterApiService smartMeterApiService;
    private final UserApiService userApiService;

    @Autowired
    public HouseFacade(HouseManagementService houseManagementService, HouseMapper houseMapper, SmartMeterApiService smartMeterApiService, UserApiService userApiService) {
        this.houseManagementService = houseManagementService;
        this.houseMapper = houseMapper;
        this.smartMeterApiService = smartMeterApiService;
        this.userApiService = userApiService;
    }

    @Transactional(readOnly = true)
    public HouseDTO findById(String houseId) {
        return houseMapper.mapToDto(houseManagementService.findById(houseId));
    }

    @Transactional(readOnly = true)
    public List<HouseDetailDTO> listAllHouses(String userId) {
        return houseMapper.mapToHouseDetailDtoList(houseManagementService.listAllHouses(userId));
    }

    public HouseUserDetailDTO registerNewHouse(HouseUserDetailDTO houseUserDetailDTO) {
        userApiService.checkUserExists(houseUserDetailDTO.getUserId());
        var house = houseMapper.map(houseUserDetailDTO);
        return houseMapper.mapToHouseUserDetailDto(houseManagementService.registerNewHouse(house));
    }

    public HouseDetailDTO unregisterHouse(String houseId) {
        return houseMapper.mapToHouseDetailDto(houseManagementService.deleteHouse(houseId));
    }

    public HouseUserDetailDTO updateHouse(HouseUserDetailDTO houseUserDetailDTO) {
        var house = houseMapper.map(houseUserDetailDTO);
        return houseMapper.mapToHouseUserDetailDto(houseManagementService.updateHouse(house));
    }

    public void updateHouseMainPowerSwitch(String houseId, boolean isPowerOn) {
        smartMeterApiService.updateHouseMainPowerSwitch(houseId, isPowerOn);
    }
}
