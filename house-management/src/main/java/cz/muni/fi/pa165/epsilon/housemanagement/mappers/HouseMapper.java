package cz.muni.fi.pa165.epsilon.housemanagement.mappers;

import cz.muni.fi.pa165.epsilon.housemanagement.api.HouseDTO;
import cz.muni.fi.pa165.epsilon.housemanagement.api.HouseDetailDTO;
import cz.muni.fi.pa165.epsilon.housemanagement.api.HouseUserDetailDTO;
import cz.muni.fi.pa165.epsilon.housemanagement.data.model.House;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Named;

import java.util.List;

@Mapper(componentModel = "spring")
public interface HouseMapper {

    House map(HouseUserDetailDTO houseUserDetailDTO);

    HouseDTO mapToDto(House house);

    @Named(value = "useMe")
    HouseDetailDTO mapToHouseDetailDto(House house);

    @IterableMapping(qualifiedByName = "useMe")
    List<HouseDetailDTO> mapToHouseDetailDtoList(List<House> houses);

    HouseUserDetailDTO mapToHouseUserDetailDto(House house);
}
