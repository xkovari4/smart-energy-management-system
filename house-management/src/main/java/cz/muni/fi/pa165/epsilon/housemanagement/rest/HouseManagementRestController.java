package cz.muni.fi.pa165.epsilon.housemanagement.rest;

import cz.muni.fi.pa165.epsilon.housemanagement.HouseManagementApplication;
import cz.muni.fi.pa165.epsilon.housemanagement.api.HouseDTO;
import cz.muni.fi.pa165.epsilon.housemanagement.api.HouseDetailDTO;
import cz.muni.fi.pa165.epsilon.housemanagement.api.HouseUserDetailDTO;
import cz.muni.fi.pa165.epsilon.housemanagement.exceptions.ValidationException;
import cz.muni.fi.pa165.epsilon.housemanagement.facade.HouseFacade;
import cz.muni.fi.pa165.epsilon.housemanagement.rest.exceptionhandling.ApiError;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RestController
@Tag(name = "HouseManagementService", description = "API for house management")
@OpenAPIDefinition(
        info = @Info(
                title = "Smart Energy Management OpenAPI Service",
                description = "House Management Service",
                version = "1.0.0"
        ),
        servers = {@Server(url = "http://house-management:8080", description = "Docker network server"),
                @Server(url = "http://localhost:8081", description = "Localhost network server")}
)
@RequestMapping(path = "/house-management")
public class HouseManagementRestController {

    private final HouseFacade houseFacade;

    @Autowired
    public HouseManagementRestController(HouseFacade houseFacade) {
        this.houseFacade = houseFacade;
    }

    @Operation(
            summary = "returns house",
            description = "looks up a house by its id.",
            security = {@SecurityRequirement(name = HouseManagementApplication.SECURITY_SCHEME_OAUTH2, scopes = {"test_read"}),
                    @SecurityRequirement(name = HouseManagementApplication.SECURITY_SCHEME_BEARER, scopes = {"test_read"})},
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK, house found"),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_read", content = @Content()),
                    @ApiResponse(responseCode = "404", description = "house not found",
                            content = @Content(schema = @Schema(implementation = ApiError.class)))
            }
    )
    @GetMapping(path = "/houses/{houseId}")
    @CrossOrigin(origins = "*")
    @Transactional(readOnly = true)
    public ResponseEntity<HouseDTO> findById(@PathVariable("houseId") String id) {
        return ResponseEntity.ok(houseFacade.findById(id));
    }

    @Operation(
            summary = "list all houses",
            description = "lists all houses",
            security = {@SecurityRequirement(name = HouseManagementApplication.SECURITY_SCHEME_OAUTH2, scopes = {"test_read"}),
                    @SecurityRequirement(name = HouseManagementApplication.SECURITY_SCHEME_BEARER, scopes = {"test_read"})},
            responses = {
                    @ApiResponse(responseCode = "201", description = "houses listed"),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_read", content = @Content()),
            }
    )
    @GetMapping(path = "/houses/list/{userId}")
    @CrossOrigin(origins = "*")
    @Transactional(readOnly = true)
    public ResponseEntity<List<HouseDetailDTO>> listAllHouses(@PathVariable("userId") String id) {
        return ResponseEntity.ok(houseFacade.listAllHouses(id));
    }

    @Operation(
            summary = "register new house",
            description = "registers new house",
            security = {@SecurityRequirement(name = HouseManagementApplication.SECURITY_SCHEME_OAUTH2, scopes = {"test_write"}),
                    @SecurityRequirement(name = HouseManagementApplication.SECURITY_SCHEME_BEARER, scopes = {"test_write"})},
            responses = {
                    @ApiResponse(responseCode = "201", description = "house registered"),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_write", content = @Content()),
                    @ApiResponse(responseCode = "400", description = "request validation failed",
                            content = @Content(schema = @Schema(implementation = ApiError.class)))
            }
    )
    @PostMapping(path = "/houses")
    @CrossOrigin(origins = "*")
    @ResponseStatus(HttpStatus.CREATED)
    @Transactional
    public ResponseEntity<HouseDetailDTO> registerNewHouse(
            @Valid @RequestBody HouseUserDetailDTO requestDto,
            BindingResult bindingResult
    ) {
        checkFieldError(bindingResult);
        var house = houseFacade.registerNewHouse(requestDto);
        return new ResponseEntity<>(house, HttpStatus.CREATED);
    }

    @Operation(
            summary = "unregister house.",
            description = "unregisters house with corresponding id",
            security = {@SecurityRequirement(name = HouseManagementApplication.SECURITY_SCHEME_OAUTH2, scopes = {"test_1"}),
                    @SecurityRequirement(name = HouseManagementApplication.SECURITY_SCHEME_BEARER, scopes = {"test_1"})},
            responses = {
                    @ApiResponse(responseCode = "204", description = "house successfully unregistered."),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_1", content = @Content()),
                    @ApiResponse(responseCode = "404", description = "house not found",
                            content = @Content(schema = @Schema(implementation = ApiError.class)))
            }
    )
    @DeleteMapping(path = "/houses/{houseId}")
    @CrossOrigin(origins = "*")
    @Transactional
    public ResponseEntity<Void> unregisterHouse(@PathVariable("houseId") String id) {
        houseFacade.unregisterHouse(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Operation(
            summary = "shutdown power",
            description = "shutdown all smart meters in the house",
            security = {@SecurityRequirement(name = HouseManagementApplication.SECURITY_SCHEME_OAUTH2, scopes = {"test_2"}),
                    @SecurityRequirement(name = HouseManagementApplication.SECURITY_SCHEME_BEARER, scopes = {"test_2"})},
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK, house found"),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_2", content = @Content()),
                    @ApiResponse(responseCode = "404", description = "house not found/smart meter not not found",
                            content = @Content(schema = @Schema(implementation = ApiError.class))),
            }
    )
    @GetMapping(path = "/houses/{houseId}/togglePower")
    @CrossOrigin(origins = "*")
    @Transactional
    public ResponseEntity<Void> togglePower(@PathVariable("houseId") String id, @RequestParam("isPowerOn") boolean isPowerOn) {
        houseFacade.updateHouseMainPowerSwitch(id, isPowerOn);
        return ResponseEntity.ok().build();
    }


    @Operation(
            summary = "update house.",
            description = "updates house.",
            security = {@SecurityRequirement(name = HouseManagementApplication.SECURITY_SCHEME_OAUTH2, scopes = {"test_write"}),
                    @SecurityRequirement(name = HouseManagementApplication.SECURITY_SCHEME_BEARER, scopes = {"test_write"})},
            responses = {
                    @ApiResponse(responseCode = "200", description = "house updated"),
                    @ApiResponse(responseCode = "400", description = "house update validation failed.",
                            content = @Content(schema = @Schema(implementation = ApiError.class))),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_write", content = @Content()),
                    @ApiResponse(responseCode = "404", description = "house not found",
                            content = @Content(schema = @Schema(implementation = ApiError.class)))
            }
    )
    @PutMapping(path = "/houses")
    @CrossOrigin(origins = "*")
    @Transactional
    public ResponseEntity<HouseUserDetailDTO> updateNewHouse(
            @Valid @RequestBody HouseUserDetailDTO requestDto,
            BindingResult bindingResult
    ) {
        checkFieldError(bindingResult);
        var house = houseFacade.updateHouse(requestDto);
        return ResponseEntity.ok(house);
    }

    private void checkFieldError(BindingResult bindingResult) throws ValidationException {
        if (bindingResult.hasErrors()) {
            for (FieldError fe : bindingResult.getFieldErrors()) {
                throw new ValidationException(String.format("FieldError: %s; %s", fe.getField(), fe.getDefaultMessage()));
            }
        }
    }
}
