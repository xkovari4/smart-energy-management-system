package cz.muni.fi.pa165.epsilon.housemanagement.rest;

import io.swagger.v3.oas.annotations.media.DiscriminatorMapping;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Schema(
        title = "register new house",
        description = "object for registering new houses",
        subTypes = UpdateHouseRequest.class,
        discriminatorProperty = "requestType",
        discriminatorMapping = {
                @DiscriminatorMapping(
                        value = "cz.muni.fi.pa165.epsilon.rest.UpdateHouseRequest",
                        schema = UpdateHouseRequest.class),
        }
)

@Getter
@Setter
@ToString
@SuppressWarnings("unused")
public class RegisterNewHouseRequest {

    @NotBlank
    @Schema(description = "ID of a user to whom the house belongs", example = "c2b15c16-1f83-4151-bf2c-54952ad5c7db")
    private String userId;
    @NotBlank
    @Schema(description = "Country where the house is located", example = "Czech Republic")
    private String country;

    @NotBlank
    @Schema(description = "City in which the house is located", example = "Brno")
    private String city;

    @NotBlank
    @Schema(description = "Street name", example = "Botanicka")
    private String street;

    @NotBlank
    @Schema(description = "Postal code", example = "48931")
    private String postalCode;
}
