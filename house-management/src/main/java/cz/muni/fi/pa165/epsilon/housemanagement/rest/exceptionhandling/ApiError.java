package cz.muni.fi.pa165.epsilon.housemanagement.rest.exceptionhandling;

import lombok.*;
import org.springframework.http.HttpStatus;

import java.time.Clock;
import java.time.LocalDateTime;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ApiError {

    private LocalDateTime timestamp;
    private HttpStatus status;
    private String message;
    private String path;

    public ApiError(HttpStatus status, String message, String path) {
        this.timestamp = LocalDateTime.now(Clock.systemUTC());
        this.status = status;
        this.message = message;
        this.path = path;
    }
}
