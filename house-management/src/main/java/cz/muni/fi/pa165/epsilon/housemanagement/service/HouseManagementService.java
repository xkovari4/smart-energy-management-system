package cz.muni.fi.pa165.epsilon.housemanagement.service;

import cz.muni.fi.pa165.epsilon.housemanagement.data.model.House;
import cz.muni.fi.pa165.epsilon.housemanagement.data.repository.HouseRepository;
import cz.muni.fi.pa165.epsilon.housemanagement.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.function.Supplier;

@Service
@Transactional
public class HouseManagementService {

    private final HouseRepository houseRepository;

    @Autowired
    public HouseManagementService(HouseRepository houseRepository) {
        this.houseRepository = houseRepository;
    }

    @Transactional(readOnly = true)
    public House findById(String houseId) {
        return houseRepository.findById(houseId).orElseThrow(houseNotFound(houseId));
    }

    @Transactional(readOnly = true)
    public List<House> listAllHouses(String userId) {
        return houseRepository.listAllHouses(userId);
    }

    public House registerNewHouse(House house) {
        return houseRepository.save(house);
    }

    public House deleteHouse(String houseId) {
        var house = findById(houseId); // should throw exception if not found
        houseRepository.deleteById(houseId);
        return house;
    }

    public House updateHouse(House house) {
        try {
            findById(house.getHouseId());
        } catch (ResourceNotFoundException e) {
            throw houseNotFound(
                    house.getHouseId(),
                    house.getUserId()
            ).get();
        }
        return houseRepository.save(house);
    }

    private Supplier<ResourceNotFoundException> houseNotFound(String houseId) {
        return () -> new ResourceNotFoundException("House with houseId: " + houseId + " was not found.");
    }

    private Supplier<ResourceNotFoundException> houseNotFound(String houseId, String userId) {
        return () -> new ResourceNotFoundException("House with houseId: " + houseId + " and userId: " + userId + " was not found.");
    }
}
