package cz.muni.fi.pa165.epsilon.housemanagement.service.smartMeterApi;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.epsilon.smart_meter_management.client.SmartMeterApi;
import cz.muni.fi.pa165.epsilon.smart_meter_management.client.invoker.ApiClient;
import cz.muni.fi.pa165.epsilon.user_management.client.UserManagementServiceApi;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.net.http.HttpClient;
import java.time.Duration;

@Configuration
public class ApiConfig {

    private final ApplicationContext context;

    @Autowired
    public ApiConfig(ApplicationContext context) {
        this.context = context;
    }

    @Bean
    public SmartMeterApi SmartMeterApi() {
        HttpClient.Builder httpClientBuilder = HttpClient.newBuilder()
                .connectTimeout(Duration.ofSeconds(30))
                .version(HttpClient.Version.HTTP_2);

        ApiClient apiClient = new ApiClient(httpClientBuilder, new ObjectMapper(), null);
        apiClient.setRequestInterceptor(request -> request.header("Authorization", "Bearer " + getBearerToken()));

        return new SmartMeterApi(apiClient);
    }

    @Bean
    public UserManagementServiceApi UserManagementServiceApi() {
        HttpClient.Builder httpClientBuilder = HttpClient.newBuilder()
                .connectTimeout(Duration.ofSeconds(30))
                .version(HttpClient.Version.HTTP_2);

        var apiClient = new cz.muni.fi.pa165.epsilon.user_management.client.invoker.ApiClient(httpClientBuilder, new ObjectMapper(), null);
        apiClient.setRequestInterceptor(request -> request.header("Authorization", "Bearer " + getBearerToken()));

        return new UserManagementServiceApi(apiClient);
    }

    private String getBearerToken() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String token = request.getHeader("Authorization");
        if (token != null && token.startsWith("Bearer ")) {
            return token.substring(7);
        }
        return null;
    }
}
