package cz.muni.fi.pa165.epsilon.housemanagement.service.smartMeterApi;

import cz.muni.fi.pa165.epsilon.housemanagement.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.epsilon.housemanagement.service.HouseManagementService;
import cz.muni.fi.pa165.epsilon.smart_meter_management.client.SmartMeterApi;
import cz.muni.fi.pa165.epsilon.smart_meter_management.client.invoker.ApiException;
import cz.muni.fi.pa165.epsilon.smart_meter_management.client.model.SmartMeterDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SmartMeterApiService {

    private final SmartMeterApi smartMeterApi;
    private final HouseManagementService houseManagementService;

    @Autowired
    public SmartMeterApiService(SmartMeterApi smartMeterApi, HouseManagementService houseManagementService) {
        this.smartMeterApi = smartMeterApi;
        this.houseManagementService = houseManagementService;
    }

    @Transactional
    public void updateHouseMainPowerSwitch(String houseId, Boolean isPowerOn) {
        houseManagementService.findById(houseId); // should throw an exception if there is no house with such ID
        try {
            var smartMeterIds = smartMeterApi.listAllForHouseId(houseId).stream().map(SmartMeterDTO::getId).toList();
            for (String id : smartMeterIds) {
                if (isPowerOn) {
                    smartMeterApi.turnOnSmartMeter(id);
                } else {
                    smartMeterApi.turnOffSmartMeter(id);
                }
            }
        } catch (ApiException e) {
            throw new ResourceNotFoundException(e.getMessage());
        }
    }
}
