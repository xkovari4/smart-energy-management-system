package cz.muni.fi.pa165.epsilon.housemanagement.service.smartMeterApi;

import cz.muni.fi.pa165.epsilon.housemanagement.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.epsilon.user_management.client.UserManagementServiceApi;
import cz.muni.fi.pa165.epsilon.user_management.client.invoker.ApiException;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserApiService {

    private final UserManagementServiceApi userManagementServiceApi;

    @Autowired
    public UserApiService(UserManagementServiceApi userManagementServiceApi) {
        this.userManagementServiceApi = userManagementServiceApi;
    }

    @Transactional
    public void checkUserExists(String userId) {
        try {
            userManagementServiceApi.findById(userId);
        } catch (ApiException e) {
            throw new ResourceNotFoundException(e.getMessage());
        }
    }
}
