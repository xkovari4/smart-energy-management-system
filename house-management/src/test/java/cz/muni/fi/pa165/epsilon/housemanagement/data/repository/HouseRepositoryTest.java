package cz.muni.fi.pa165.epsilon.housemanagement.data.repository;

import cz.muni.fi.pa165.epsilon.housemanagement.data.model.House;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.List;

import static cz.muni.fi.pa165.epsilon.housemanagement.util.TestDataFactory.houseEntity;
import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class HouseRepositoryTest {

    @Autowired
    private HouseRepository houseRepository;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    void listAllHouses_housesFound_returnsHouse() {
        // Arrange
        entityManager.merge(houseEntity);

        // Act
        List<House> houses = houseRepository.listAllHouses(houseEntity.getUserId());

        // Assert
        assertThat(houses).hasSize(1);
        House h = houses.get(0);
        assertThat(h.getCountry()).isEqualTo(houseEntity.getCountry());
        assertThat(h.getCity()).isEqualTo(houseEntity.getCity());
        assertThat(h.getStreet()).isEqualTo(houseEntity.getStreet());
        assertThat(h.getPostalCode()).isEqualTo(houseEntity.getPostalCode());
        assertThat(h.getUserId()).isEqualTo(houseEntity.getUserId());
    }

    @Test
    void listAllHouses_houseExistsButNoHousesFound_returnsEmptyList() {
        // Arrange
        entityManager.merge(houseEntity);

        // Act
        List<House> houses = houseRepository.listAllHouses("nonExistingUserId");

        // Assert
        assertThat(houses).isEmpty();
    }
}
