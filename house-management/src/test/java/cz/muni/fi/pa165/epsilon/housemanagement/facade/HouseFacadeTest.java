package cz.muni.fi.pa165.epsilon.housemanagement.facade;

import cz.muni.fi.pa165.epsilon.housemanagement.api.HouseDTO;
import cz.muni.fi.pa165.epsilon.housemanagement.data.model.House;
import cz.muni.fi.pa165.epsilon.housemanagement.api.HouseDetailDTO;
import cz.muni.fi.pa165.epsilon.housemanagement.api.HouseUserDetailDTO;
import cz.muni.fi.pa165.epsilon.housemanagement.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.epsilon.housemanagement.exceptions.ValidationException;
import cz.muni.fi.pa165.epsilon.housemanagement.mappers.HouseMapper;
import cz.muni.fi.pa165.epsilon.housemanagement.service.HouseManagementService;
import cz.muni.fi.pa165.epsilon.housemanagement.service.smartMeterApi.SmartMeterApiService;
import cz.muni.fi.pa165.epsilon.housemanagement.service.smartMeterApi.UserApiService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

import static cz.muni.fi.pa165.epsilon.housemanagement.util.TestDataFactory.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class HouseFacadeTest {
    @Mock
    private HouseManagementService houseService;

    @Mock
    private HouseMapper houseMapper;

    @Mock
    private SmartMeterApiService smartMeterApiService;

    @Mock
    private UserApiService userApiService;

    @InjectMocks
    private HouseFacade houseFacade;

    @Test
    void findById_houseFound_returnHouse() {
        // Arrange
        when(houseService.findById("houseId")).thenReturn(houseEntity);
        when(houseMapper.mapToDto(any())).thenReturn(houseDTO);

        // Act
        HouseDTO foundEntity = houseFacade.findById("houseId");

        // Assert
        assertThat(foundEntity).isEqualTo(houseDTO);
        verify(houseService).findById("houseId");
        verify(houseMapper).mapToDto(houseEntity);
    }

    @Test
    void findById_houseNotFound_throwsNoSuchElementException() {
        // Arrange
        when(houseService.findById("notExistingHouseId")).thenThrow(NoSuchElementException.class);

        // Act & Assert
        assertThrows(NoSuchElementException.class, () -> houseFacade.findById("notExistingHouseId"));
    }

    @Test
    void listAllHousesForUser_houseFound_returnHouses() {
        // Arrange
        when(houseService.listAllHouses("userId")).thenReturn(houseEntities);
        when(houseMapper.mapToHouseDetailDtoList(any())).thenReturn(houseDetailDTOs);

        // Act
        List<HouseDetailDTO> foundEntities = houseFacade.listAllHouses("userId");

        // Assert
        assertThat(foundEntities).isEqualTo(houseDetailDTOs);
        verify(houseService).listAllHouses("userId");
        verify(houseMapper).mapToHouseDetailDtoList(houseEntities);
    }

    @Test
    void listAllHousesForUser_housesNotFound_returnEmptyList() {
        // Arrange
        when(houseService.listAllHouses("userId")).thenReturn(Collections.emptyList());
        when(houseMapper.mapToHouseDetailDtoList(any())).thenReturn(Collections.emptyList());

        // Act
        List<HouseDetailDTO> foundEntities = houseFacade.listAllHouses("userId");

        // Assert
        assertThat(foundEntities).isEmpty();
        verify(houseService).listAllHouses("userId");
        verify(houseMapper).mapToHouseDetailDtoList(Collections.emptyList());
    }

    @Test
    void registerNewHouse_successfulRegistration_returnsHouseUserDetailDTO() {
        // Arrange
        HouseUserDetailDTO inputDTO = new HouseUserDetailDTO();

        House houseEntity = new House();

        House registeredHouse = new House();

        HouseUserDetailDTO expectedDTO = new HouseUserDetailDTO();

        when(houseMapper.map(inputDTO)).thenReturn(houseEntity);
        when(houseService.registerNewHouse(houseEntity)).thenReturn(registeredHouse);
        when(houseMapper.mapToHouseUserDetailDto(registeredHouse)).thenReturn(expectedDTO);

        // Act
        HouseUserDetailDTO actualDTO = houseFacade.registerNewHouse(inputDTO);

        // Assert
        assertEquals(expectedDTO, actualDTO);
        verify(houseMapper).map(inputDTO);
        verify(houseService).registerNewHouse(houseEntity);
        verify(houseMapper).mapToHouseUserDetailDto(registeredHouse);
    }

    @Test
    void registerNewHouse_invalidData_throwsValidationException() {
        // Arrange
        HouseUserDetailDTO inputDTO = new HouseUserDetailDTO();

        House houseEntity = new House();

        when(houseMapper.map(inputDTO)).thenReturn(houseEntity);
        doThrow(ValidationException.class).when(houseService).registerNewHouse(houseEntity);

        // Act & Assert
        assertThrows(ValidationException.class, () -> houseFacade.registerNewHouse(inputDTO));
        verify(houseMapper).map(inputDTO);
        verify(houseService).registerNewHouse(houseEntity);
    }

    @Test
    void unregisterHouse_houseDeleted_returnsHouse() {
        // Arrange
        when(houseService.deleteHouse(houseEntity.getHouseId())).thenReturn(houseEntity);
        when(houseMapper.mapToHouseDetailDto(any())).thenReturn(houseDetailDTO);

        // Act
        HouseDetailDTO deletedEntity = houseFacade.unregisterHouse(houseEntity.getHouseId());

        // Assert
        assertThat(deletedEntity).isEqualTo(houseDetailDTO);
        verify(houseService).deleteHouse(houseEntity.getHouseId());
        verify(houseMapper).mapToHouseDetailDto(houseEntity);
    }

    @Test
    void unregisterHouse_houseNotFound_throwsNoSuchElementException() {
        // Arrange
        when(houseService.deleteHouse("notExistingHouseId")).thenThrow(NoSuchElementException.class);

        // Act & Assert
        assertThrows(NoSuchElementException.class, () -> houseFacade.unregisterHouse("notExistingHouseId"));
    }

    @Test
    void updateHouse_houseUpdate_returnsHouse() {
        // Arrange
        when(houseService.updateHouse(houseEntity)).thenReturn(houseEntity);
        when(houseMapper.mapToHouseUserDetailDto(any())).thenReturn(houseUserDetailDTO);
        when(houseMapper.map(any())).thenReturn(houseEntity);

        // Act
        HouseUserDetailDTO updatedEntity = houseFacade.updateHouse(houseUserDetailDTO);

        // Assert
        assertThat(updatedEntity).isEqualTo(houseUserDetailDTO);
        verify(houseService).updateHouse(houseEntity);
        verify(houseMapper).mapToHouseUserDetailDto(houseEntity);
        verify(houseMapper).map(houseUserDetailDTO);
    }

    @Test
    void updateHouse_invalidData_throwsValidationException() {
        // Arrange
        when(houseService.updateHouse(any())).thenThrow(ValidationException.class);

        // Act & Assert
        assertThrows(ValidationException.class, () -> houseFacade.updateHouse(houseUserDetailDTO));
    }

    @Test
    void updateHouseMainPowerSwitch_houseUpdate_updateHouseMainPowerSwitchOnSmartMeterApiServiceCalled() {
        // Act
        houseFacade.updateHouseMainPowerSwitch(houseEntity.getHouseId(), true);

        // Assert
        verify(smartMeterApiService).updateHouseMainPowerSwitch(houseEntity.getHouseId(), true);
    }

    @Test
    void updateHouseMainPowerSwitch_invalidHouseId_throwsResourceNotFoundException() {
        // Arrange
        doThrow(new ResourceNotFoundException("House not found")).when(smartMeterApiService).updateHouseMainPowerSwitch("invalidHouseId", true);

        // Act & Assert
        assertThrows(ResourceNotFoundException.class, () -> houseFacade.updateHouseMainPowerSwitch("invalidHouseId", true));
    }
}
