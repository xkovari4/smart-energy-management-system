package cz.muni.fi.pa165.epsilon.housemanagement.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.epsilon.housemanagement.api.HouseDetailDTO;
import cz.muni.fi.pa165.epsilon.housemanagement.data.model.House;
import cz.muni.fi.pa165.epsilon.housemanagement.data.repository.HouseRepository;
import cz.muni.fi.pa165.epsilon.housemanagement.rest.exceptionhandling.ApiError;
import cz.muni.fi.pa165.epsilon.housemanagement.util.ObjectConverter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.List;

import static cz.muni.fi.pa165.epsilon.housemanagement.util.TestDataFactory.houseDTO;
import static cz.muni.fi.pa165.epsilon.housemanagement.util.TestDataFactory.houseEntity;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@WithMockUser(authorities = {"SCOPE_test_write", "SCOPE_test_read", "SCOPE_test_1", "SCOPE_test_2"})
@AutoConfigureMockMvc
@Transactional
class HouseManagementIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private HouseRepository houseRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    void beforeEach() {
        houseRepository.deleteAll();
    }

    @Test
    void findById_houseFound_returnsHouseHouseDTO() throws Exception {
        // Arrange
        String houseId = houseRepository.save(houseEntity).getHouseId();

        // Act & Assert
        mockMvc.perform(get("/house-management/houses/{houseId}", houseId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.country").value(houseDTO.getCountry()))
                .andExpect(jsonPath("$.city").value(houseDTO.getCity()))
                .andExpect(jsonPath("$.street").value(houseDTO.getStreet()))
                .andExpect(jsonPath("$.postalCode").value(houseDTO.getPostalCode()));
    }

    @Test
    void findById_houseNotFound_returnsApiError() throws Exception {
        // Act & Assert
        String responseJson = mockMvc.perform(get("/house-management/houses/{id}", "nonExistingId")
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        ApiError response = ObjectConverter.convertJsonToObject(responseJson, ApiError.class);

        assertTrue(response.getTimestamp().isBefore(LocalDateTime.now()));
        assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void listAllHousesForUser_housesFound_returnsHouses() throws Exception {
        // Arrange
        String userId = "KubaTheRichMan";
        List<HouseDetailDTO> houseDetailDtos = prepareHouses(userId);

        // Act
        String responseJson = mockMvc.perform(get("/house-management/houses/list/{userId}", userId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        HouseDetailDTO[] response = objectMapper.readValue(responseJson, HouseDetailDTO[].class);

        // Assert
        assertThat(response).hasSize(3);
        assertThat(response[0]).isEqualTo(houseDetailDtos.get(0));
        assertThat(response[1]).isEqualTo(houseDetailDtos.get(1));
        assertThat(response[2]).isEqualTo(houseDetailDtos.get(2));
    }

    private List<HouseDetailDTO> prepareHouses(String userId) {
        House house1 = new House();
        house1.setUserId(userId);
        house1.setCountry("CZ");
        house1.setCity("Brno");
        house1.setStreet("Bratislavska");
        house1.setPostalCode("60200");
        String houseId = houseRepository.save(house1).getHouseId();

        HouseDetailDTO house1Dto = new HouseDetailDTO();
        house1Dto.setHouseId(houseId);
        house1Dto.setCountry("CZ");
        house1Dto.setCity("Brno");
        house1Dto.setStreet("Bratislavska");
        house1Dto.setPostalCode("60200");

        House house2 = new House();
        house2.setUserId(userId);
        house2.setCountry("CZ");
        house2.setCity("Brno");
        house2.setStreet("Cejl");
        house2.setPostalCode("60200");
        houseId = houseRepository.save(house2).getHouseId();

        HouseDetailDTO house2Dto = new HouseDetailDTO();
        house2Dto.setHouseId(houseId);
        house2Dto.setCountry("CZ");
        house2Dto.setCity("Brno");
        house2Dto.setStreet("Cejl");
        house2Dto.setPostalCode("60200");

        House house3 = new House();
        house3.setUserId(userId);
        house3.setCountry("CZ");
        house3.setCity("Brno");
        house3.setStreet("Francouzska");
        house3.setPostalCode("60200");
        houseId = houseRepository.save(house3).getHouseId();

        HouseDetailDTO house3Dto = new HouseDetailDTO();
        house3Dto.setHouseId(houseId);
        house3Dto.setCountry("CZ");
        house3Dto.setCity("Brno");
        house3Dto.setStreet("Francouzska");
        house3Dto.setPostalCode("60200");

        return List.of(house1Dto, house2Dto, house3Dto);
    }
}
