package cz.muni.fi.pa165.epsilon.housemanagement.rest;

import cz.muni.fi.pa165.epsilon.housemanagement.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.epsilon.housemanagement.facade.HouseFacade;
import cz.muni.fi.pa165.epsilon.housemanagement.mappers.HouseMapper;
import cz.muni.fi.pa165.epsilon.housemanagement.rest.exceptionhandling.ApiError;
import cz.muni.fi.pa165.epsilon.housemanagement.util.ObjectConverter;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.Collections;

import static cz.muni.fi.pa165.epsilon.housemanagement.util.TestDataFactory.*;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@WithMockUser(authorities = {"SCOPE_test_write", "SCOPE_test_read", "SCOPE_test_1", "SCOPE_test_2"})
@AutoConfigureMockMvc
class HouseManagementRestControllerWebMvcTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private HouseFacade houseFacade;

    @MockBean
    private HouseMapper houseMapper;

    @Test
    void findById_houseFound_returnsHouse() throws Exception {
        // Arrange
        String id = houseEntity.getHouseId();
        when(houseFacade.findById(id)).thenReturn(houseDTO);

        // Act & assert
        mockMvc.perform(get("/house-management/houses/{houseId}", id)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.country").value(houseDTO.getCountry()))
                .andExpect(jsonPath("$.city").value(houseDTO.getCity()))
                .andExpect(jsonPath("$.street").value(houseDTO.getStreet()))
                .andExpect(jsonPath("$.postalCode").value(houseDTO.getPostalCode()));

        // Assert
        verify(houseFacade).findById(id);
    }

    @Test
    void findById_houseNotFound_returnsApiError() throws Exception {
        // Arrange
        String nonExistingId = "non-existing-id";
        when(houseFacade.findById(any())).thenThrow(ResourceNotFoundException.class);

        // Act & assert
        String responseJson = mockMvc.perform(get("/house-management/houses/{id}", nonExistingId)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        ApiError response = ObjectConverter.convertJsonToObject(responseJson, ApiError.class);

        // Assert
        assertTrue(response.getTimestamp().isBefore(LocalDateTime.now()));
        assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
        verify(houseFacade).findById(nonExistingId);
    }

    @Test
    void listAllHousesForUser_housesFound_returnsHouses() throws Exception {
        // Arrange
        String userId = houseEntity.getUserId();
        when(houseFacade.listAllHouses(userId)).thenReturn(houseDetailDTOs);

        // Act & assert
        mockMvc.perform(get("/house-management/houses/list/{userId}", userId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(2))
                .andExpect(jsonPath("$[0].houseId").value("houseId"))
                .andExpect(jsonPath("$[1].houseId").value("houseId2"));

        verify(houseFacade).listAllHouses(userId);
    }

    @Test
    void listAllHousesForUser_nonExistingUser_returnsEmptyList() throws Exception {
        // Arrange
        String nonExistingId = "non-existing-id";
        when(houseFacade.listAllHouses(nonExistingId)).thenReturn(Collections.emptyList());

        // Act & assert
        mockMvc.perform(get("/house-management/houses/list/{userId}", nonExistingId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(0));

        verify(houseFacade).listAllHouses(nonExistingId);
    }

    @Test
    void registerNewHouse_houseCreated_returnsHouse() throws Exception {
        // Arrange
        when(houseFacade.registerNewHouse(houseUserDetailDTO)).thenReturn(houseUserDetailDTO);

        // Act & assert
        mockMvc.perform(post("/house-management/houses")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"userId\":\"" + houseUserDetailDTO.getUserId() + "\"," +
                                "\"houseId\":\"" + houseUserDetailDTO.getHouseId() + "\"," +
                                "\"country\":\"" + houseUserDetailDTO.getCountry() + "\"," +
                                "\"city\":\"" + houseUserDetailDTO.getCity() + "\"," +
                                "\"street\":\"" + houseUserDetailDTO.getStreet() + "\"," +
                                "\"postalCode\":\"" + houseUserDetailDTO.getPostalCode() + "\"}"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.country").value(houseUserDetailDTO.getCountry()))
                .andExpect(jsonPath("$.city").value(houseUserDetailDTO.getCity()))
                .andExpect(jsonPath("$.street").value(houseUserDetailDTO.getStreet()))
                .andExpect(jsonPath("$.postalCode").value(houseUserDetailDTO.getPostalCode()));

        verify(houseFacade).registerNewHouse(houseUserDetailDTO);
    }

    @Test
    void unregisterHouse_houseDeleted_returnsHouse() throws Exception {
        // Act & Assert
        mockMvc.perform(delete("/house-management/houses/{houseId}", houseEntity.getHouseId())
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        verify(houseFacade).unregisterHouse(houseEntity.getHouseId());
    }

    @Test
    void registerHouse_validationError_returnsError() throws Exception {
        // Act & Assert
        mockMvc.perform(post("/house-management/houses")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"houseId\":\"" + houseUserDetailDTO.getHouseId() + "\"," +
                                "\"country\":\"" + houseDTO.getCountry() + "\"," +
                                "\"city\":\"" + houseDTO.getCity() + "\"," +
                                "\"street\":\"" + houseDTO.getStreet() + "\"," +
                                "\"postalCode\":\"" + houseDTO.getPostalCode() + "\"}"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("FieldError: userId; must not be blank"))
                .andExpect(jsonPath("$.status").value("BAD_REQUEST"));

        // Verify that the facade method is never called since validation should fail
        verify(houseFacade, never()).registerNewHouse(houseMapper.mapToHouseUserDetailDto(houseEntity));
    }

    @Test
    void unregisterHouse_houseNotFound_returnsApiError() throws Exception {
        // Arrange
        doThrow(ResourceNotFoundException.class).when(houseFacade).unregisterHouse("notExistingHouseId");

        // Act & Assert
        String responseJson = mockMvc.perform(delete("/house-management/houses/{houseId}", "notExistingHouseId"))
                .andExpect(status().isNotFound())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        ApiError response = ObjectConverter.convertJsonToObject(responseJson, ApiError.class);

        // Assert
        assertTrue(response.getTimestamp().isBefore(LocalDateTime.now()));
        assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void updateHouse_houseUpdate_returnsHouse() throws Exception {
        // Arrange
        when(houseFacade.updateHouse(houseUserDetailDTO)).thenReturn(houseUserDetailDTO);

        // Act & Assert
        mockMvc.perform(put("/house-management/houses")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"userId\":\"" + houseUserDetailDTO.getUserId() + "\"," +
                    "\"houseId\":\"" + houseUserDetailDTO.getHouseId() + "\"," +
                    "\"country\":\"" + houseUserDetailDTO.getCountry() + "\"," +
                    "\"city\":\"" + houseUserDetailDTO.getCity() + "\"," +
                    "\"street\":\"" + houseUserDetailDTO.getStreet() + "\"," +
                    "\"postalCode\":\"" + houseUserDetailDTO.getPostalCode() + "\"}"))
            .andExpect(status().isOk());

        verify(houseFacade).updateHouse(any());
    }

    @Test
    void updateHouse_validationError_returnsError() throws Exception {
        // Act & Assert
        mockMvc.perform(put("/house-management/houses")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"userId\":\"" + houseUserDetailDTO.getUserId() + "\"," +
                                "\"houseId\":\"" + houseUserDetailDTO.getHouseId() + "\"," +
                                "\"country\":\"" + houseUserDetailDTO.getCountry() + "\"," +
                                "\"city\":\"" + houseUserDetailDTO.getCity() + "\"," +
                                "\"street\":\"" + houseUserDetailDTO.getStreet() + "\"," +
                                "\"postalCode\":\"" + "" + "\"}"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("FieldError: postalCode; must not be blank"))
                .andExpect(jsonPath("$.status").value("BAD_REQUEST"));

        // Verify that the facade method is never called since validation should fail
        verify(houseFacade, never()).updateHouse(any());
    }

    @Test
    void togglePower_houseFound_updateHouseMainPowerSwitchOnFacadeCalled() throws Exception {
        // Act & Assert
        mockMvc.perform(get("/house-management/houses/{houseId}/togglePower?isPowerOn=true", houseEntity.getHouseId()))
            .andExpect(status().isOk());

        verify(houseFacade).updateHouseMainPowerSwitch(houseEntity.getHouseId(), true);
    }

    @Test
    void togglePower_houseNotFound_returnsApiError() throws Exception {
        // Arrange
        doThrow(ResourceNotFoundException.class).when(houseFacade).updateHouseMainPowerSwitch("notExistingHouseId", true);

        // Act & Assert
        String responseJson = mockMvc.perform(get("/house-management/houses/{houseId}/togglePower?isPowerOn=true", "notExistingHouseId"))
            .andExpect(status().isNotFound())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);
        ApiError response = ObjectConverter.convertJsonToObject(responseJson, ApiError.class);

        // Assert
        assertTrue(response.getTimestamp().isBefore(LocalDateTime.now()));
        assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
    }
}
