package cz.muni.fi.pa165.epsilon.housemanagement.service;

import cz.muni.fi.pa165.epsilon.housemanagement.data.model.House;
import cz.muni.fi.pa165.epsilon.housemanagement.data.repository.HouseRepository;
import cz.muni.fi.pa165.epsilon.housemanagement.exceptions.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static cz.muni.fi.pa165.epsilon.housemanagement.util.TestDataFactory.houseEntities;
import static cz.muni.fi.pa165.epsilon.housemanagement.util.TestDataFactory.houseEntity;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class HouseManagementServiceTest {
    @Mock
    private HouseRepository houseRepository;

    @InjectMocks
    private HouseManagementService houseService;

    @Test
    void findById_houseFound_returnsHouse() {
        // Arrange
        when(houseRepository.findById("houseId")).thenReturn(Optional.ofNullable(houseEntity));

        // Act
        House foundEntity = houseService.findById("houseId");

        // Assert
        assertThat(foundEntity).isEqualTo(houseEntity);
        verify(houseRepository).findById("houseId");
    }

    @Test
    void findById_houseNotFound_throwsResourceNotFoundException() {
        assertThrows(ResourceNotFoundException.class, () -> houseService.findById("notExistingHouseId"));
    }

    @Test
    void listAllHousesForUser_housesFound_returnsHouses() {
        // Arrange
        when(houseRepository.listAllHouses("userId")).thenReturn(houseEntities);

        // Act
        List<House> foundEntity = houseService.listAllHouses("userId");

        // Assert
        assertThat(foundEntity).isEqualTo(houseEntities);
        verify(houseRepository).listAllHouses("userId");
    }

    @Test
    void listAllHousesForUser_noHousesForUser_returnsEmptyList() {
        // Arrange
        when(houseRepository.listAllHouses("userId")).thenReturn(Collections.emptyList());

        // Act
        List<House> foundEntity = houseService.listAllHouses("userId");

        // Assert
        assertThat(foundEntity).isEmpty();
        verify(houseRepository).listAllHouses("userId");
    }

    @Test
    void registerNewHouse_houseCreated_returnsHouse() {
        // Arrange
        when(houseRepository.save(any(House.class))).thenReturn(houseEntity);

        // Act
        House createdEntity = houseService.registerNewHouse(houseEntity);

        // Assert
        assertThat(createdEntity).isEqualTo(houseEntity);
        verify(houseRepository).save(any(House.class));
    }

    @Test
    void deleteHouse_houseFound_returnsHouse() {
        // Arrange
        when(houseRepository.findById(houseEntity.getHouseId())).thenReturn(Optional.ofNullable(houseEntity));

        // Act
        House deletedHouse = houseService.deleteHouse(houseEntity.getHouseId());

        // Assert
        assertThat(deletedHouse).isEqualTo(houseEntity);
        verify(houseRepository).deleteById(houseEntity.getHouseId());
    }

    @Test
    void deleteHouse_houseNotFound_throwsResourceNotFoundException() {
        assertThrows(ResourceNotFoundException.class, () -> houseService.deleteHouse("houseId"));
    }

    @Test
    void updateHouse_houseUpdated_returnsHouse() {
        // Arrange
        when(houseRepository.findById(any(String.class))).thenReturn(Optional.ofNullable(houseEntity));
        when(houseRepository.save(any(House.class))).thenReturn(houseEntity);

        // Act
        House updateHouse = houseService.updateHouse(houseEntity);

        // Assert
        assertThat(updateHouse).isEqualTo(houseEntity);
        verify(houseRepository).save(any(House.class));
    }

    @Test
    void updateHouse_houseNotFound_throwsResourceNotFoundException() {
        assertThrows(ResourceNotFoundException.class, () -> houseService.updateHouse(houseEntity));
    }

    @Test
    void findById_houseFound_verifyHouseRepositoryCalled() {
        // Arrange
        when(houseRepository.findById("houseId")).thenReturn(Optional.ofNullable(houseEntity));

        // Act
        houseService.findById("houseId");

        // Assert
        verify(houseRepository).findById("houseId");
    }

    @Test
    void deleteHouse_verifyHouseRepositoryCalled() {
        // Arrange
        when(houseRepository.findById(houseEntity.getHouseId())).thenReturn(Optional.ofNullable(houseEntity));

        // Act
        houseService.deleteHouse(houseEntity.getHouseId());

        // Assert
        verify(houseRepository).deleteById(houseEntity.getHouseId());
    }
}
