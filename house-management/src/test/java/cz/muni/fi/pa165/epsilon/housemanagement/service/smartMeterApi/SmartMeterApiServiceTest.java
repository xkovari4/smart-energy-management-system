package cz.muni.fi.pa165.epsilon.housemanagement.service.smartMeterApi;

import cz.muni.fi.pa165.epsilon.housemanagement.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.epsilon.housemanagement.service.HouseManagementService;
import cz.muni.fi.pa165.epsilon.smart_meter_management.client.SmartMeterApi;
import cz.muni.fi.pa165.epsilon.smart_meter_management.client.invoker.ApiException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static cz.muni.fi.pa165.epsilon.housemanagement.util.TestDataFactory.*;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SmartMeterApiServiceTest {

    @Mock
    private SmartMeterApi smartMeterApi;

    @Mock
    private HouseManagementService houseManagementService;

    @InjectMocks
    private SmartMeterApiService smartMeterApiService;

    @Test
    void updateHouseMainPowerSwitch_houseFound() throws ApiException {
        // Arrange
        when(houseManagementService.findById(houseEntity.getHouseId())).thenReturn(houseEntity);
        when(smartMeterApi.listAllForHouseId(houseEntity.getHouseId())).thenReturn(smartMeterDTOs);

        // Act & Assert
        assertDoesNotThrow(() -> smartMeterApiService.updateHouseMainPowerSwitch(houseEntity.getHouseId(), true));
    }

    @Test
    void updateHouseMainPowerSwitch_houseNotFound() {
        // Arrange
        when(houseManagementService.findById("NotExistingHouseId")).thenThrow(ResourceNotFoundException.class);

        // Act & Assert
        assertThrows(ResourceNotFoundException.class, () -> smartMeterApiService.updateHouseMainPowerSwitch("NotExistingHouseId", true));
    }

    @Test
    void updateHouseMainPowerSwitch_houseTurnOn_houseTurnedOn() throws ApiException {
        // Arrange
        when(houseManagementService.findById(houseEntity.getHouseId())).thenReturn(houseEntity);
        when(smartMeterApi.listAllForHouseId(houseEntity.getHouseId())).thenReturn(smartMeterDTOs);

        // Act
        smartMeterApiService.updateHouseMainPowerSwitch(houseEntity.getHouseId(), true);

        // Assert
        verify(smartMeterApi).turnOnSmartMeter(smartMeterDTO.getId());
    }

    @Test
    void updateHouseMainPowerSwitch_houseTurnOff_houseTurnedOff() throws ApiException {
        // Arrange
        when(houseManagementService.findById(houseEntity.getHouseId())).thenReturn(houseEntity);
        when(smartMeterApi.listAllForHouseId(houseEntity.getHouseId())).thenReturn(smartMeterDTOs);

        // Act
        smartMeterApiService.updateHouseMainPowerSwitch(houseEntity.getHouseId(), false);

        // Assert
        verify(smartMeterApi).turnOffSmartMeter(smartMeterDTO.getId());
    }

    @Test
    void updateHouseMainPowerSwitch_apiException_throwsResourceNotFoundException() throws ApiException {
        // Arrange
        when(houseManagementService.findById(houseEntity.getHouseId())).thenReturn(houseEntity);
        when(smartMeterApi.listAllForHouseId(houseEntity.getHouseId())).thenThrow(ApiException.class);

        // Act & Assert
        assertThrows(ResourceNotFoundException.class, () -> smartMeterApiService.updateHouseMainPowerSwitch(houseEntity.getHouseId(), true));
    }
}
