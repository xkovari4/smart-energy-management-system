package cz.muni.fi.pa165.epsilon.housemanagement.util;

import cz.muni.fi.pa165.epsilon.housemanagement.api.HouseDTO;
import cz.muni.fi.pa165.epsilon.housemanagement.api.HouseDetailDTO;
import cz.muni.fi.pa165.epsilon.housemanagement.api.HouseUserDetailDTO;
import cz.muni.fi.pa165.epsilon.housemanagement.data.model.House;
import cz.muni.fi.pa165.epsilon.smart_meter_management.client.model.SmartMeterDTO;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TestDataFactory {
    public static House houseEntity = getHouseEntityFactory();
    public static List<House> houseEntities = getHouseEntitiesFactory();
    public static HouseDTO houseDTO = getHouseDtoFactory();
    public static HouseDetailDTO houseDetailDTO = getHouseDetailDtoFactory();
    public static List<HouseDetailDTO> houseDetailDTOs = getHouseDetailDtosFactory();
    public static HouseUserDetailDTO houseUserDetailDTO = getHouseUserDetailDtoFactory();
    public static SmartMeterDTO smartMeterDTO = getSmartMeterDtoFactory();
    public static List<SmartMeterDTO> smartMeterDTOs = getSmartMeterDtosFactory();

    private static House getHouseEntityFactory() {
        var house = new House();
        house.setUserId("userId");
        house.setHouseId("houseId");
        house.setCountry("CZ");
        house.setCity("Brno");
        house.setStreet("Ceska");
        house.setPostalCode("60200");
        return house;
    }

    private static List<House> getHouseEntitiesFactory() {
        House house1 = getHouseEntityFactory();
        House house2 = getHouseEntityFactory();
        house2.setHouseId("houseId2");
        return List.of(house1, house2);
    }

    private static HouseDTO getHouseDtoFactory() {
        HouseDTO houseDTO = new HouseDTO();
        houseDTO.setCity("Brno");
        houseDTO.setCountry("CZ");
        houseDTO.setStreet("Ceska");
        houseDTO.setPostalCode("60200");
        return houseDTO;
    }

    private static HouseDetailDTO getHouseDetailDtoFactory() {
        HouseDetailDTO houseDetailDto = new HouseDetailDTO();
        houseDetailDto.setHouseId("houseId");
        houseDetailDto.setCity("Brno");
        houseDetailDto.setCountry("CZ");
        houseDetailDto.setStreet("Ceska");
        houseDetailDto.setPostalCode("60200");
        return houseDetailDto;
    }

    private static List<HouseDetailDTO> getHouseDetailDtosFactory() {
        HouseDetailDTO houseDetailDTO1 = getHouseDetailDtoFactory();
        HouseDetailDTO houseDetailDTO2 = getHouseDetailDtoFactory();
        houseDetailDTO2.setHouseId("houseId2");
        return List.of(houseDetailDTO1, houseDetailDTO2);
    }

    private static HouseUserDetailDTO getHouseUserDetailDtoFactory() {
        HouseUserDetailDTO houseUserDetailDTO = new HouseUserDetailDTO();
        houseUserDetailDTO.setHouseId("houseId");
        houseUserDetailDTO.setUserId("userId");
        houseUserDetailDTO.setCity("Brno");
        houseUserDetailDTO.setCountry("CZ");
        houseUserDetailDTO.setStreet("Ceska");
        houseUserDetailDTO.setPostalCode("60200");
        return houseUserDetailDTO;
    }

    private static SmartMeterDTO getSmartMeterDtoFactory() {
        var sm = new SmartMeterDTO();
        sm.setId("1");
        sm.setName("Test Meter");
        sm.setDescription("Test Description");
        sm.setIpAddress("127.0.0.1");
        sm.setHouseId("Test House");
        sm.setIsPoweredOn(true);
        return sm;
    }

    private static List<SmartMeterDTO> getSmartMeterDtosFactory() {
        return List.of(getSmartMeterDtoFactory());
    }
}
