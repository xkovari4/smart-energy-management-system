from locust import HttpUser, task, between
from random import randint, choice
from string import hexdigits
import uuid

auth_token = "insert-your-auth-token-here"

def random_ip(v): # credits: https://codereview.stackexchange.com/questions/200337/random-ip-address-generator
    if v == 4:
        octets = []
        for x in range(4):
            octets.append(str(randint(0,255)))
        return '.'.join(octets)

    elif v == 6:
        octets = []
        for x in range(8):
            octet = []
            for x in range(4):
                octet.append(str(choice(hexdigits.lower())))
            octets.append(''.join(octet))
        return ':'.join(octets)
    else:
       return

house_electronics = ["TV", "Refrigerator", "Microwave", "Coffee Maker", "Toaster", "Blender", "Dishwasher", "Washing Machine", "Dryer", "Oven", "Stove", "Air Conditioner", "Heater", "Fan", "Lamp", "Vacuum Cleaner", "Iron", "Hair Dryer", "Electric Kettle", "Rice Cooker", "Electric Grill", "Food Processor", "Electric Mixer", "Dehumidifier", "Humidifier", "Water Heater", "Television", "DVD Player", "Stereo System", "Home Theater System", "Speaker", "Game Console", "Router", "Modem", "Printer", "Scanner", "Fax Machine", "Desktop Computer", "Laptop", "Tablet", "Smartphone", "Smartwatch", "Fitness Tracker", "Digital Camera", "Security Camera", "Doorbell Camera", "Baby Monitor", "Smart Thermostat", "Smart Light Bulb", "Smart Plug", "Smart Lock", "Smart Doorbell", "Smart Smoke Detector", "Smart Carbon Monoxide Detector", "Smart Water Leak Detector", "Smart Garage Door Opener", "Smart Sprinkler Controller", "Smart Alarm Clock", "E-reader", "Portable Speaker", "Bluetooth Headphones", "Wireless Earbuds", "Power Strip", "USB Charger", "Wireless Charger", "Battery Charger", "Electric Toothbrush", "Water Flosser", "Hair Straightener", "Curling Iron", "Shaver", "Electric Razor", "Air Purifier", "UV-C Sanitizer", "Robotic Vacuum", "Robotic Mop", "Robotic Lawn Mower", "Smart Scale", "Smart Mirror", "Streaming Device", "Digital Photo Frame", "Home Assistant", "Voice Assistant Speaker", "Smart Refrigerator", "Smart Oven", "Smart Washer", "Smart Dryer", "Smart Dishwasher", "Smart Trash Can", "Smart Blinds", "Smart Curtain", "Smart Mirror", "Smart Bathtub", "Smart Shower", "Smart Toilet", "Smart Bidet"]
descriptions = ["The one in Living room", "The one in Kitchen", "The one in Bedroom", "The one in Bathroom", "The one in Dining room", "The one in Home office", "The one in Basement", "The one in Attic", "The one in Garage", "The one in Laundry room", "The one in Patio", "The one in Backyard", "The one in Front porch", "The one in Guest room", "The one in Children's room", "The one in Playroom", "The one in Closet", "The one in Hallway", "The one in Staircase", "The one in Study room"]
house_ids = ['45097215-6b00-458e-bb2a-546b0401d884', '89d02f8e-a64a-4958-bdd0-093caa51cf72', '179ba454-bb80-4797-ad33-cbebb2c82790', 'c3b2bcb8-51e2-4a40-9d12-cb5da17f64c6', '96f281c1-64ea-45f2-865e-1e89a28936dc', '78fc675c-32e6-4442-8b34-4a23376ca44a', 'f473bbf4-9ba3-4f30-9fe7-d1f0b201539d', 'd5b179e3-2c3a-4bb3-9cb2-3ec9df806cbf', 'd358b691-96b0-406d-a7bf-0dd6edfbcc8a', '67be633c-a3b1-4f91-b475-f675b78f18be', '7a273b76-5447-41d1-bf2a-c060361534cc', '2d03171c-6490-4f80-8521-836d48c3b3ba', 'ffa68758-74d4-4017-8855-0ce6ec0668cf', 'cb4e21d8-c07a-47e3-96d9-d34ebdfa930d', 'd7e70de2-de29-49c0-9942-8a5c0a9a3ff8', '5a7b5321-5186-4a7e-a7de-320dde1b92b7', '0007893e-e80d-4360-98e8-c58aa2acdb1f', '94ff9bb7-d3d4-4237-8d5c-c5306ab775b8', 'e2a38ba7-b3ed-4a3a-9bc7-43e26b9ae67a', 'cc501dbf-4d25-4b04-8c6a-28246caf7e3a']


class MyUser(HttpUser):
    wait_time = between(1, 3)

    @task
    def create_smart_meter_and_turn_it_on(self):
        response = self.create_smart_meter()
        if response.status_code == 201:
            smart_meter_id = response.json().get('id')
            self.turn_on_smart_meter(smart_meter_id)
        else:
            print("Failed to create smart meter")

        
    def turn_on_smart_meter(self, smart_meter_id):
        headers = {
            'accept': 'application/json',
            'Authorization': 'Bearer ' + auth_token
        }
        response = self.client.put(f'http://localhost:8082/api/smart-meters/{smart_meter_id}/turn-on', headers=headers)
        if response.status_code != 200:
            print("Failed to turn on smart meter")


    def create_smart_meter(self):
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + auth_token
        }
        data = {
            "name": choice(house_electronics),
            "description": choice(descriptions),
            "ipAddress": random_ip(6),
            "houseId": choice(house_ids),
            "isPoweredOn": False
        }
        return self.client.post('http://localhost:8082/api/smart-meters', json=data, headers=headers)

        