package cz.muni.fi.pa165.epsilon.smartmetermanagement.api;

import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.model.SmartMeter;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;

import java.sql.Timestamp;
import java.util.Objects;

/**
 * DTO for a smart meter.
 *
 * @see SmartMeter
 */
@Schema(title = "Smart Meter's measure DTO", description = "DTO representation of one smart meter's measure")
public class MeasureDTO {

    @Schema(description = "uuid", example = "123e4567-e89b-12d3-a456-426614174000")
    private @NotBlank String id;
    @Schema(description = "Smart meter's ID by which the measure was taken", example = "123e4567-e89b-12d3-a456-426614174000")
    private @NotBlank String smartMeterId;
    @Schema(description = "Timestamp of when the measure was taken", example = "1564061013564")
    private Timestamp timestamp;
    @Schema(description = "Measured value", example = "130.3")
    private @NotBlank double measuredValue;

    public MeasureDTO() {
    }

    public MeasureDTO(String id, String smartMeterId, Timestamp timestamp, double measuredValue) {
        this.id = id;
        this.smartMeterId = smartMeterId;
        this.timestamp = timestamp;
        this.measuredValue = measuredValue;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSmartMeterId() {
        return smartMeterId;
    }

    public void setSmartMeterId(String smartMeterId) {
        this.smartMeterId = smartMeterId;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public double getMeasuredValue() {
        return measuredValue;
    }

    public void setMeasuredValue(double measuredValue) {
        this.measuredValue = measuredValue;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (MeasureDTO) obj;
        return Objects.equals(this.id, that.id) && Objects.equals(this.smartMeterId, that.smartMeterId) && Objects.equals(this.timestamp, that.timestamp) && Double.doubleToLongBits(this.measuredValue) == Double.doubleToLongBits(that.measuredValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, smartMeterId, timestamp, measuredValue);
    }

    @Override
    public String toString() {
        return "MeasureDTO[" + "id=" + id + ", " + "smartMeterId=" + smartMeterId + ", " + "timestamp=" + timestamp + ", " + "value=" + measuredValue + ']';
    }

}
