package cz.muni.fi.pa165.epsilon.smartmetermanagement.config;

import cz.muni.fi.pa165.epsilon.smartmetermanagement.SmartMeterManagementApplication;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springdoc.core.customizers.OpenApiCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class SmartMeterManagementSecurityConfig {
    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests(x -> x
                        .requestMatchers(HttpMethod.GET, "/api/measures").hasAuthority("SCOPE_test_read")
                        .requestMatchers(HttpMethod.DELETE, "/api/measures").hasAuthority("SCOPE_test_1")
                        .requestMatchers(HttpMethod.GET, "/api/measures/{smartMeterId}").hasAuthority("SCOPE_test_read")
                        .requestMatchers(HttpMethod.GET, "/api/measures/{smartMeterId}/{date}").hasAuthority("SCOPE_test_read")

                        .requestMatchers(HttpMethod.GET, "/api/smart-meters").hasAuthority("SCOPE_test_read")
                        .requestMatchers(HttpMethod.PUT, "/api/smart-meters").hasAuthority("SCOPE_test_write")
                        .requestMatchers(HttpMethod.POST, "/api/smart-meters").hasAuthority("SCOPE_test_write")
                        .requestMatchers(HttpMethod.PUT, "/api/smart-meters/{smartMeterId}/turn-on").hasAuthority("SCOPE_test_2")
                        .requestMatchers(HttpMethod.PUT, "/api/smart-meters/{smartMeterId}/turn-off").hasAuthority("SCOPE_test_2")
                        .requestMatchers(HttpMethod.GET, "/api/smart-meters/{smartMeterId}").hasAuthority("SCOPE_test_read")
                        .requestMatchers(HttpMethod.DELETE, "/api/smart-meters/{smartMeterId}").hasAuthority("SCOPE_test_1")
                        .requestMatchers(HttpMethod.GET, "/api/smart-meters-for-house/{houseId}").hasAuthority("SCOPE_test_read")

                        .anyRequest().permitAll()
                )
                .oauth2ResourceServer(oauth2 -> oauth2.opaqueToken(Customizer.withDefaults()))
                .csrf().disable();
        return http.build();
    }


    @Bean
    public OpenApiCustomizer openAPICustomizer() {
        return openApi ->
                openApi.getComponents()
                        .addSecuritySchemes(SmartMeterManagementApplication.SECURITY_SCHEME_BEARER,
                                new SecurityScheme()
                                        .type(SecurityScheme.Type.HTTP)
                                        .scheme("bearer")
                                        .description("Provide a valid access token")
                        );

    }
}
