package cz.muni.fi.pa165.epsilon.smartmetermanagement.data.model;


import jakarta.persistence.*;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "measure")
public class Measure implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "id_measure")
    private String id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "smart_meter_id")
    private SmartMeter smartMeter;

    @Column(name = "timestamp_")
    private Timestamp timestamp;

    @Column(name = "measured_value", nullable = false)
    private double measuredValue;

    public Measure() {
    }

    public Measure(String id, SmartMeter smartMeter, Timestamp timestamp, double measuredValue) {
        this.id = id;
        this.smartMeter = smartMeter;
        this.timestamp = timestamp;
        this.measuredValue = measuredValue;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SmartMeter getSmartMeter() {
        return smartMeter;
    }

    public void setSmartMeter(SmartMeter smartMeter) {
        this.smartMeter = smartMeter;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public double getMeasuredValue() {
        return measuredValue;
    }

    public void setMeasuredValue(double measuredValue) {
        this.measuredValue = measuredValue;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (Measure) obj;
        return Objects.equals(this.id, that.id) && Objects.equals(this.smartMeter, that.smartMeter) && Objects.equals(this.timestamp, that.timestamp) && Double.doubleToLongBits(this.measuredValue) == Double.doubleToLongBits(that.measuredValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, smartMeter, timestamp, measuredValue);
    }

    @Override
    public String toString() {
        return "Measure[" + "id=" + id + ", " + "smartMeter=" + smartMeter + ", " + "timestamp=" + timestamp + ", " + "value=" + measuredValue + ']';
    }

}
