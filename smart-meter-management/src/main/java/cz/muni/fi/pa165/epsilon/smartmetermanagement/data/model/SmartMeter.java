package cz.muni.fi.pa165.epsilon.smartmetermanagement.data.model;

import jakarta.persistence.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "smart_meter")
public class SmartMeter implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "id_smart_meter")
    private String id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "ip_address", unique = true, nullable = false)
    private String ipAddress;

    @Column(name = "house_id", nullable = false)
    private String houseId;

    @Column(name = "is_powered_on")
    private Boolean isPoweredOn;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "smartMeter", cascade = CascadeType.ALL, orphanRemoval = true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Set<Measure> measures;

    public SmartMeter() {

    }

    public SmartMeter(String id, String name, String description, String ipAddress, String houseId, Boolean isPoweredOn, Set<Measure> measures) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.ipAddress = ipAddress;
        this.houseId = houseId;
        this.isPoweredOn = isPoweredOn;
        this.measures = measures;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getHouseId() {
        return houseId;
    }

    public void setHouseId(String houseId) {
        this.houseId = houseId;
    }

    public Boolean getIsPoweredOn() {
        return isPoweredOn;
    }

    public void setIsPoweredOn(Boolean isPoweredOn) {
        this.isPoweredOn = isPoweredOn;
    }

    public Set<Measure> getMeasures() {
        return measures;
    }

    public void setMeasures(Set<Measure> measures) {
        this.measures = measures;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (SmartMeter) obj;
        return Objects.equals(this.id, that.id) && Objects.equals(this.name, that.name) && Objects.equals(this.description, that.description) && Objects.equals(this.ipAddress, that.ipAddress) && Objects.equals(this.houseId, that.houseId) && Objects.equals(this.isPoweredOn, that.isPoweredOn) && Objects.equals(this.measures, that.measures);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, ipAddress, houseId, isPoweredOn, measures);
    }

    @Override
    public String toString() {
        return "SmartMeter[" + "id=" + id + ", " + "name=" + name + ", " + "description=" + description + ", " + "ipAddress=" + ipAddress + ", " + "houseId=" + houseId + ", " + "isPoweredOn=" + isPoweredOn + ']';
    }

}

