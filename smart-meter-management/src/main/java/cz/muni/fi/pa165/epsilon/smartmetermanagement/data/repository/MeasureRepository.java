package cz.muni.fi.pa165.epsilon.smartmetermanagement.data.repository;

import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.model.Measure;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface MeasureRepository extends JpaRepository<Measure, String> {

    @Query("SELECT sm.measures FROM SmartMeter sm WHERE sm.id = :smartMeterId")
    List<Measure> findAllForSmartMeterId(String smartMeterId);

    @Query("SELECT m FROM SmartMeter sm LEFT JOIN sm.measures m WHERE sm.id = :smartMeterId " +
            "AND YEAR(m.timestamp) = YEAR(:date)" +
            "AND MONTH(m.timestamp) = MONTH(:date)" +
            "AND DAY(m.timestamp) = DAY(:date)")
    List<Measure> findAllForSmartMeterIdForDay(String smartMeterId, LocalDate date);
}