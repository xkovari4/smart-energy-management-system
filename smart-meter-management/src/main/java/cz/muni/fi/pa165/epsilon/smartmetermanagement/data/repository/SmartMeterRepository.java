package cz.muni.fi.pa165.epsilon.smartmetermanagement.data.repository;

import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.model.SmartMeter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SmartMeterRepository extends JpaRepository<SmartMeter, String> {

    @Query("SELECT sm FROM SmartMeter sm WHERE sm.houseId like :houseId")
    List<SmartMeter> findAllForHouseId(String houseId);
}
