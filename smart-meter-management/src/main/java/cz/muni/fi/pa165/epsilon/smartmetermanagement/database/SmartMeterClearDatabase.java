package cz.muni.fi.pa165.epsilon.smartmetermanagement.database;

import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.repository.MeasureRepository;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.repository.SmartMeterRepository;
import jakarta.annotation.PostConstruct;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Transactional
@AllArgsConstructor
@Profile("database-clear")
public class SmartMeterClearDatabase {
    private final MeasureRepository measureRepository;
    private final SmartMeterRepository smartMeterRepository;

    @PostConstruct
    public void init() {
        System.out.println("Initializing Database Clearer...");
        clearDatabase();
    }

    private void clearDatabase() {
        measureRepository.deleteAll();
        smartMeterRepository.deleteAll();
        System.out.println("Database cleared");
    }
}
