package cz.muni.fi.pa165.epsilon.smartmetermanagement.database;

import com.github.javafaker.Faker;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.model.Measure;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.model.SmartMeter;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.repository.MeasureRepository;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.repository.SmartMeterRepository;
import jakarta.annotation.PostConstruct;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;

@Service
@Transactional
@AllArgsConstructor
@Profile("database-start")
public class SmartMeterSeedDatabase {

    private static final int NUMBER_OF_SMART_METERS = 100;
    private static final int MAX_MEASURES = 3;
    private final MeasureRepository measureRepository;
    private final SmartMeterRepository smartMeterRepository;
    private final Faker faker = new Faker();

    @PostConstruct
    public void init() {
        System.out.println("Initializing Database Seeder...");
        if (smartMeterRepository.count() == 0) {
            seedSmartMetersAndMeasures();
        } else {
            System.out.println("SmartMeter database already seeded");
        }
    }

    private void seedSmartMetersAndMeasures() {
        for (int i = 0; i < NUMBER_OF_SMART_METERS; i++) {
            SmartMeter smartMeter = new SmartMeter();
            smartMeter.setName(faker.animal().name());
            String quote = faker.elderScrolls().quote();
            smartMeter.setDescription(quote.length() > 254 ? quote.substring(0, 254) : quote);
            smartMeter.setIpAddress(faker.internet().publicIpV4Address());
            smartMeter.setIsPoweredOn(faker.options().option(Boolean.FALSE, Boolean.TRUE));
            smartMeter.setHouseId(String.valueOf(i));
            smartMeterRepository.save(smartMeter);

            int numberOfMeasures = faker.number().numberBetween(1, MAX_MEASURES);
            for (int j = 0; j < numberOfMeasures; j++) {
                Measure measure = new Measure();
                measure.setTimestamp(Timestamp.valueOf(LocalDateTime.now().minusDays(faker.number().numberBetween(1, 365))));
                measure.setMeasuredValue(faker.number().randomNumber());
                measure.setSmartMeter(smartMeter);
                measureRepository.save(measure);
            }
        }
        System.out.println("Database seeded");
    }
}
