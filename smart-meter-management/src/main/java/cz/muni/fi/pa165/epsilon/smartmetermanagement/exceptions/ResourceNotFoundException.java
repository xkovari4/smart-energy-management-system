package cz.muni.fi.pa165.epsilon.smartmetermanagement.exceptions;

/**
 * Runtime exception representing non-existing resource.
 */
public class ResourceNotFoundException extends RuntimeException {

    public ResourceNotFoundException(String message) {
        super(message);
    }
}
