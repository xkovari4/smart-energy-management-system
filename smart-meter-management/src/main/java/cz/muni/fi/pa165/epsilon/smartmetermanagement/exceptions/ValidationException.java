package cz.muni.fi.pa165.epsilon.smartmetermanagement.exceptions;

/**
 * Validation exception
 */
public class ValidationException extends RuntimeException {

    public ValidationException(String message) {
        super(message);
    }
}
