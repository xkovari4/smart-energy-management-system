package cz.muni.fi.pa165.epsilon.smartmetermanagement.facade;

import cz.muni.fi.pa165.epsilon.smartmetermanagement.api.MeasureDTO;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.mappers.MeasureMapper;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.service.MeasureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
@Transactional
public class MeasureFacade {
    private final MeasureService measureService;
    private final MeasureMapper measureMapper;

    @Autowired
    public MeasureFacade(MeasureService measureService, MeasureMapper measureMapper) {
        this.measureService = measureService;
        this.measureMapper = measureMapper;
    }

    @Transactional(readOnly = true)
    public List<MeasureDTO> listAll() {
        return measureMapper.mapToList(measureService.listAll());
    }

    @Transactional(readOnly = true)
    public List<MeasureDTO> listAllForSmartMeterId(String smartMeterId) {
        return measureMapper.mapToList(measureService.listAllForSmartMeterId(smartMeterId));
    }

    @Transactional(readOnly = true)
    public List<MeasureDTO> listAllForSmartMeterIdForDay(String smartMeterId, LocalDate date) {
        return measureMapper.mapToList(measureService.listAllForSmartMeterIdForDay(smartMeterId, date));
    }

    public List<MeasureDTO> removeMeasures(List<String> measureIds) {
        return measureMapper.mapToList(measureService.removeMeasures(measureIds));
    }
}
