package cz.muni.fi.pa165.epsilon.smartmetermanagement.facade;

import cz.muni.fi.pa165.epsilon.smartmetermanagement.api.SmartMeterDTO;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.mappers.SmartMeterMapper;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.rest.dto.RegisterSmartMeterRequestDTO;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.rest.dto.UpdateSmartMeterRequestDTO;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.service.SmartMeterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class SmartMeterFacade {
    private final SmartMeterService smartMeterService;
    private final SmartMeterMapper smartMeterMapper;

    @Autowired
    public SmartMeterFacade(SmartMeterService smartMeterService, SmartMeterMapper smartMeterMapper) {
        this.smartMeterService = smartMeterService;
        this.smartMeterMapper = smartMeterMapper;
    }

    public SmartMeterDTO registerNewSmartMeter(RegisterSmartMeterRequestDTO smartMeterDTO) {
        var smartMeter = smartMeterMapper.mapToSmartMeter(smartMeterDTO);
        return smartMeterMapper.mapToDto(smartMeterService.registerNewSmartMeter(smartMeter));
    }

    @Transactional(readOnly = true)
    public SmartMeterDTO findById(String smartMeterId) {
        return smartMeterMapper.mapToDto(smartMeterService.findById(smartMeterId));
    }

    public SmartMeterDTO updateSmartMeter(UpdateSmartMeterRequestDTO smartMeterDTO) {
        var smartMeter = smartMeterMapper.mapToSmartMeterUpdate(smartMeterDTO);
        return smartMeterMapper.mapToDto(smartMeterService.updateSmartMeter(smartMeter));
    }

    public SmartMeterDTO removeSmartMeter(String smartMeterId) {
        return smartMeterMapper.mapToDto(smartMeterService.removeSmartMeter(smartMeterId));
    }

    @Transactional(readOnly = true)
    public List<SmartMeterDTO> listAll() {
        return smartMeterMapper.mapToList(smartMeterService.listAll());
    }

    @Transactional(readOnly = true)
    public List<SmartMeterDTO> listAllForHouseId(String houseId) {
        return smartMeterMapper.mapToList(smartMeterService.listAllForHouseId(houseId));
    }

    public SmartMeterDTO turnOnSmartMeter(String smartMeterId) {
        return smartMeterMapper.mapToDto(smartMeterService.turnOnSmartMeter(smartMeterId));
    }

    public SmartMeterDTO turnOffSmartMeter(String smartMeterId) {
        return smartMeterMapper.mapToDto(smartMeterService.turnOffSmartMeter(smartMeterId));
    }
}
