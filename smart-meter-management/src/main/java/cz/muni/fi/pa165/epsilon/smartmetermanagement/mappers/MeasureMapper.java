package cz.muni.fi.pa165.epsilon.smartmetermanagement.mappers;

import cz.muni.fi.pa165.epsilon.smartmetermanagement.api.MeasureDTO;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.model.Measure;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * Mapper for {@link Measure}
 */
@Mapper(componentModel = "spring")
public interface MeasureMapper {
    default MeasureDTO mapToDto(Measure measure) {
        if (measure == null) {
            return null;
        }

        MeasureDTO measureDTO = new MeasureDTO();

        measureDTO.setId(measure.getId());
        measureDTO.setTimestamp(measure.getTimestamp());
        measureDTO.setMeasuredValue(measure.getMeasuredValue());
        measureDTO.setSmartMeterId(measure.getSmartMeter().getId());

        return measureDTO;
    }

    List<MeasureDTO> mapToList(List<Measure> measures);
}
