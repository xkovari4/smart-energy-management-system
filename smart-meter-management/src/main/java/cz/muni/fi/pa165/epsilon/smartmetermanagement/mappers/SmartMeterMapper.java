package cz.muni.fi.pa165.epsilon.smartmetermanagement.mappers;

import cz.muni.fi.pa165.epsilon.smartmetermanagement.api.SmartMeterDTO;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.model.SmartMeter;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.rest.dto.RegisterSmartMeterRequestDTO;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.rest.dto.UpdateSmartMeterRequestDTO;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * Mapper for {@link SmartMeter}
 */
@Mapper(componentModel = "spring")
public interface SmartMeterMapper {
    SmartMeter mapToSmartMeter(RegisterSmartMeterRequestDTO smartMeterDTO);

    SmartMeter mapToSmartMeterUpdate(UpdateSmartMeterRequestDTO smartMeterDTO);

    SmartMeterDTO mapToDto(SmartMeter smartMeter);

    List<SmartMeterDTO> mapToList(List<SmartMeter> smartMeters);
}
