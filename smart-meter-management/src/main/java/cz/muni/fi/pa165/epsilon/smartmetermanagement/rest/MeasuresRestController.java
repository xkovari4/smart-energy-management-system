package cz.muni.fi.pa165.epsilon.smartmetermanagement.rest;

import cz.muni.fi.pa165.epsilon.smartmetermanagement.SmartMeterManagementApplication;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.api.MeasureDTO;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.facade.MeasureFacade;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

/**
 * Spring REST Controller for communication using HTTP protocol and JSON data format.
 */
@RestController
@OpenAPIDefinition(
        info = @Info(title = "Measure service",
                version = "1.1",
                description = """
                        Simple service for managing Measures collected by Smart Meters.
                        The API has operations for:
                        - getting stored measures for specific Smart Meter, specified by Smart Meter's ID
                        - removing multiple measures by their IDs
                        """,
                contact = @Contact(name = "Vit Nakladal", email = "485524@mail.muni.cz", url = "https://is.muni.cz/auth/osoba/485524"),
                license = @License(name = "Apache 2.0", url = "https://www.apache.org/licenses/LICENSE-2.0.html")
        ),
        servers = {@Server(url = "http://smart-meter-management:8080", description = "Docker network server"),
                @Server(url = "http://localhost:8082", description = "Localhost network server")}
)
@Tag(name = "Measure", description = "microservice for measures")
@RequestMapping(path = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
public class MeasuresRestController {
    private final MeasureFacade measureFacade;

    @Autowired
    public MeasuresRestController(MeasureFacade measureFacade) {
        this.measureFacade = measureFacade;
    }

    @Operation(
            summary = "Get all measures",
            description = "List all measures",
            security = {@SecurityRequirement(name = SmartMeterManagementApplication.SECURITY_SCHEME_OAUTH2, scopes = {"test_read"}),
                    @SecurityRequirement(name = SmartMeterManagementApplication.SECURITY_SCHEME_BEARER, scopes = {"test_read"})}
    )
    @GetMapping("/measures")
    @CrossOrigin(origins = "*")
    @Transactional(readOnly = true)
    public ResponseEntity<List<MeasureDTO>> listAll() {
        return ResponseEntity.ok(measureFacade.listAll());
    }

    @Operation(
            summary = "Get all measures for smart meter",
            description = "List all measures having precise smartMeterId",
            security = {@SecurityRequirement(name = SmartMeterManagementApplication.SECURITY_SCHEME_OAUTH2, scopes = {"test_read"}),
                    @SecurityRequirement(name = SmartMeterManagementApplication.SECURITY_SCHEME_BEARER, scopes = {"test_read"})}
    )
    @GetMapping("/measures/{smartMeterId}")
    @CrossOrigin(origins = "*")
    @Transactional(readOnly = true)
    public ResponseEntity<List<MeasureDTO>> listAllForSmartMeterId(@PathVariable("smartMeterId") String smartMeterId) {
        return ResponseEntity.ok(measureFacade.listAllForSmartMeterId(smartMeterId));
    }

    @Operation(
            summary = "Get all measures for smart meter and for specified day",
            description = "List all measures having precise smartMeterId and is measured within in specified day, the date has to be in 'yyyy-mm-dd' format",
            security = {@SecurityRequirement(name = SmartMeterManagementApplication.SECURITY_SCHEME_OAUTH2, scopes = {"test_read"}),
                    @SecurityRequirement(name = SmartMeterManagementApplication.SECURITY_SCHEME_BEARER, scopes = {"test_read"})}
    )
    @GetMapping("/measures/{smartMeterId}/{date}")
    @CrossOrigin(origins = "*")
    @Transactional(readOnly = true)
    public ResponseEntity<List<MeasureDTO>> listAllForSmartMeterIdForDay(@PathVariable("smartMeterId") String smartMeterId, @PathVariable("date") LocalDate date) {
        return ResponseEntity.ok(measureFacade.listAllForSmartMeterIdForDay(smartMeterId, date));
    }

    @Operation(
            summary = "Remove multiple measures",
            description = "Removes collection of measures, specified by list of IDs",
            security = {@SecurityRequirement(name = SmartMeterManagementApplication.SECURITY_SCHEME_OAUTH2, scopes = {"test_1"}),
                    @SecurityRequirement(name = SmartMeterManagementApplication.SECURITY_SCHEME_BEARER, scopes = {"test_1"})},
            responses = {@ApiResponse(responseCode = "204", description = "Measures removed"),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_1", content = @Content()),}
    )
    @DeleteMapping("/measures")
    @CrossOrigin(origins = "*")
    @Transactional
    public ResponseEntity<List<MeasureDTO>> removeSmartMeter(@RequestParam List<String> measureIds) {
        List<MeasureDTO> removedMeasures = measureFacade.removeMeasures(measureIds);
        return new ResponseEntity<>(removedMeasures, HttpStatus.NO_CONTENT);
    }
}
