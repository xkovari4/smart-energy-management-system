package cz.muni.fi.pa165.epsilon.smartmetermanagement.rest;

import cz.muni.fi.pa165.epsilon.smartmetermanagement.SmartMeterManagementApplication;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.api.SmartMeterDTO;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.exceptions.ValidationException;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.facade.SmartMeterFacade;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.rest.dto.RegisterSmartMeterRequestDTO;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.rest.dto.UpdateSmartMeterRequestDTO;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.rest.exceptionhandling.ApiError;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Spring REST Controller for communication using HTTP protocol and JSON data format.
 */
@RestController
@OpenAPIDefinition(
        info = @Info(title = "Smart Meter service",
                version = "1.1",
                description = """
                        Simple service for managing Smart Meters and collecting measured data from them.
                        The API has operations for:
                        - CRUD operations regarding a single Smart Meter (specified by ID)
                        - getting all Smart Meters for specified house ID
                        """,
                contact = @Contact(name = "Vit Nakladal", email = "485524@mail.muni.cz", url = "https://is.muni.cz/auth/osoba/485524"),
                license = @License(name = "Apache 2.0", url = "https://www.apache.org/licenses/LICENSE-2.0.html")
        ),
        servers = {@Server(url = "http://smart-meter-management:8080", description = "Docker network server"),
                @Server(url = "http://localhost:8082", description = "Localhost network server")}
)
@Tag(name = "SmartMeter", description = "microservice for smart meter")
@RequestMapping(path = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
public class SmartMeterRestController {
    private final SmartMeterFacade smartMeterFacade;

    @Autowired
    public SmartMeterRestController(SmartMeterFacade smartMeterFacade) {
        this.smartMeterFacade = smartMeterFacade;
    }

    @Operation(
            summary = "Register new smart meter",
            description = "Creates a new smart meter by given parameters",
            security = {@SecurityRequirement(name = SmartMeterManagementApplication.SECURITY_SCHEME_OAUTH2, scopes = {"test_write"}),
                    @SecurityRequirement(name = SmartMeterManagementApplication.SECURITY_SCHEME_BEARER, scopes = {"test_write"})},
            responses = {
                    @ApiResponse(responseCode = "200", description = "Smart meter created"),
                    @ApiResponse(responseCode = "400", description = "Smart meter validation failed",
                            content = @Content(schema = @Schema(implementation = ApiError.class))),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_write", content = @Content()),
            }
    )
    @PostMapping(path = "/smart-meters")
    @CrossOrigin(origins = "*")
    @ResponseStatus(HttpStatus.CREATED)
    @Transactional
    public ResponseEntity<SmartMeterDTO> registerSmartMeter(@Valid @RequestBody RegisterSmartMeterRequestDTO r,
                                                            BindingResult bindingResult) {
        checkFieldError(bindingResult);

        SmartMeterDTO newSmartMeter = smartMeterFacade.registerNewSmartMeter(r);
        return new ResponseEntity<>(newSmartMeter, HttpStatus.CREATED);
    }

    @Operation(
            summary = "Get smart meter",
            description = "Returns smart meter by given ID",
            security = {@SecurityRequirement(name = SmartMeterManagementApplication.SECURITY_SCHEME_OAUTH2, scopes = {"test_read"}),
                    @SecurityRequirement(name = SmartMeterManagementApplication.SECURITY_SCHEME_BEARER, scopes = {"test_read"})},
            responses = {
                    @ApiResponse(responseCode = "200", description = "Smart meter found"),
                    @ApiResponse(responseCode = "404", description = "Smart meter not found",
                            content = @Content(schema = @Schema(implementation = ApiError.class))),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_read", content = @Content()),
            }
    )
    @GetMapping("/smart-meters/{smartMeterId}")
    @CrossOrigin(origins = "*")
    @Transactional(readOnly = true)
    public ResponseEntity<SmartMeterDTO> findById(@PathVariable("smartMeterId") String smartMeterId) {
        return ResponseEntity.ok(smartMeterFacade.findById(smartMeterId));
    }

    @Operation(
            summary = "Update smart meter",
            description = "Updates smart meter with given data",
            security = {@SecurityRequirement(name = SmartMeterManagementApplication.SECURITY_SCHEME_OAUTH2, scopes = {"test_write"}),
                    @SecurityRequirement(name = SmartMeterManagementApplication.SECURITY_SCHEME_BEARER, scopes = {"test_write"})},
            responses = {
                    @ApiResponse(responseCode = "200", description = "Smart meter updated"),
                    @ApiResponse(responseCode = "400", description = "Smart meter validation failed",
                            content = @Content(schema = @Schema(implementation = ApiError.class))),
                    @ApiResponse(responseCode = "404", description = "Smart meter not found",
                            content = @Content(schema = @Schema(implementation = ApiError.class))),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_write", content = @Content()),
            }
    )
    @PutMapping("/smart-meters")
    @CrossOrigin(origins = "*")
    @Transactional
    public ResponseEntity<SmartMeterDTO> updateSmartMeter(@Valid @RequestBody UpdateSmartMeterRequestDTO r, BindingResult bindingResult) {
        checkFieldError(bindingResult);

        SmartMeterDTO updatedSm = smartMeterFacade.updateSmartMeter(r);
        return ResponseEntity.ok(updatedSm);
    }

    @Operation(
            summary = "Turn on a smart meter",
            security = {@SecurityRequirement(name = SmartMeterManagementApplication.SECURITY_SCHEME_OAUTH2, scopes = {"test_2"}),
                    @SecurityRequirement(name = SmartMeterManagementApplication.SECURITY_SCHEME_BEARER, scopes = {"test_2"})},
            responses = {
                    @ApiResponse(responseCode = "200", description = "Smart meter power state successfully set"),
                    @ApiResponse(responseCode = "404", description = "Smart meter not found",
                            content = @Content(schema = @Schema(implementation = ApiError.class))),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_2", content = @Content()),
            }
    )
    @PutMapping("/smart-meters/{smartMeterId}/turn-on")
    @CrossOrigin(origins = "*")
    @Transactional
    public ResponseEntity<SmartMeterDTO> turnOnSmartMeter(@PathVariable("smartMeterId") String smartMeterId) {
        return ResponseEntity.ok(smartMeterFacade.turnOnSmartMeter(smartMeterId));
    }

    @Operation(
            summary = "Turn off a smart meter",
            security = {@SecurityRequirement(name = SmartMeterManagementApplication.SECURITY_SCHEME_OAUTH2, scopes = {"test_2"}),
                    @SecurityRequirement(name = SmartMeterManagementApplication.SECURITY_SCHEME_BEARER, scopes = {"test_2"})},
            responses = {
                    @ApiResponse(responseCode = "200", description = "Smart meter power state successfully set"),
                    @ApiResponse(responseCode = "404", description = "Smart meter not found",
                            content = @Content(schema = @Schema(implementation = ApiError.class))),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_2", content = @Content()),
            }
    )
    @PutMapping("/smart-meters/{smartMeterId}/turn-off")
    @CrossOrigin(origins = "*")
    @Transactional
    public ResponseEntity<SmartMeterDTO> turnOffSmartMeter(@PathVariable("smartMeterId") String smartMeterId) {
        return ResponseEntity.ok(smartMeterFacade.turnOffSmartMeter(smartMeterId));
    }

    @Operation(
            summary = "Remove smart meter and all its measures",
            description = "Removes smart meter by given ID, together with all its measures",
            security = {@SecurityRequirement(name = SmartMeterManagementApplication.SECURITY_SCHEME_OAUTH2, scopes = {"test_1"}),
                    @SecurityRequirement(name = SmartMeterManagementApplication.SECURITY_SCHEME_BEARER, scopes = {"test_1"})},
            responses = {
                    @ApiResponse(responseCode = "204", description = "Smart meter removed"),
                    @ApiResponse(responseCode = "404", description = "Smart meter not found",
                            content = @Content(schema = @Schema(implementation = ApiError.class))),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_1", content = @Content()),
            }
    )
    @DeleteMapping("/smart-meters/{smartMeterId}")
    @CrossOrigin(origins = "*")
    @Transactional
    public ResponseEntity<SmartMeterDTO> removeSmartMeter(@PathVariable("smartMeterId") String smartMeterId) {
        SmartMeterDTO removedSm = smartMeterFacade.removeSmartMeter(smartMeterId);
        return new ResponseEntity<>(removedSm, HttpStatus.NO_CONTENT);
    }

    @Operation(
            summary = "Get all smart meters",
            description = "List all smart meters",
            security = {@SecurityRequirement(name = SmartMeterManagementApplication.SECURITY_SCHEME_OAUTH2, scopes = {"test_read"}),
                    @SecurityRequirement(name = SmartMeterManagementApplication.SECURITY_SCHEME_BEARER, scopes = {"test_read"})}
    )
    @GetMapping("/smart-meters")
    @CrossOrigin(origins = "*")
    @Transactional(readOnly = true)
    public ResponseEntity<List<SmartMeterDTO>> listAll() {
        return ResponseEntity.ok(smartMeterFacade.listAll());
    }

    @Operation(
            summary = "Get all smart meters for house",
            description = "List all smart meters having precise houseId",
            security = {@SecurityRequirement(name = SmartMeterManagementApplication.SECURITY_SCHEME_OAUTH2, scopes = {"test_read"}),
                    @SecurityRequirement(name = SmartMeterManagementApplication.SECURITY_SCHEME_BEARER, scopes = {"test_read"})}
    )
    @GetMapping("/smart-meters-for-house/{houseId}")
    @CrossOrigin(origins = "*")
    @Transactional(readOnly = true)
    public ResponseEntity<List<SmartMeterDTO>> listAllForHouseId(@PathVariable("houseId") String houseId) {
        return ResponseEntity.ok(smartMeterFacade.listAllForHouseId(houseId));
    }

    private void checkFieldError(BindingResult bindingResult) throws ValidationException {
        if (bindingResult.hasErrors()) {
            for (FieldError fe : bindingResult.getFieldErrors()) {
                throw new ValidationException(String.format("FieldError: %s; %s", fe.getField(), fe.getDefaultMessage()));
            }
        }
    }
}
