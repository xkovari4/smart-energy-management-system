package cz.muni.fi.pa165.epsilon.smartmetermanagement.rest.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;

import java.util.Objects;

/**
 * Represents JSON-formatted body of HTTP request to createSmartMeter operation.
 */
@Schema(description = "Object for requesting new Smart Meter.")
public class UpdateSmartMeterRequestDTO {

    @Schema(description = "uuid", example = "123e4567-e89b-12d3-a456-426614174000")
    private @NotBlank String id;

    @Schema(description = "Smart Meter's name", example = "TV")
    private @NotBlank String name;

    @Schema(description = "Smart Meter's description", example = "The first from the left in the living room")
    private String description;

    @Schema(description = "IP address to communicate with the Smart Meter", example = "c1ac:5304:9ca0:f8a1:d166:58ff:c86d:fbb6")
    private @NotBlank String ipAddress;

    @Schema(description = "Reference to the house to which the Smart Meter belongs", example = "52df1f2d-ba04-4a15-91e7-af06e7bb2509")
    private @NotBlank String houseId;

    @Schema(description = "State of the Smart Meter (turned on or off)", type = "boolean", example = "false", defaultValue = "false")
    private Boolean isPoweredOn;

    public UpdateSmartMeterRequestDTO() {
    }

    public UpdateSmartMeterRequestDTO(String id, String name, String description, String ipAddress, String houseId, Boolean isPoweredOn) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.ipAddress = ipAddress;
        this.houseId = houseId;
        this.isPoweredOn = isPoweredOn;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getHouseId() {
        return houseId;
    }

    public void setHouseId(String houseId) {
        this.houseId = houseId;
    }

    public Boolean getIsPoweredOn() {
        return isPoweredOn;
    }

    public void setIsPoweredOn(Boolean isPoweredOn) {
        this.isPoweredOn = isPoweredOn;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (UpdateSmartMeterRequestDTO) obj;
        return Objects.equals(this.id, that.id) && Objects.equals(this.name, that.name) && Objects.equals(this.description, that.description) && Objects.equals(this.ipAddress, that.ipAddress) && Objects.equals(this.houseId, that.houseId) && Objects.equals(this.isPoweredOn, that.isPoweredOn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, ipAddress, houseId, isPoweredOn);
    }

    @Override
    public String toString() {
        return "UpdateSmartMeterRequestDTO[" + "id=" + id + ", " + "name=" + name + ", " + "description=" + description + ", " + "ipAddress=" + ipAddress + ", " + "houseId=" + houseId + ", " + "isPoweredOn=" + isPoweredOn + ']';
    }

}
