package cz.muni.fi.pa165.epsilon.smartmetermanagement.service;

import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.model.Measure;

import java.time.LocalDate;
import java.util.List;

public interface MeasureService {
    void collectMeasures();

    List<Measure> listAll();

    List<Measure> listAllForSmartMeterId(String smartMeterId);

    List<Measure> listAllForSmartMeterIdForDay(String smartMeterId, LocalDate date);

    List<Measure> removeMeasures(List<String> measureIds);

}
