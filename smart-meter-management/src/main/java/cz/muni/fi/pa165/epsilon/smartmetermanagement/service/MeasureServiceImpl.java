package cz.muni.fi.pa165.epsilon.smartmetermanagement.service;

import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.model.Measure;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.model.SmartMeter;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.repository.MeasureRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;
import java.util.UUID;

@Service
@EnableScheduling
public class MeasureServiceImpl implements MeasureService {
    private static final Logger log = LoggerFactory.getLogger(MeasureServiceImpl.class);
    private final MeasureRepository measureRepository;
    private final SmartMeterService smartMeterService;

    @Autowired
    public MeasureServiceImpl(MeasureRepository measureRepository, SmartMeterService smartMeterService) {
        this.measureRepository = measureRepository;
        this.smartMeterService = smartMeterService;
    }

    @Override
    @Scheduled(fixedRate = 2000)
    @Transactional
    public void collectMeasures() {
        List<SmartMeter> smartMeters = smartMeterService.listAll();
        List<SmartMeter> smartMetersFiltered = smartMeters.stream()
                .filter(SmartMeter::getIsPoweredOn).toList();
        log.info("Collecting measures for {} number of smart meters", smartMetersFiltered.size());

        List<Measure> measures = smartMetersFiltered.stream()
                .map(this::collectMeasure)
                .toList();
        measureRepository.saveAll(measures);
    }

    private Measure collectMeasure(SmartMeter sm) {
        UUID uuid = UUID.randomUUID();
        Timestamp currentTimestamp = Timestamp.valueOf(LocalDateTime.now());
        double measuredValue = (new Random()).nextDouble(1000);
        return new Measure(uuid.toString(), sm, currentTimestamp, measuredValue);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Measure> listAll() {
        return measureRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Measure> listAllForSmartMeterId(String smartMeterId) {
        return measureRepository.findAllForSmartMeterId(smartMeterId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Measure> listAllForSmartMeterIdForDay(String smartMeterId, LocalDate date) {
        return measureRepository.findAllForSmartMeterIdForDay(smartMeterId, date);
    }

    @Override
    @Transactional
    public List<Measure> removeMeasures(List<String> measureIds) {
        List<Measure> measuresToRemove = measureRepository.findAllById(measureIds);
        measureRepository.deleteAll(measuresToRemove);
        return measuresToRemove;
    }
}
