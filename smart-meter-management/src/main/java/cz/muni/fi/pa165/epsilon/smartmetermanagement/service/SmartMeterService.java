package cz.muni.fi.pa165.epsilon.smartmetermanagement.service;

import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.model.SmartMeter;

import java.util.List;

public interface SmartMeterService {
    SmartMeter registerNewSmartMeter(SmartMeter newSmartMeter);

    SmartMeter findById(String smartMeterId);

    SmartMeter updateSmartMeter(SmartMeter newSmartMeter);

    SmartMeter removeSmartMeter(String smartMeterId);

    List<SmartMeter> listAll();

    List<SmartMeter> listAllForHouseId(String houseId);

    SmartMeter turnOnSmartMeter(String smartMeterId);

    SmartMeter turnOffSmartMeter(String smartMeterId);
}
