package cz.muni.fi.pa165.epsilon.smartmetermanagement.service;

import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.model.SmartMeter;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.repository.SmartMeterRepository;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class SmartMeterServiceImpl implements SmartMeterService {
    private final SmartMeterRepository smartMeterRepository;

    @Autowired
    public SmartMeterServiceImpl(SmartMeterRepository smartMeterRepository) {
        this.smartMeterRepository = smartMeterRepository;
    }

    @Override
    public SmartMeter registerNewSmartMeter(SmartMeter newSmartMeter) {
        UUID uuid = UUID.randomUUID();
        newSmartMeter.setId(uuid.toString());
        return smartMeterRepository.save(newSmartMeter);
    }

    @Override
    @Transactional(readOnly = true)
    public SmartMeter findById(String smartMeterId) {
        return smartMeterRepository.findById(smartMeterId)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Smart meter with id: %s not found.", smartMeterId)));
    }

    @Override
    public SmartMeter updateSmartMeter(SmartMeter newSmartMeter) {
        SmartMeter oldSmartMeter = findById(newSmartMeter.getId());
        smartMeterRepository.save(newSmartMeter);

        if (newSmartMeter.getIsPoweredOn() != null && !newSmartMeter.getIsPoweredOn().equals(oldSmartMeter.getIsPoweredOn())) {
            if (newSmartMeter.getIsPoweredOn()) {
                turnOn(newSmartMeter);
            } else {
                turnOff(newSmartMeter);
            }
        }
        return newSmartMeter;
    }

    @SuppressWarnings("unused")
    private void turnOn(SmartMeter newSmartMeter) {
        // Intentionally unimplemented
    }

    @SuppressWarnings("unused")
    private void turnOff(SmartMeter newSmartMeter) {
        // Intentionally unimplemented
    }

    @Override
    public SmartMeter removeSmartMeter(String smartMeterId) {
        SmartMeter smToDelete = findById(smartMeterId);
        smartMeterRepository.deleteById(smartMeterId);
        return smToDelete;
    }

    @Override
    @Transactional(readOnly = true)
    public List<SmartMeter> listAll() {
        return smartMeterRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public List<SmartMeter> listAllForHouseId(String houseId) {
        return smartMeterRepository.findAllForHouseId(houseId);
    }

    @Override
    public SmartMeter turnOnSmartMeter(String smartMeterId) {
        SmartMeter oldSmartMeter = findById(smartMeterId);
        SmartMeter newSmartMeter = new SmartMeter(smartMeterId, oldSmartMeter.getName(), oldSmartMeter.getDescription(),
                oldSmartMeter.getIpAddress(), oldSmartMeter.getHouseId(), true, oldSmartMeter.getMeasures());
        smartMeterRepository.save(newSmartMeter);
        return newSmartMeter;
    }

    @Override
    public SmartMeter turnOffSmartMeter(String smartMeterId) {
        SmartMeter oldSmartMeter = findById(smartMeterId);
        SmartMeter newSmartMeter = new SmartMeter(smartMeterId, oldSmartMeter.getName(), oldSmartMeter.getDescription(),
                oldSmartMeter.getIpAddress(), oldSmartMeter.getHouseId(), false, oldSmartMeter.getMeasures());
        smartMeterRepository.save(newSmartMeter);
        return newSmartMeter;
    }
}
