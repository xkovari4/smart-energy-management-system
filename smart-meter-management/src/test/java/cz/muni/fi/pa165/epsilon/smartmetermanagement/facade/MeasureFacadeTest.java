package cz.muni.fi.pa165.epsilon.smartmetermanagement.facade;

import cz.muni.fi.pa165.epsilon.smartmetermanagement.api.MeasureDTO;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.mappers.MeasureMapper;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.service.MeasureService;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.util.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class MeasureFacadeTest {

    @Mock
    private MeasureService measureService;

    @Mock
    private MeasureMapper measureMapper;

    @InjectMocks
    private MeasureFacade measureFacade;

    @Test
    void listAll_measuresFound_returnListOfMeasureDTO() {
        // Arrange
        var expectedMeasures = Collections.singletonList(TestDataFactory.measureDTO);
        when(measureService.listAll()).thenReturn(Collections.singletonList(TestDataFactory.measureEntity));
        when(measureMapper.mapToList(any())).thenReturn(Collections.singletonList(TestDataFactory.measureDTO));

        // Act
        List<MeasureDTO> result = measureFacade.listAll();

        // Assert
        assertThat(expectedMeasures).isEqualTo(result);
    }

    @Test
    void listAll_measuresNotFound_returnEmptyList() {
        // Arrange
        when(measureService.listAll()).thenReturn(new ArrayList<>());
        when(measureMapper.mapToList(any())).thenReturn(new ArrayList<>());

        // Act
        List<MeasureDTO> result = measureFacade.listAll();

        // Assert
        assertTrue(result.isEmpty());
    }

    @Test
    void listAllForSmartMeterId_idExistsSmartMetersFound_returnListOfMeasureDTO() {
        // Arrange
        String smartMeterId = TestDataFactory.measureEntity.getSmartMeter().getId();
        List<MeasureDTO> expectedMeasures = Collections.singletonList(TestDataFactory.measureDTO);
        when(measureService.listAllForSmartMeterId(smartMeterId)).thenReturn(Collections.singletonList(TestDataFactory.measureEntity));
        when(measureMapper.mapToList(any())).thenReturn(expectedMeasures);

        // Act
        List<MeasureDTO> result = measureFacade.listAllForSmartMeterId(smartMeterId);

        // Assert
        assertThat(expectedMeasures).isEqualTo(result);
    }

    @Test
    void listAllForSmartMeterId_idDoesNotExistSmartMetersNotFound_returnEmptyList() {
        // Arrange
        String smartMeterId = "nonExistentId";
        when(measureService.listAllForSmartMeterId(smartMeterId)).thenReturn(new ArrayList<>());
        when(measureMapper.mapToList(any())).thenReturn(new ArrayList<>());

        // Act
        List<MeasureDTO> result = measureFacade.listAllForSmartMeterId(smartMeterId);

        // Assert
        assertTrue(result.isEmpty());
    }

    @Test
    void listAllForSmartMeterIdForDay_idExistsSmartMetersFound_returnListOfMeasureDTO() {
        // Arrange
        String smartMeterId = TestDataFactory.measureEntity.getSmartMeter().getId();
        LocalDate date = TestDataFactory.measureEntity.getTimestamp().toLocalDateTime().toLocalDate();
        List<MeasureDTO> expectedMeasures = Collections.singletonList(TestDataFactory.measureDTO);
        when(measureService.listAllForSmartMeterIdForDay(smartMeterId, date)).thenReturn(Collections.singletonList(TestDataFactory.measureEntity));
        when(measureMapper.mapToList(any())).thenReturn(expectedMeasures);

        // Act
        List<MeasureDTO> result = measureFacade.listAllForSmartMeterIdForDay(smartMeterId, date);

        // Assert
        assertEquals(expectedMeasures, result);
    }

    @Test
    void listAllForSmartMeterIdForDay_idDoesNotExists_returnEmptyList() {
        // Arrange
        String smartMeterId = "nonExistentId";
        LocalDate date = LocalDate.now();
        List<MeasureDTO> expectedMeasures = new ArrayList<>();
        when(measureService.listAllForSmartMeterIdForDay(smartMeterId, date)).thenReturn(new ArrayList<>());
        when(measureMapper.mapToList(any())).thenReturn(new ArrayList<>());

        // Act
        List<MeasureDTO> result = measureFacade.listAllForSmartMeterIdForDay(smartMeterId, date);

        // Assert
        assertEquals(expectedMeasures, result);
    }

    @Test
    void listAllForSmartMeterId_nullSmartMeterId_returnEmptyList() {
        // Arrange
        String smartMeterId = null;
        when(measureService.listAllForSmartMeterId(smartMeterId)).thenReturn(new ArrayList<>());
        when(measureMapper.mapToList(any())).thenReturn(new ArrayList<>());

        // Act
        List<MeasureDTO> result = measureFacade.listAllForSmartMeterId(smartMeterId);

        // Assert
        assertTrue(result.isEmpty());
    }

    @Test
    void listAllForSmartMeterIdForDay_nullDate_returnEmptyList() {
        // Arrange
        String smartMeterId = TestDataFactory.measureEntity.getSmartMeter().getId();
        LocalDate date = null;
        when(measureService.listAllForSmartMeterIdForDay(smartMeterId, date)).thenReturn(new ArrayList<>());
        when(measureMapper.mapToList(any())).thenReturn(new ArrayList<>());

        // Act
        List<MeasureDTO> result = measureFacade.listAllForSmartMeterIdForDay(smartMeterId, date);

        // Assert
        assertTrue(result.isEmpty());
    }


    @Test
    void removeMeasures_listWithExistingIdsMeasuresFound_returnMeasureDTOList() {
        // Arrange
        List<String> measureIds = Collections.singletonList(TestDataFactory.measureEntity.getSmartMeter().getId());
        List<MeasureDTO> expectedMeasures = Collections.singletonList(TestDataFactory.measureDTO);
        when(measureService.removeMeasures(measureIds)).thenReturn(Collections.singletonList(TestDataFactory.measureEntity));
        when(measureMapper.mapToList(any())).thenReturn(expectedMeasures);

        // Act
        List<MeasureDTO> result = measureFacade.removeMeasures(measureIds);

        // Assert
        assertEquals(expectedMeasures, result);
    }

    @Test
    void removeMeasures_listWithExistingIdsMeasuresFound_returnEmptyList() {
        // Arrange
        List<String> measureIds = new ArrayList<>();
        measureIds.add(TestDataFactory.measureEntity.getSmartMeter().getId());
        List<MeasureDTO> expectedMeasures = Collections.singletonList(TestDataFactory.measureDTO);
        when(measureService.removeMeasures(measureIds)).thenReturn(Collections.singletonList(TestDataFactory.measureEntity));
        when(measureMapper.mapToList(any())).thenReturn(Collections.singletonList(TestDataFactory.measureDTO));

        // Act
        List<MeasureDTO> result = measureFacade.removeMeasures(measureIds);

        // Assert
        assertEquals(expectedMeasures, result);
    }

    @Test
    void removeMeasures_emptyIdListNoMeasuresFound_returnEmptyList() {
        // Arrange
        List<String> measureIds = new ArrayList<>();
        List<MeasureDTO> expectedMeasures = new ArrayList<>();
        when(measureService.removeMeasures(measureIds)).thenReturn(new ArrayList<>());
        when(measureMapper.mapToList(any())).thenReturn(new ArrayList<>());

        // Act
        List<MeasureDTO> result = measureFacade.removeMeasures(measureIds);

        // Assert
        assertEquals(expectedMeasures, result);
    }

    @Test
    void removeMeasures_nullMeasureIds_returnEmptyList() {
        // Arrange
        List<String> measureIds = null;
        when(measureService.removeMeasures(measureIds)).thenReturn(new ArrayList<>());
        when(measureMapper.mapToList(any())).thenReturn(new ArrayList<>());

        // Act
        List<MeasureDTO> result = measureFacade.removeMeasures(measureIds);

        // Assert
        assertTrue(result.isEmpty());
    }
}