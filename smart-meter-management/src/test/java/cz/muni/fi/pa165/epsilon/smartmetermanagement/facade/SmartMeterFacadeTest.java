package cz.muni.fi.pa165.epsilon.smartmetermanagement.facade;

import cz.muni.fi.pa165.epsilon.smartmetermanagement.api.SmartMeterDTO;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.model.SmartMeter;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.mappers.SmartMeterMapper;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.rest.dto.RegisterSmartMeterRequestDTO;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.rest.dto.UpdateSmartMeterRequestDTO;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.service.SmartMeterService;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.util.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class SmartMeterFacadeTest {

    @Mock
    private SmartMeterService smartMeterService;

    @Mock
    private SmartMeterMapper smartMeterMapper;

    @InjectMocks
    private SmartMeterFacade smartMeterFacade;

    @Test
    void registerNewSmartMeter_validInput_returnNewSmartMeterDTO() {
        // Arrange
        RegisterSmartMeterRequestDTO requestDto = new RegisterSmartMeterRequestDTO();
        requestDto.setName("TV");
        requestDto.setDescription("The first from the left in the living room");
        requestDto.setIpAddress("c1ac:5304:9ca0:f8a1:d166:58ff:c86d:fbb6");
        requestDto.setHouseId("52df1f2d-ba04-4a15-91e7-af06e7bb2509");
        requestDto.setIsPoweredOn(false);

        SmartMeter smartMeter = smartMeterMapper.mapToSmartMeter(requestDto);

        SmartMeterDTO expectedSmartMeterDTO = smartMeterMapper.mapToDto(smartMeter);

        when(smartMeterMapper.mapToSmartMeter(requestDto)).thenReturn(smartMeter);
        when(smartMeterService.registerNewSmartMeter(smartMeter)).thenReturn(smartMeter);
        when(smartMeterMapper.mapToDto(smartMeter)).thenReturn(expectedSmartMeterDTO);

        // Act
        SmartMeterDTO result = smartMeterFacade.registerNewSmartMeter(requestDto);

        // Assert
        assertThat(expectedSmartMeterDTO).isEqualTo(result);
        verify(smartMeterMapper, times(2)).mapToSmartMeter(requestDto);
        verify(smartMeterService).registerNewSmartMeter(smartMeter);
        verify(smartMeterMapper, times(2)).mapToDto(smartMeter);
    }

    @Test
    void findById_idExistsMeterFound_returnSmartMeterDTO() {
        String smartMeterId = TestDataFactory.smartMeterEntity.getId();
        var name = TestDataFactory.smartMeterEntity.getName();
        var description = TestDataFactory.smartMeterEntity.getDescription();
        var ipAddress = TestDataFactory.smartMeterEntity.getIpAddress();
        var houseId = TestDataFactory.smartMeterEntity.getHouseId();
        var isPoweredOn = TestDataFactory.smartMeterEntity.getIsPoweredOn();
        SmartMeterDTO expectedSmartMeterDTO = new SmartMeterDTO(smartMeterId, name, description, ipAddress, houseId, isPoweredOn);
        when(smartMeterMapper.mapToDto(any())).thenReturn(expectedSmartMeterDTO);
        when(smartMeterService.findById(anyString())).thenReturn(TestDataFactory.smartMeterEntity);

        SmartMeterDTO result = smartMeterFacade.findById(smartMeterId);

        assertThat(expectedSmartMeterDTO).isEqualTo(result);
    }

    @Test
    void findById_idDoesNotExistMeterNotFound_throwsResourceNotFoundException() {
        String smartMeterId = "nonExistentId";
        when(smartMeterService.findById(anyString())).thenThrow(ResourceNotFoundException.class);

        assertThrows(ResourceNotFoundException.class, () -> smartMeterFacade.findById(smartMeterId));
    }

    @Test
    void updateSmartMeter_idFound_returnSmartMeterDTO() {
        // Arrange
        UpdateSmartMeterRequestDTO requestDto = new UpdateSmartMeterRequestDTO();
        requestDto.setId("123e4567-e89b-12d3-a456-426614174000");
        requestDto.setName("TV");
        requestDto.setDescription("The first from the left in the living room");
        requestDto.setIpAddress("c1ac:5304:9ca0:f8a1:d166:58ff:c86d:fbb6");
        requestDto.setHouseId("52df1f2d-ba04-4a15-91e7-af06e7bb2509");
        requestDto.setIsPoweredOn(false);

        SmartMeter smartMeterEntity = smartMeterMapper.mapToSmartMeterUpdate(requestDto);

        SmartMeterDTO expectedSmartMeterDTO = smartMeterMapper.mapToDto(smartMeterEntity);

        when(smartMeterMapper.mapToSmartMeterUpdate(requestDto)).thenReturn(smartMeterEntity);
        when(smartMeterService.updateSmartMeter(smartMeterEntity)).thenReturn(smartMeterEntity);
        when(smartMeterMapper.mapToDto(smartMeterEntity)).thenReturn(expectedSmartMeterDTO);

        // Act
        SmartMeterDTO result = smartMeterFacade.updateSmartMeter(requestDto);

        // Assert
        assertThat(expectedSmartMeterDTO).isEqualTo(result);
        verify(smartMeterMapper, times(2)).mapToSmartMeterUpdate(requestDto);
        verify(smartMeterService).updateSmartMeter(smartMeterEntity);
        verify(smartMeterMapper, times(2)).mapToDto(smartMeterEntity);
    }

    @Test
    void turnOnSmartMeter_turnOnSmartMeterCalled_returnSmartMeterDTO() {
        when(smartMeterMapper.mapToDto(any())).thenReturn(TestDataFactory.smartMeterDTO);
        when(smartMeterService.turnOnSmartMeter(anyString()))
                .thenReturn(TestDataFactory.smartMeterEntity);

        smartMeterFacade.turnOnSmartMeter(TestDataFactory.smartMeterEntity.getId());

        verify(smartMeterService).turnOnSmartMeter(TestDataFactory.smartMeterEntity.getId());
    }

    @Test
    void turnOffSmartMeter_turnOffSmartMeterCalled_returnSmartMeterDTO() {
        when(smartMeterMapper.mapToDto(any())).thenReturn(TestDataFactory.smartMeterDTO);
        when(smartMeterService.turnOffSmartMeter(anyString()))
                .thenReturn(TestDataFactory.smartMeterEntity);

        smartMeterFacade.turnOffSmartMeter(TestDataFactory.smartMeterEntity.getId());

        verify(smartMeterService).turnOffSmartMeter(TestDataFactory.smartMeterEntity.getId());
    }

    @Test
    void removeSmartMeter_smartMeterFound_returnSmartMeterDTO() {
        when(smartMeterMapper.mapToDto(any())).thenReturn(TestDataFactory.smartMeterDTO);
        when(smartMeterService.removeSmartMeter(anyString())).thenReturn(TestDataFactory.smartMeterEntity);

        SmartMeterDTO result = smartMeterFacade.removeSmartMeter(TestDataFactory.smartMeterEntity.getId());

        assertThat(TestDataFactory.smartMeterDTO).isEqualTo(result);
    }

    @Test
    void removeSmartMeter_smartMeterNotFound_throwsResourceNotFoundException() {
        String smartMeterId = "nonExistentId";
        when(smartMeterService.removeSmartMeter(anyString())).thenThrow(ResourceNotFoundException.class);

        assertThrows(ResourceNotFoundException.class, () -> smartMeterFacade.removeSmartMeter(smartMeterId));
    }

    @Test
    void listAll_returnsListOfSmartMeterDTOs() {
        when(smartMeterService.listAll()).thenReturn(Collections.singletonList(TestDataFactory.smartMeterEntity));

        List<SmartMeterDTO> expectedSmartMeterDTOs = Collections.singletonList(TestDataFactory.smartMeterDTO);
        when(smartMeterMapper.mapToList(any())).thenReturn(expectedSmartMeterDTOs);

        List<SmartMeterDTO> result = smartMeterFacade.listAll();

        assertThat(expectedSmartMeterDTOs).isEqualTo(result);
    }

    @Test
    void listAll_noSmartMetersFound_returnsEmptyList() {
        when(smartMeterService.listAll()).thenReturn(Collections.emptyList());
        when(smartMeterMapper.mapToList(any())).thenReturn(Collections.emptyList());

        List<SmartMeterDTO> result = smartMeterFacade.listAll();

        assertThat(result).isEmpty();
    }

    @Test
    void listAllForHouseId_returnsListOfSmartMeterDTOs() {
        var houseId = TestDataFactory.smartMeterEntity.getHouseId();
        when(smartMeterService.listAllForHouseId(houseId)).thenReturn(Collections.singletonList(TestDataFactory.smartMeterEntity));

        List<SmartMeterDTO> expectedSmartMeterDTOs = Collections.singletonList(TestDataFactory.smartMeterDTO);
        when(smartMeterMapper.mapToList(any())).thenReturn(expectedSmartMeterDTOs);

        List<SmartMeterDTO> result = smartMeterFacade.listAllForHouseId(houseId);

        assertThat(expectedSmartMeterDTOs).isEqualTo(result);
    }

    @Test
    void listAllForHouseId_noSmartMetersFound_returnsEmptyList() {
        var houseId = "nonExistentHouseId";
        when(smartMeterService.listAllForHouseId(houseId)).thenReturn(Collections.emptyList());
        when(smartMeterMapper.mapToList(any())).thenReturn(Collections.emptyList());

        List<SmartMeterDTO> result = smartMeterFacade.listAllForHouseId(houseId);

        assertThat(result).isEmpty();
    }
}
