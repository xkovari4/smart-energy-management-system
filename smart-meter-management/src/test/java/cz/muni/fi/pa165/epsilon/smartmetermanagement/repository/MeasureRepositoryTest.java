package cz.muni.fi.pa165.epsilon.smartmetermanagement.repository;

import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.model.Measure;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.model.SmartMeter;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.repository.MeasureRepository;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.repository.SmartMeterRepository;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.util.TestDataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class MeasureRepositoryTest {

    @Autowired
    private MeasureRepository measureRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    private SmartMeter smartMeter;

    @BeforeEach
    void beforeEach() {
        smartMeter = TestDataFactory.smartMeterEntity;
        smartMeter = testEntityManager.merge(smartMeter);
        testEntityManager.flush();
    }

    @Test
    void findAllForSmartMeterId_meterFound_returnsMeasuresList() {
        // Arrange
        var measure = new Measure();
        measure.setSmartMeter(smartMeter);
        testEntityManager.persistAndFlush(measure);

        // Act
        List<Measure> measures = measureRepository.findAllForSmartMeterId(smartMeter.getId());

        // Assert
        assertThat(measures).hasSize(1);
    }

    @Test
    void findAllForSmartMeterId_meterNotFound_returnsEmptyList() {
        // Act
        List<Measure> measures = measureRepository.findAllForSmartMeterId("nonExistentId");

        // Assert
        assertThat(measures).isEmpty();
    }

    @Test
    void findAllForSmartMeterIdForDay_meterFoundAndDayFound_returnsMeasuresList() {
        // Arrange
        var measure = new Measure();
        measure.setSmartMeter(smartMeter);
        var localDate = LocalDate.now().atStartOfDay();
        var timestamp = Timestamp.valueOf(localDate);
        measure.setTimestamp(timestamp);
        testEntityManager.persistAndFlush(measure);

        // Act
        List<Measure> measures = measureRepository.findAllForSmartMeterIdForDay(smartMeter.getId(), localDate.toLocalDate());

        // Assert
        assertThat(measures).hasSize(1);
    }

    @Test
    void findAllForSmartMeterIdForDay_meterFoundAndDayNotFound_returnsEmptyList() {
        // Arrange
        var measure = new Measure();
        measure.setSmartMeter(smartMeter);
        var timestamp = Timestamp.valueOf(LocalDate.now().atStartOfDay());
        measure.setTimestamp(timestamp);
        testEntityManager.persistAndFlush(measure);

        // Act
        List<Measure> measures = measureRepository.findAllForSmartMeterIdForDay(smartMeter.getId(), LocalDate.now().minusMonths(1));

        // Assert
        assertThat(measures).isEmpty();
    }

    @Test
    void findAllForSmartMeterIdForDay_meterNotFound_returnsEmptyList() {
        // Act
        List<Measure> measures = measureRepository.findAllForSmartMeterIdForDay("nonExistentId", LocalDate.now());

        // Assert
        assertThat(measures).isEmpty();
    }
}
