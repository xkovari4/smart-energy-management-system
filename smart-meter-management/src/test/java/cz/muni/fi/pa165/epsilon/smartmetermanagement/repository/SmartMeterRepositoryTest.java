package cz.muni.fi.pa165.epsilon.smartmetermanagement.repository;

import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.model.SmartMeter;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.repository.SmartMeterRepository;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.util.TestDataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class SmartMeterRepositoryTest {

    @Autowired
    private SmartMeterRepository smartMeterRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    private SmartMeter smartMeter;

    @BeforeEach
    void beforeEach() {
        smartMeter = TestDataFactory.smartMeterEntity;
        smartMeter = testEntityManager.merge(smartMeter);
        testEntityManager.flush();
    }

    @Test
    public void findAllForHouseId_meterFound_returnsListWithOneSmartMeter() {
        // Act
        List<SmartMeter> smartMeters = smartMeterRepository.findAllForHouseId(smartMeter.getHouseId());

        // Assert
        assertThat(smartMeters).hasSize(1);
    }

    @Test
    public void findAllForHouseId_meterNotFound_returnsEmptyList() {
        // Act
        List<SmartMeter> smartMeters = smartMeterRepository.findAllForHouseId("nonExistentHouseId");

        // Assert
        assertThat(smartMeters).isEmpty();
    }
}
