package cz.muni.fi.pa165.epsilon.smartmetermanagement.rest;

import cz.muni.fi.pa165.epsilon.smartmetermanagement.api.MeasureDTO;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.facade.MeasureFacade;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.util.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static cz.muni.fi.pa165.epsilon.smartmetermanagement.util.TestDataFactory.measureDTO;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MeasureRestControllerTest {

    @Mock
    private MeasureFacade measureFacade;

    @InjectMocks
    private MeasuresRestController measuresRestController;

    @Test
    void listAll_oneEntity_returnMeasuresList() {
        // Arrange
        when(measureFacade.listAll()).thenReturn(List.of(measureDTO));

        // Act
        ResponseEntity<List<MeasureDTO>> response = measuresRestController.listAll();

        // Assert
        assertThat(response.hasBody()).isEqualTo(true);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(List.of(measureDTO));
    }

    @Test
    void listAll_noEntities_returnEmptyList() {
        // Arrange
        when(measureFacade.listAll()).thenReturn(List.of());

        // Act
        ResponseEntity<List<MeasureDTO>> response = measuresRestController.listAll();

        // Assert
        assertThat(response.hasBody()).isEqualTo(true);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(List.of());
    }

    @Test
    void listAllForSmartMeterId_oneEntity_returnMeasuresList() {
        // Arrange
        var id = TestDataFactory.measureEntity.getSmartMeter().getId();
        when(measureFacade.listAllForSmartMeterId(id)).thenReturn(List.of(measureDTO));

        // Act
        ResponseEntity<List<MeasureDTO>> response = measuresRestController.listAllForSmartMeterId(id);

        // Assert
        assertThat(response.hasBody()).isEqualTo(true);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(List.of(measureDTO));
    }

    @Test
    void listAllForSmartMeterId_noEntities_returnEmptyList() {
        // Arrange
        var id = TestDataFactory.measureEntity.getSmartMeter().getId();
        when(measureFacade.listAllForSmartMeterId(id)).thenReturn(List.of());

        // Act
        ResponseEntity<List<MeasureDTO>> response = measuresRestController.listAllForSmartMeterId(id);

        // Assert
        assertThat(response.hasBody()).isEqualTo(true);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(List.of());
    }

    @Test
    void listAllForSmartMeterIdForDay_oneEntity_returnMeasuresList() {
        // Arrange
        var id = TestDataFactory.measureEntity.getSmartMeter().getId();
        var date = TestDataFactory.measureEntity.getTimestamp().toLocalDateTime().toLocalDate();
        when(measureFacade.listAllForSmartMeterIdForDay(id, date)).thenReturn(List.of(measureDTO));

        // Act
        ResponseEntity<List<MeasureDTO>> response = measuresRestController.listAllForSmartMeterIdForDay(id, date);

        // Assert
        assertThat(response.hasBody()).isEqualTo(true);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(List.of(measureDTO));
    }

    @Test
    void listAllForSmartMeterIdForDay_noEntities_returnEmptyList() {
        // Arrange
        var id = TestDataFactory.measureEntity.getSmartMeter().getId();
        var date = TestDataFactory.measureEntity.getTimestamp().toLocalDateTime().toLocalDate();
        when(measureFacade.listAllForSmartMeterIdForDay(id, date)).thenReturn(List.of());

        // Act
        ResponseEntity<List<MeasureDTO>> response = measuresRestController.listAllForSmartMeterIdForDay(id, date);

        // Assert
        assertThat(response.hasBody()).isEqualTo(true);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(List.of());
    }

    @Test
    void removeSmartMeter_measuresExist_returnMeasuresList() {
        // Arrange
        var id = TestDataFactory.measureEntity.getSmartMeter().getId();
        when(measureFacade.removeMeasures(List.of(id))).thenReturn(List.of(measureDTO));
        // Act
        ResponseEntity<List<MeasureDTO>> response = measuresRestController.removeSmartMeter(List.of(id));

        // Assert
        assertThat(response.hasBody()).isEqualTo(true);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
        assertThat(response.getBody()).isEqualTo(List.of(measureDTO));
    }

    @Test
    void removeSmartMeter_noMeasuresExist_returnEmptyList() {
        // Arrange
        var id = TestDataFactory.measureEntity.getSmartMeter().getId();
        when(measureFacade.removeMeasures(List.of(id))).thenReturn(List.of());

        // Act
        ResponseEntity<List<MeasureDTO>> response = measuresRestController.removeSmartMeter(List.of(id));

        // Assert
        assertThat(response.hasBody()).isEqualTo(true);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
        assertThat(response.getBody()).isEqualTo(List.of());
    }
}
