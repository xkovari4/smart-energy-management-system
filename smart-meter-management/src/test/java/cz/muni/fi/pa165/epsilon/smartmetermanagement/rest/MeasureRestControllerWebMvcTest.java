package cz.muni.fi.pa165.epsilon.smartmetermanagement.rest;

import cz.muni.fi.pa165.epsilon.smartmetermanagement.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.facade.MeasureFacade;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.util.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.List;

import static cz.muni.fi.pa165.epsilon.smartmetermanagement.util.TestDataFactory.measureDTO;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@WithMockUser(authorities = {"SCOPE_test_write", "SCOPE_test_read", "SCOPE_test_1", "SCOPE_test_2"})
@AutoConfigureMockMvc
public class MeasureRestControllerWebMvcTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MeasureFacade measureFacade;

    @Test
    void listAll_measuresFound_returnsMeasures() throws Exception {
        // Arrange
        when(measureFacade.listAll()).thenReturn(List.of(measureDTO));

        // Act & assert
        mockMvc.perform(get("/api/measures")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(1))
                .andExpect(jsonPath("$[0].id").value("1"));

        verify(measureFacade).listAll();
    }

    @Test
    void listAll_noMeasuresFound_returnsEmptyList() throws Exception {
        // Arrange
        when(measureFacade.listAll()).thenReturn(List.of());

        // Act & Assert
        mockMvc.perform(get("/api/measures")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(0));

        verify(measureFacade).listAll();
    }

    @Test
    void listAllForSmartMeterId_measuresFound_returnMeasures() throws Exception {
        // Arrange
        var id = TestDataFactory.measureEntity.getSmartMeter().getId();
        Mockito.when(measureFacade.listAllForSmartMeterId(id)).thenReturn(List.of(measureDTO));

        // Act
        mockMvc.perform(get("/api/measures/{smartMeterId}", id)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(1))
                .andExpect(jsonPath("$[0].id").value("1"))
                .andExpect(jsonPath("$[0].smartMeterId").value("1"));

        // Assert
        verify(measureFacade).listAllForSmartMeterId(id);
    }

    @Test
    void listAllForSmartMeterId_noMeasuresFound_returnEmptyList() throws Exception {
        // Arrange
        var id = TestDataFactory.measureEntity.getSmartMeter().getId();
        when(measureFacade.listAllForSmartMeterId(id)).thenReturn(List.of());

        // Act & Assert
        mockMvc.perform(get("/api/measures/{smartMeterId}", id)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(0));

        verify(measureFacade).listAllForSmartMeterId(id);
    }

    @Test
    void listAllForSmartMeterIdForDay_invalidSmartMeterId_throwsResourceNotFoundException() throws Exception {
        // Arrange
        var invalidId = "invalidSmartMeterId";
        var date = LocalDate.now();
        when(measureFacade.listAllForSmartMeterIdForDay(invalidId, date)).thenThrow(new ResourceNotFoundException("Smart meter not found"));

        // Act & Assert
        mockMvc.perform(get("/api/measures/{smartMeterId}/{date}", invalidId, date)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.status").value("NOT_FOUND"))
                .andExpect(jsonPath("$.message").value("Smart meter not found"));

        verify(measureFacade).listAllForSmartMeterIdForDay(invalidId, date);
    }

    @Test
    void listAllForSmartMeterIdForDay_measuresFound_returnMeasures() throws Exception {
        // Arrange
        var id = TestDataFactory.measureEntity.getSmartMeter().getId();
        var date = TestDataFactory.measureEntity.getTimestamp().toLocalDateTime().toLocalDate();
        Mockito.when(measureFacade.listAllForSmartMeterIdForDay(id, date)).thenReturn(List.of(measureDTO));

        // Act
        mockMvc.perform(get("/api/measures/{smartMeterId}/{date}", id, date)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(1))
                .andExpect(jsonPath("$[0].id").value("1"))
                .andExpect(jsonPath("$[0].smartMeterId").value("1"));

        // Assert
        verify(measureFacade).listAllForSmartMeterIdForDay(id, date);
    }

    @Test
    void listAllForSmartMeterIdForDay_noMeasuresFound_returnEmptyList() throws Exception {
        // Arrange
        var id = TestDataFactory.measureEntity.getSmartMeter().getId();
        var date = TestDataFactory.measureEntity.getTimestamp().toLocalDateTime().toLocalDate();
        when(measureFacade.listAllForSmartMeterIdForDay(id, date)).thenReturn(List.of());

        // Act & Assert
        mockMvc.perform(get("/api/measures/{smartMeterId}/{date}", id, date)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(0));

        verify(measureFacade).listAllForSmartMeterIdForDay(id, date);
    }

    @Test
    void removeSmartMeter_meterDeleted_returnMeasures() throws Exception {
        mockMvc.perform(delete("/api/measures?measureIds=1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        verify(measureFacade).removeMeasures(List.of(TestDataFactory.measureEntity.getSmartMeter().getId()));
    }

    @Test
    void removeSmartMeter_invalidSmartMeterId_throwsResourceNotFoundException() throws Exception {
        // Arrange
        var invalidId = "invalidSmartMeterId";
        when(measureFacade.removeMeasures(List.of(invalidId))).thenThrow(new ResourceNotFoundException("Smart meter not found"));

        // Act & Assert
        mockMvc.perform(delete("/api/measures?measureIds={measureId}", invalidId)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.status").value("NOT_FOUND"))
                .andExpect(jsonPath("$.message").value("Smart meter not found"));

        verify(measureFacade).removeMeasures(List.of(invalidId));
    }
}
