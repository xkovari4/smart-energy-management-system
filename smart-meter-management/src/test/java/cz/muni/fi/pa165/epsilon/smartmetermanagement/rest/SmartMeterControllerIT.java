package cz.muni.fi.pa165.epsilon.smartmetermanagement.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.api.SmartMeterDTO;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.model.SmartMeter;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.repository.MeasureRepository;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.repository.SmartMeterRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@WithMockUser(authorities = {"SCOPE_test_write", "SCOPE_test_read", "SCOPE_test_1", "SCOPE_test_2"})
@AutoConfigureMockMvc
@Transactional
public class SmartMeterControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private SmartMeterRepository smartMeterRepository;

    @Autowired
    private MeasureRepository measureRepository;

    @BeforeEach
    void beforeEach() {
        measureRepository.deleteAll();
        smartMeterRepository.deleteAll();
    }

    @Test
    void findById_smartMeterNotFound_returnsSmartMeterNotFound() throws Exception {
        // Act
        String responseJson = mockMvc.perform(get("/api/smart-meters/{smartMeterId}", "dfa44846IOUhlfkh-w87q09rupfdshj")
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        SmartMeterDTO response = objectMapper.readValue(responseJson, SmartMeterDTO.class);

        // Assert
        assertThat(response.getId()).isNull();
        assertThat(response.getName()).isNull();
        assertThat(response.getDescription()).isNull();
        assertThat(response.getHouseId()).isNull();
        assertThat(response.getIpAddress()).isNull();
        assertThat(response.getIsPoweredOn()).isFalse();
    }

    @Test
    void listAllSmartMeters_smartMetersFound_returnsListWithOneSmartMeter() throws Exception {
        // Arrange
        SmartMeter sm = new SmartMeter();
        sm.setName("smartMeter");
        sm.setDescription("smartMeter description");
        var ipAddress = UUID.randomUUID().toString();
        sm.setIpAddress(ipAddress);
        sm.setHouseId("Test House");
        sm.setIsPoweredOn(false);
        smartMeterRepository.save(sm);

        // Act
        String responseJson = mockMvc.perform(get("/api/smart-meters")
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        SmartMeterDTO[] response = objectMapper.readValue(responseJson, SmartMeterDTO[].class);

        // Assert
        assertThat(response).hasSize(1);
        assertThat(response[0].getId()).isNotEmpty();
        assertThat(response[0].getName()).isEqualTo("smartMeter");
        assertThat(response[0].getDescription()).isEqualTo("smartMeter description");
        assertThat(response[0].getIpAddress()).isEqualTo(ipAddress);
        assertThat(response[0].getHouseId()).isEqualTo("Test House");
        assertThat(response[0].getIsPoweredOn()).isFalse();
    }
}
