package cz.muni.fi.pa165.epsilon.smartmetermanagement.rest;

import cz.muni.fi.pa165.epsilon.smartmetermanagement.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.facade.SmartMeterFacade;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.util.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@WithMockUser(authorities = {"SCOPE_test_write", "SCOPE_test_read", "SCOPE_test_1", "SCOPE_test_2"})
@AutoConfigureMockMvc
public class SmartMeterRestControllerMvcTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SmartMeterFacade smartMeterFacade;

    @Test
    void registerSmartMeter_meterCreated_returnMeter() throws Exception {
        // Arrange
        when(smartMeterFacade.registerNewSmartMeter(TestDataFactory.smartMeterRequestDTO)).thenReturn(TestDataFactory.smartMeterDTO);

        // Act & assert
        mockMvc.perform(post("/api/smart-meters")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"" + TestDataFactory.smartMeterDTO.getName() + "\"," +
                                "\"description\":\"" + TestDataFactory.smartMeterDTO.getDescription() + "\"," +
                                "\"ipAddress\":\"" + TestDataFactory.smartMeterDTO.getIpAddress() + "\"," +
                                "\"houseId\":\"" + TestDataFactory.smartMeterDTO.getHouseId() + "\"," +
                                "\"isPoweredOn\":\"" + TestDataFactory.smartMeterDTO.getIsPoweredOn() + "\"}"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value(TestDataFactory.smartMeterDTO.getName()))
                .andExpect(jsonPath("$.description").value(TestDataFactory.smartMeterDTO.getDescription()))
                .andExpect(jsonPath("$.ipAddress").value(TestDataFactory.smartMeterDTO.getIpAddress()))
                .andExpect(jsonPath("$.houseId").value(TestDataFactory.smartMeterDTO.getHouseId()))
                .andExpect(jsonPath("$.isPoweredOn").value(TestDataFactory.smartMeterDTO.getIsPoweredOn()));

        verify(smartMeterFacade).registerNewSmartMeter(TestDataFactory.smartMeterRequestDTO);
    }

    @Test
    void registerSmartMeter_invalidInput_returnsBadRequest() throws Exception {
        // Act & assert
        mockMvc.perform(post("/api/smart-meters")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void findById_smartMeterFound_returnsSmartMeter() throws Exception {
        // Arrange
        String id = TestDataFactory.smartMeterDTO.getId();
        when(smartMeterFacade.findById(id)).thenReturn(TestDataFactory.smartMeterDTO);

        // Act & assert
        mockMvc.perform(get("/api/smart-meters/{smartMeterId}", id)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(TestDataFactory.smartMeterDTO.getName()))
                .andExpect(jsonPath("$.description").value(TestDataFactory.smartMeterDTO.getDescription()))
                .andExpect(jsonPath("$.ipAddress").value(TestDataFactory.smartMeterDTO.getIpAddress()))
                .andExpect(jsonPath("$.houseId").value(TestDataFactory.smartMeterDTO.getHouseId()))
                .andExpect(jsonPath("$.isPoweredOn").value(TestDataFactory.smartMeterDTO.getIsPoweredOn()));

        // Assert
        verify(smartMeterFacade).findById(id);
    }

    @Test
    void findById_invalidId_returnsNotFound() throws Exception {
        // Arrange
        String invalidId = "invalidId";
        when(smartMeterFacade.findById(invalidId)).thenThrow(new ResourceNotFoundException("Smart meter not found"));

        // Act & assert
        mockMvc.perform(get("/api/smart-meters/{smartMeterId}", invalidId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.status").value("NOT_FOUND"))
                .andExpect(jsonPath("$.message").value("Smart meter not found"));

        // Assert
        verify(smartMeterFacade).findById(invalidId);
    }

    @Test
    void updateSmartMeter_meterUpdate_returnsMeter() throws Exception {
        var updateSmartMeterRequestDTO = TestDataFactory.updateSmartMeterRequestDTO;
        when(smartMeterFacade.updateSmartMeter(updateSmartMeterRequestDTO)
        ).thenReturn(TestDataFactory.smartMeterDTO);

        String content = "{\"id\":\"" + updateSmartMeterRequestDTO.getId() + "\"," +
                "\"name\":\"" + updateSmartMeterRequestDTO.getName() + "\"," +
                "\"description\":\"" + updateSmartMeterRequestDTO.getDescription() + "\"," +
                "\"ipAddress\":\"" + updateSmartMeterRequestDTO.getIpAddress() + "\"," +
                "\"houseId\":\"" + updateSmartMeterRequestDTO.getHouseId() + "\"," +
                "\"isPoweredOn\": " + updateSmartMeterRequestDTO.getIsPoweredOn().toString() + "}";
        mockMvc.perform(put("/api/smart-meters")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isOk());

        verify(smartMeterFacade).updateSmartMeter(updateSmartMeterRequestDTO);
    }

    @Test
    void updateSmartMeter_invalidInput_returnsBadRequest() throws Exception {
        // Act & assert
        mockMvc.perform(put("/api/smart-meters")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void turnOnSmartMeter_turnOnSmartMeterCalled_returnsMeter() throws Exception {
        var updateSmartMeterRequestDTO = TestDataFactory.updateSmartMeterRequestDTO;
        when(smartMeterFacade.turnOnSmartMeter(updateSmartMeterRequestDTO.getId())
        ).thenReturn(TestDataFactory.smartMeterDTO);

        mockMvc.perform(put("/api/smart-meters/{smartMeterId}/turn-on", updateSmartMeterRequestDTO.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(smartMeterFacade).turnOnSmartMeter(updateSmartMeterRequestDTO.getId());
    }

    @Test
    void turnOffSmartMeter_turnOffSmartMeterCalled_returnsMeter() throws Exception {
        var updateSmartMeterRequestDTO = TestDataFactory.updateSmartMeterRequestDTO;
        when(smartMeterFacade.turnOffSmartMeter(updateSmartMeterRequestDTO.getId())
        ).thenReturn(TestDataFactory.smartMeterDTO);

        mockMvc.perform(put("/api/smart-meters/{smartMeterId}/turn-off", updateSmartMeterRequestDTO.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(smartMeterFacade).turnOffSmartMeter(updateSmartMeterRequestDTO.getId());
    }

    @Test
    void removeSmartMeter_meterRemoved_returnsSmartMeter() throws Exception {
        mockMvc.perform(delete("/api/smart-meters/{smartMeterId}", TestDataFactory.smartMeterEntity.getId())
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        verify(smartMeterFacade).removeSmartMeter(TestDataFactory.smartMeterEntity.getId());
    }

    @Test
    void removeSmartMeter_invalidId_returnsNotFound() throws Exception {
        // Arrange
        String invalidId = "invalidId";
        when(smartMeterFacade.removeSmartMeter(invalidId)).thenThrow(new ResourceNotFoundException("Smart meter not found"));

        // Act & assert
        mockMvc.perform(delete("/api/smart-meters/{smartMeterId}", invalidId)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.status").value("NOT_FOUND"))
                .andExpect(jsonPath("$.message").value("Smart meter not found"));

        verify(smartMeterFacade).removeSmartMeter(invalidId);
    }

    @Test
    void listAll_metersFound_returnListOfMeters() throws Exception {
        // Arrange
        when(smartMeterFacade.listAll()).thenReturn(List.of(TestDataFactory.smartMeterDTO));

        // Act & assert
        mockMvc.perform(get("/api/smart-meters")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(1))
                .andExpect(jsonPath("$[0].id").value("1"));

        verify(smartMeterFacade).listAll();
    }

    @Test
    void listAll_noMetersFound_returnsEmptyList() throws Exception {
        // Arrange
        when(smartMeterFacade.listAll()).thenReturn(List.of());

        // Act & assert
        mockMvc.perform(get("/api/smart-meters")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(0));

        verify(smartMeterFacade).listAll();
    }

    @Test
    void listAllForHouseId_metersFound_returnListOfMeters() throws Exception {
        // Arrange
        when(smartMeterFacade.listAllForHouseId(TestDataFactory.smartMeterDTO.getHouseId())).thenReturn(List.of(TestDataFactory.smartMeterDTO));

        // Act & assert
        mockMvc.perform(get("/api/smart-meters-for-house/{houseId}", TestDataFactory.smartMeterDTO.getHouseId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(1))
                .andExpect(jsonPath("$[0].id").value("1"));

        verify(smartMeterFacade).listAllForHouseId(TestDataFactory.smartMeterDTO.getHouseId());
    }

    @Test
    void listAllForHouseId_noMetersFound_returnsEmptyList() throws Exception {
        // Arrange
        when(smartMeterFacade.listAllForHouseId(TestDataFactory.smartMeterDTO.getHouseId())).thenReturn(List.of());

        // Act & assert
        mockMvc.perform(get("/api/smart-meters-for-house/{houseId}", TestDataFactory.smartMeterDTO.getHouseId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(0));

        verify(smartMeterFacade).listAllForHouseId(TestDataFactory.smartMeterDTO.getHouseId());
    }
}
