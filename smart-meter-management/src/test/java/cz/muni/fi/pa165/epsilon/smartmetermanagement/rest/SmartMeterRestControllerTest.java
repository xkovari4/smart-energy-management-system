package cz.muni.fi.pa165.epsilon.smartmetermanagement.rest;

import cz.muni.fi.pa165.epsilon.smartmetermanagement.api.SmartMeterDTO;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.facade.SmartMeterFacade;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static cz.muni.fi.pa165.epsilon.smartmetermanagement.util.TestDataFactory.smartMeterDTO;
import static cz.muni.fi.pa165.epsilon.smartmetermanagement.util.TestDataFactory.smartMeterEntity;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class SmartMeterRestControllerTest {

    @Mock
    private SmartMeterFacade smartMeterFacade;

    @InjectMocks
    private SmartMeterRestController smartMeterRestController;

    @Test
    void listAll_oneEntity_returnMeasuresList() {
        // Arrange
        when(smartMeterFacade.listAll()).thenReturn(List.of(smartMeterDTO));

        // Act
        ResponseEntity<List<SmartMeterDTO>> response = smartMeterRestController.listAll();

        // Assert
        assertThat(response.hasBody()).isEqualTo(true);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(List.of(smartMeterDTO));
    }

    @Test
    void listAll_noEntities_returnEmptyList() {
        // Arrange
        when(smartMeterFacade.listAll()).thenReturn(List.of());

        // Act
        ResponseEntity<List<SmartMeterDTO>> response = smartMeterRestController.listAll();

        // Assert
        assertThat(response.hasBody()).isEqualTo(true);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(List.of());
    }

    @Test
    void listAllForHouseId_oneEntity_returnMeasuresList() {
        // Arrange
        var houseId = smartMeterEntity.getHouseId();
        when(smartMeterFacade.listAllForHouseId(houseId)).thenReturn(List.of(smartMeterDTO));

        // Act
        ResponseEntity<List<SmartMeterDTO>> response = smartMeterRestController.listAllForHouseId(houseId);

        // Assert
        assertThat(response.hasBody()).isEqualTo(true);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(List.of(smartMeterDTO));
    }

    @Test
    void listAllForHouseId_noEntities_returnEmptyList() {
        // Arrange
        var houseId = smartMeterEntity.getHouseId();
        when(smartMeterFacade.listAllForHouseId(houseId)).thenReturn(List.of());

        // Act
        ResponseEntity<List<SmartMeterDTO>> response = smartMeterRestController.listAllForHouseId(houseId);

        // Assert
        assertThat(response.hasBody()).isEqualTo(true);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(List.of());
    }

    @Test
    void listAllForHouseId_invalidHouseId_returnsNotFound() {
        // Arrange
        var invalidHouseId = "invalidHouseId";
        when(smartMeterFacade.listAllForHouseId(invalidHouseId)).thenThrow(new ResourceNotFoundException("House not found"));

        // Act
        ResourceNotFoundException thrown = assertThrows(ResourceNotFoundException.class, () -> {
            smartMeterRestController.listAllForHouseId(invalidHouseId);
        });

        // Assert
        assertThat(thrown.getMessage()).isEqualTo("House not found");
        verify(smartMeterFacade).listAllForHouseId(invalidHouseId);
    }

    @Test
    void findById_smartMeterFound_returnsSmartMeter() {
        // Arrange
        var id = smartMeterEntity.getId();
        when(smartMeterFacade.findById(id)).thenReturn(smartMeterDTO);

        // Act
        ResponseEntity<SmartMeterDTO> response = smartMeterRestController.findById(id);

        // Assert
        assertThat(response.hasBody()).isTrue();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(smartMeterDTO);
        verify(smartMeterFacade).findById(id);
    }

    @Test
    void findById_invalidId_returnsNotFound() {
        // Arrange
        var invalidId = "invalidId";
        when(smartMeterFacade.findById(invalidId)).thenThrow(new ResourceNotFoundException("Smart meter not found"));

        // Act & Assert
        ResourceNotFoundException thrown = assertThrows(ResourceNotFoundException.class, () -> {
            smartMeterRestController.findById(invalidId);
        });

        assertThat(thrown.getMessage()).isEqualTo("Smart meter not found");
        verify(smartMeterFacade).findById(invalidId);
    }

    @Test
    void removeSmartMeter_meterRemoved_returnSmartMeter() {
        // Arrange
        when(smartMeterFacade.removeSmartMeter(smartMeterEntity.getId())).thenReturn(smartMeterDTO);

        // Act
        ResponseEntity<SmartMeterDTO> response = smartMeterRestController.removeSmartMeter(smartMeterEntity.getId());

        // Assert
        assertThat(response.hasBody()).isEqualTo(true);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
        assertThat(response.getBody()).isEqualTo(smartMeterDTO);
    }

    @Test
    void removeSmartMeter_meterRemoved_returnNoContent() {
        // Arrange
        when(smartMeterFacade.removeSmartMeter(smartMeterEntity.getId())).thenReturn(smartMeterDTO);

        // Act
        ResponseEntity<SmartMeterDTO> response = smartMeterRestController.removeSmartMeter(smartMeterEntity.getId());

        // Assert
        assertThat(response.hasBody()).isTrue();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
        assertThat(response.getBody()).isEqualTo(smartMeterDTO);
        verify(smartMeterFacade).removeSmartMeter(smartMeterEntity.getId());
    }

    @Test
    void removeSmartMeter_invalidId_returnsNotFound() {
        // Arrange
        var invalidId = "invalidId";
        when(smartMeterFacade.removeSmartMeter(invalidId)).thenThrow(new ResourceNotFoundException("Smart meter not found"));

        // Act & Assert
        ResourceNotFoundException thrown = assertThrows(ResourceNotFoundException.class, () -> {
            smartMeterRestController.removeSmartMeter(invalidId);
        });

        assertThat(thrown.getMessage()).isEqualTo("Smart meter not found");
        verify(smartMeterFacade).removeSmartMeter(invalidId);
    }
}