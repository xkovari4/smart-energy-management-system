package cz.muni.fi.pa165.epsilon.smartmetermanagement.service;

import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.model.Measure;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.model.SmartMeter;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.repository.MeasureRepository;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.util.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MeasureServiceImplTest {

    @Mock
    private MeasureRepository measureRepository;

    @Mock
    private SmartMeterService smartMeterService;

    @InjectMocks
    private MeasureServiceImpl measureService;


    @Test
    void collectMeasures_validData_measuresCollected() {
        // Arrange
        var smartMeterOne = new SmartMeter("1", "", "", "", "", true, Collections.emptySet());
        var smartMeterTwo = new SmartMeter("2", "", "", "", "", true, Collections.emptySet());
        List<SmartMeter> smartMeters = List.of(smartMeterOne, smartMeterTwo);
        when(smartMeterService.listAll()).thenReturn(smartMeters);

        // Act
        measureService.collectMeasures();

        // Assert
        verify(measureRepository, times(1)).saveAll(anyList());
    }

    @Test
    void collectMeasures_noSmartMeters_noMeasuresCollected() {
        // Arrange
        when(smartMeterService.listAll()).thenReturn(Collections.emptyList());

        // Act
        measureService.collectMeasures();

        // Assert
        verify(measureRepository, atMostOnce()).saveAll(anyList());
    }

    @Test
    void listAll_measuresFound_returnsListOfMeasures() {
        // Arrange
        List<Measure> measures = List.of(TestDataFactory.measureEntity);
        when(measureRepository.findAll()).thenReturn(measures);

        // Act
        List<Measure> result = measureService.listAll();

        // Assert
        assertThat(result).isEqualTo(measures);
    }

    @Test
    void listAll_measuresNotFound_returnsEmptyList() {
        // Arrange
        List<Measure> measures = List.of(TestDataFactory.measureEntity);
        when(measureRepository.findAll()).thenReturn(Collections.emptyList());

        // Act
        List<Measure> result = measureService.listAll();

        // Assert
        assertThat(result).isEmpty();
    }

    @Test
    void listAllForSmartMeterId_measuresFound_returnsListOfMeasuresForSmartMeterId() {
        // Arrange
        List<Measure> measures = List.of(TestDataFactory.measureEntity);
        when(measureRepository.findAllForSmartMeterId(TestDataFactory.measureEntity.getSmartMeter().getId())).thenReturn(measures);

        // Act
        List<Measure> result = measureService.listAllForSmartMeterId(TestDataFactory.measureEntity.getSmartMeter().getId());

        // Assert
        assertThat(result).isEqualTo(measures);
    }

    @Test
    void listAllForSmartMeterIdForDay_measuresFound_returnsListOfMeasuresForSmartMeterIdForGivenDay() {
        // Arrange
        var date = TestDataFactory.measureEntity.getTimestamp().toLocalDateTime().toLocalDate();
        List<Measure> measures = List.of(TestDataFactory.measureEntity);
        when(measureRepository.findAllForSmartMeterIdForDay(TestDataFactory.measureEntity.getSmartMeter().getId(), date)).thenReturn(measures);

        // Act
        List<Measure> result = measureService.listAllForSmartMeterIdForDay(TestDataFactory.measureEntity.getSmartMeter().getId(), date);

        // Assert
        assertThat(result).isEqualTo(measures);
    }

    @Test
    void listAllForSmartMeterIdForDay_measuresForADayNotFound_returnsEmptyList() {
        // Arrange
        var date = TestDataFactory.measureEntity.getTimestamp().toLocalDateTime().toLocalDate();
        List<Measure> measures = List.of(TestDataFactory.measureEntity);
        when(measureRepository.findAllForSmartMeterIdForDay(
                anyString(),
                any(LocalDate.class)))
                .thenReturn(Collections.emptyList());

        // Act
        List<Measure> result = measureService.listAllForSmartMeterIdForDay(TestDataFactory.measureEntity.getSmartMeter().getId(), date);

        // Assert
        assertThat(result).isEmpty();
    }

    @Test
    void listAllForSmartMeterId_measuresNotFound_returnsEmptyList() {
        // Arrange
        String smartMeterId = "non-existent-id";
        when(measureRepository.findAllForSmartMeterId(smartMeterId)).thenReturn(Collections.emptyList());

        // Act
        List<Measure> result = measureService.listAllForSmartMeterId(smartMeterId);

        // Assert
        assertThat(result).isEmpty();
    }

    @Test
    void listAllForSmartMeterIdForDay_invalidSmartMeterId_returnsEmptyList() {
        // Arrange
        String smartMeterId = "non-existent-id";
        LocalDate date = LocalDate.now();
        when(measureRepository.findAllForSmartMeterIdForDay(smartMeterId, date)).thenReturn(Collections.emptyList());

        // Act
        List<Measure> result = measureService.listAllForSmartMeterIdForDay(smartMeterId, date);

        // Assert
        assertThat(result).isEmpty();
    }


    @Test
    void removeMeasures_validMeasureIds_measuresRemoved() {
        // Mock measure IDs
        List<String> measureIds = List.of("1", "2");

        // Arrange to remove
        List<Measure> measuresToRemove = List.of(new Measure("1", TestDataFactory.measureEntity.getSmartMeter(), Timestamp.valueOf(LocalDateTime.now()), 500.0));
        when(measureRepository.findAllById(measureIds)).thenReturn(measuresToRemove);

        // Act
        List<Measure> result = measureService.removeMeasures(measureIds);

        // Assert
        assertThat(result).isEqualTo(measuresToRemove);
    }

    @Test
    void removeMeasures_invalidMeasureIds_emptyListReturned() {
        // Mock invalid measure IDs
        List<String> measureIds = List.of("1", "2");

        // Mock no measures found
        when(measureRepository.findAllById(measureIds)).thenReturn(Collections.emptyList());

        // Act
        List<Measure> result = measureService.removeMeasures(measureIds);

        // Assert
        assertThat(result).isEmpty();
    }

    @Test
    void removeMeasures_noMeasuresToRemove_emptyListReturned() {
        // Arrange
        List<String> measureIds = List.of("non-existent-id");
        when(measureRepository.findAllById(measureIds)).thenReturn(Collections.emptyList());

        // Act
        List<Measure> result = measureService.removeMeasures(measureIds);

        // Assert
        assertThat(result).isEmpty();
    }
}