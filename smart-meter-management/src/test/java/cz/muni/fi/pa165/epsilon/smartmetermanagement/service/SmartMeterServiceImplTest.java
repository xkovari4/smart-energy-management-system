package cz.muni.fi.pa165.epsilon.smartmetermanagement.service;

import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.model.Measure;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.model.SmartMeter;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.repository.SmartMeterRepository;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.util.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class SmartMeterServiceImplTest {
    @Mock
    private SmartMeterRepository smartMeterRepository;
    @InjectMocks
    private SmartMeterServiceImpl smartMeterService;

    @Test
    void registerNewSmartMeter_meterRegistered_returnNewSmartMeter() {
        // Arrange
        SmartMeter smartMeter = new SmartMeter();
        smartMeter.setName("TV");
        smartMeter.setDescription("The first from the left in the living room");
        smartMeter.setIpAddress("c1ac:5304:9ca0:f8a1:d166:58ff:c86d:fbb6");
        smartMeter.setHouseId("52df1f2d-ba04-4a15-91e7-af06e7bb2509");
        smartMeter.setIsPoweredOn(false);

        when(smartMeterRepository.save(any(SmartMeter.class))).thenReturn(smartMeter);

        // Act
        SmartMeter result = smartMeterService.registerNewSmartMeter(smartMeter);

        // Assert
        assertNotNull(result.getId());
        assertEquals(result.getName(), smartMeter.getName());
        assertEquals(result.getDescription(), smartMeter.getDescription());
        assertEquals(result.getIpAddress(), smartMeter.getIpAddress());
        assertEquals(result.getHouseId(), smartMeter.getHouseId());
        assertEquals(result.getIsPoweredOn(), smartMeter.getIsPoweredOn());
    }

    @Test
    void findById_idFound_returnSmartMeter() {
        // Arrange
        String id = TestDataFactory.smartMeterEntity.getId();
        when(smartMeterRepository.findById(id)).thenReturn(Optional.of(TestDataFactory.smartMeterEntity));

        // Act
        SmartMeter result = smartMeterService.findById(id);

        // Assert
        assertThat(TestDataFactory.smartMeterEntity).isEqualTo(result);
    }

    @Test
    void findById_idNotFound_throwsResourceNotFoundException() {
        // Arrange
        String id = "nonExistingId";
        when(smartMeterRepository.findById(id)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(ResourceNotFoundException.class, () -> Optional
                .ofNullable(smartMeterService.findById(id)).orElseThrow());
    }

    @Test
    void updateSmartMeter_inputIsValid_returnUpdatedSmartMeter() {
        // Arrange
        String id = "existingId";
        String newName = "New Name";
        String newDescription = "New Description";
        String newIpAddress = "192.168.1.1";
        String newHouseId = "New House";
        Boolean newIsPoweredOn = true;
        Set<Measure> newMeasures = Collections.emptySet();

        SmartMeter oldSmartMeter = new SmartMeter(id, "Old Name", "Old Description", "127.0.0.1", "Old House", false, Collections.emptySet());
        SmartMeter updatedSmartMeter = new SmartMeter(id, newName, newDescription, newIpAddress, newHouseId, newIsPoweredOn, newMeasures);

        when(smartMeterRepository.findById(id)).thenReturn(Optional.of(oldSmartMeter));
        when(smartMeterRepository.save(any(SmartMeter.class))).thenReturn(updatedSmartMeter);

        // Act
        SmartMeter result = smartMeterService.updateSmartMeter(updatedSmartMeter);

        // Assert
        assertThat(updatedSmartMeter).isEqualTo(result);
        verify(smartMeterRepository).findById(id);
        verify(smartMeterRepository).save(updatedSmartMeter);
    }

    @Test
    void updateSmartMeter_idIsNull_throwsResourceNotFoundException() {
        // Arrange
        SmartMeter smartMeter = new SmartMeter(null, "New Name", "New Description", "127.0.0.1", "New House", false, Collections.emptySet());

        // Act & Assert
        assertThrows(ResourceNotFoundException.class, () -> smartMeterService.updateSmartMeter(smartMeter));
    }

    @Test
    void updateSmartMeter_idDoesNotExists_throwsResourceNotFoundException() {
        SmartMeter smartMeter = new SmartMeter("nonexistendID", "New Name", "New Description", "127.0.0.1", "New House", false, Collections.emptySet());
        when(smartMeterRepository.findById("nonexistendID")).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(ResourceNotFoundException.class, () -> smartMeterService.updateSmartMeter(smartMeter));
    }

    @Test
    void updateSmartMeter_smartMeterStateChanges_callsTurnOn() {
        // Arrange
        String id = "existingId";
        SmartMeter oldSmartMeter = new SmartMeter(id, "Old Name", "Old Description", "127.0.0.1", "Old House", false, Collections.emptySet());
        SmartMeter newSmartMeter = new SmartMeter(id, "New Name", "New Description", "192.168.1.1", "New House", true, Collections.emptySet());
        when(smartMeterRepository.findById(id)).thenReturn(Optional.of(oldSmartMeter));
        when(smartMeterRepository.save(any(SmartMeter.class))).thenReturn(oldSmartMeter);

        // Act
        smartMeterService.updateSmartMeter(newSmartMeter);

        // Assert
        verify(smartMeterRepository).save(any(SmartMeter.class));
    }

    @Test
    void updateSmartMeter_smartMeterStateChanges_callsTurnOff() {
        // Arrange
        String id = "existingId";
        SmartMeter oldSmartMeter = new SmartMeter(id, "Old Name", "Old Description", "127.0.0.1", "Old House", true, Collections.emptySet());
        SmartMeter newSmartMeter = new SmartMeter(id, "New Name", "New Description", "192.168.1.1", "New House", true, Collections.emptySet());
        when(smartMeterRepository.findById(id)).thenReturn(Optional.of(oldSmartMeter));
        when(smartMeterRepository.save(any(SmartMeter.class))).thenReturn(oldSmartMeter);

        // Act
        smartMeterService.updateSmartMeter(newSmartMeter);

        // Assert
        verify(smartMeterRepository).save(any(SmartMeter.class));
    }

    @Test
    void turnOnSmartMeter_turnOnCalled_returnTurnedOnSmartMeter() {
        // Arrange
        when(smartMeterRepository.save(TestDataFactory.smartMeterEntity))
                .thenReturn(TestDataFactory.smartMeterEntity);
        when(smartMeterRepository.findById(anyString()))
                .thenReturn(Optional.of(TestDataFactory.smartMeterTurnedOffEntity));

        // Act
        SmartMeter result = smartMeterService.turnOnSmartMeter(TestDataFactory.smartMeterTurnedOffEntity.getId());

        // Assert
        assertThat(TestDataFactory.smartMeterEntity).isEqualTo(result);
        verify(smartMeterRepository).save(TestDataFactory.smartMeterEntity);
    }

    @Test
    void turnOffSmartMeter_turnOffCalled_returnTurnedOffSmartMeter() {
        // Arrange
        when(smartMeterRepository.save(TestDataFactory.smartMeterTurnedOffEntity))
                .thenReturn(TestDataFactory.smartMeterTurnedOffEntity);
        when(smartMeterRepository.findById(anyString()))
                .thenReturn(Optional.of(TestDataFactory.smartMeterEntity));

        // Act
        SmartMeter result = smartMeterService.turnOffSmartMeter(TestDataFactory.smartMeterEntity.getId());

        // Assert
        assertThat(TestDataFactory.smartMeterTurnedOffEntity).isEqualTo(result);
        verify(smartMeterRepository).save(TestDataFactory.smartMeterTurnedOffEntity);
    }

    @Test
    void removeSmartMeter_idExists_returnSmartMeter() {
        // Arrange
        String id = "existingId";
        SmartMeter expectedSmartMeter = new SmartMeter(id, "Test Meter", "Test Description", "127.0.0.1", "Test House", false, Collections.emptySet());
        when(smartMeterRepository.findById(id)).thenReturn(Optional.of(expectedSmartMeter));

        // Act
        SmartMeter result = smartMeterService.removeSmartMeter(id);

        // Assert
        assertThat(expectedSmartMeter).isEqualTo(result);
    }

    @Test
    void removeSmartMeter_idDoesNotExist_throwsResourceNotFoundException() {
        // Arrange
        String id = "nonExistingId";
        when(smartMeterRepository.findById(id)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(ResourceNotFoundException.class, () -> smartMeterService.removeSmartMeter(id));
    }

    @Test
    void listAll_smartMetersFound_returnsListOfSmartMeters() {
        // Arrange
        List<SmartMeter> expectedSmartMeters = List.of(
                new SmartMeter("id1", "Meter 1", "Description 1", "127.0.0.1", "House 1", true, Collections.emptySet()),
                new SmartMeter("id2", "Meter 2", "Description 2", "127.0.0.2", "House 2", false, Collections.emptySet())
        );
        when(smartMeterRepository.findAll()).thenReturn(expectedSmartMeters);

        // Act
        List<SmartMeter> result = smartMeterService.listAll();

        // Assert
        assertThat(expectedSmartMeters).isEqualTo(result);
    }

    @Test
    void listAll_smartMetersNotFound_returnsEmptyList() {
        // Arrange
        when(smartMeterRepository.findAll()).thenReturn(Collections.emptyList());

        // Act
        List<SmartMeter> result = smartMeterService.listAll();

        // Assert
        assertThat(result).isEmpty();
    }

    @Test
    void listAllForHouseId_smartMetersFound_returnsListOfSmartMeters() {
        // Arrange
        String houseId = "House1";
        List<SmartMeter> expectedSmartMeters = List.of(
                new SmartMeter("id1", "Meter 1", "Description 1", "127.0.0.1", houseId, true, Collections.emptySet()),
                new SmartMeter("id2", "Meter 2", "Description 2", "127.0.0.2", houseId, false, Collections.emptySet())
        );
        when(smartMeterRepository.findAllForHouseId(houseId)).thenReturn(expectedSmartMeters);

        // Act
        List<SmartMeter> result = smartMeterService.listAllForHouseId(houseId);

        // Assert
        assertThat(expectedSmartMeters).isEqualTo(result);
    }
    @Test
    void listAllForHouseId_smartMetersNotFound_returnsEmptyList() {
        // Arrange
        String houseId = "House1";
        when(smartMeterRepository.findAllForHouseId(houseId)).thenReturn(Collections.emptyList());

        // Act
        List<SmartMeter> result = smartMeterService.listAllForHouseId(houseId);

        // Assert
        assertThat(result).isEmpty();
    }

}
