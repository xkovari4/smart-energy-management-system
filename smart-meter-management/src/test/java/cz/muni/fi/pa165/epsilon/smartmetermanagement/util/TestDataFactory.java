package cz.muni.fi.pa165.epsilon.smartmetermanagement.util;

import cz.muni.fi.pa165.epsilon.smartmetermanagement.api.MeasureDTO;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.api.SmartMeterDTO;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.model.Measure;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.data.model.SmartMeter;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.rest.dto.RegisterSmartMeterRequestDTO;
import cz.muni.fi.pa165.epsilon.smartmetermanagement.rest.dto.UpdateSmartMeterRequestDTO;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Collections;


@Component
public class TestDataFactory {

    public static Measure measureEntity = getMeasureEntity();
    public static SmartMeter smartMeterEntity = getSmartMeterEntity();
    public static SmartMeter smartMeterTurnedOffEntity = getSmartMeterTurnedOffEntity();
    public static MeasureDTO measureDTO = getMeasureDTO();
    public static SmartMeterDTO smartMeterDTO = getSmartMeterDTO();
    public static RegisterSmartMeterRequestDTO smartMeterRequestDTO = getSmartMeterRequestDTO();
   public static UpdateSmartMeterRequestDTO updateSmartMeterRequestDTO = getUpdateSmartMeterRequestDTO();

    private static Measure getMeasureEntity() {
        return new Measure("1", getSmartMeterEntity(), Timestamp.valueOf(LocalDateTime.now()), 500.0);
    }

    private static SmartMeter getSmartMeterEntity() {
        String id = "1";
        String name = "Test Meter";
        String description = "Test Description";
        String ipAddress = "127.0.0.1";
        String houseId = "Test House";
        Boolean isPoweredOn = true;
        return new SmartMeter(id, name, description, ipAddress, houseId, isPoweredOn, Collections.emptySet());
    }

    private static SmartMeter getSmartMeterTurnedOffEntity() {
        String id = "1";
        String name = "Test Meter";
        String description = "Test Description";
        String ipAddress = "127.0.0.1";
        String houseId = "Test House";
        Boolean isPoweredOn = false;
        return new SmartMeter(id, name, description, ipAddress, houseId, isPoweredOn, Collections.emptySet());
    }

    private static MeasureDTO getMeasureDTO() {
        return new MeasureDTO("1", "1", Timestamp.valueOf(LocalDateTime.now()), 500.0);
    }

    private static SmartMeterDTO getSmartMeterDTO() {
        String id = "1";
        String name = "Test Meter";
        String description = "Test Description";
        String ipAddress = "127.0.0.1";
        String houseId = "Test House";
        Boolean isPoweredOn = true;
        return new SmartMeterDTO(id, name, description, ipAddress, houseId, isPoweredOn);
    }

    private static RegisterSmartMeterRequestDTO getSmartMeterRequestDTO() {
        String name = "Test Meter";
        String description = "Test Description";
        String ipAddress = "127.0.0.1";
        String houseId = "Test House";
        Boolean isPoweredOn = true;
        return new RegisterSmartMeterRequestDTO(name, description, ipAddress, houseId, isPoweredOn);
    }

    private static UpdateSmartMeterRequestDTO getUpdateSmartMeterRequestDTO() {
        return new UpdateSmartMeterRequestDTO("1", "Test Meter", "Test Description", "127.0.0.1","Test House", true);
    }
}
