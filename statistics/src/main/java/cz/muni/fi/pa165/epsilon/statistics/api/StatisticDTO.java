package cz.muni.fi.pa165.epsilon.statistics.api;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@Schema(
        title = "Statistic DTO",
        description = "DTO representation of Statistic"
)
@Setter
@Getter
@ToString
@EqualsAndHashCode
public class StatisticDTO {
    @NotBlank
    @Schema(description = "statistic record ID", example = "123e4567-e89b-12d3-a456-426614174000")
    private String id;

    @NotBlank
    @Schema(description = "ID of the particular smartMeter", example = "123e4567-e89b-12d3-a456-426614174000")
    private String smartMeterId;

    @NotBlank
    @Schema(description = "time of the statistic creation", example = "2024-04-05T10:15:00")
    private LocalDateTime timestamp;

    @NotBlank
    @Schema(description = "number of total energy consumption, in W", example = "123.456")
    private Double totalConsumption;

    @NotBlank
    @Schema(description = "number of average energy consumption, in W", example = "123.456")
    private Double averageConsumption;

    @NotBlank
    @Schema(description = "Indicates if the statistics are aggregated", defaultValue = "false")
    private boolean isAggregated;
}
