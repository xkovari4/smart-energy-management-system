package cz.muni.fi.pa165.epsilon.statistics.config;

import cz.muni.fi.pa165.epsilon.statistics.StatisticsApplication;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springdoc.core.customizers.OpenApiCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class StatisticsSecurityConfig {

    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests(x -> x
                        .requestMatchers(HttpMethod.GET, "/statistics/{meterId}").hasAuthority("SCOPE_test_read")
                        .requestMatchers(HttpMethod.POST, "/statistics/{meterId}").hasAuthority("SCOPE_test_write")
                        .requestMatchers(HttpMethod.DELETE, "/statistics/{meterId}").hasAuthority("SCOPE_test_1")
                        .requestMatchers(HttpMethod.POST, "/statistics/aggregate").hasAuthority("SCOPE_test_write")
                        .requestMatchers(HttpMethod.GET, "/statistics/{meterId}/{startDate}/{endDate}").hasAuthority("SCOPE_test_read")
                        .requestMatchers(HttpMethod.DELETE, "/statistics/{meterId}/{startDate}/{endDate}").hasAuthority("SCOPE_test_1")
                        .requestMatchers(HttpMethod.GET, "/statistics/statistics").hasAuthority("SCOPE_test_read")
                        .anyRequest().permitAll()
                )
                .oauth2ResourceServer(oauth2 -> oauth2.opaqueToken(Customizer.withDefaults()))
                .csrf().disable();
        return http.build();
    }

    @Bean
    public OpenApiCustomizer openAPICustomizer() {
        return openApi ->
                openApi.getComponents()
                        .addSecuritySchemes(StatisticsApplication.SECURITY_SCHEME_BEARER,
                                new SecurityScheme()
                                        .type(SecurityScheme.Type.HTTP)
                                        .scheme("bearer")
                                        .description("Provide a valid access token")
                        );
    }
}