package cz.muni.fi.pa165.epsilon.statistics.data.model;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;

@Entity
@Table(name = "statistic")
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Statistic {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "statisticId")
    private String id;

    @Column(name = "smartMeterId", nullable = false)
    private String smartMeterId;

    @Column(name = "timestamp", nullable = false)
    private LocalDateTime timestamp;

    @Column(name = "totalConsumption", nullable = false)
    private Double totalConsumption;

    @Column(name = "averageConsumption", nullable = false)
    private Double averageConsumption;

    @Column(name = "isAggregated", nullable = false)
    private boolean isAggregated;

    public Statistic(String id, String smartMeterId, LocalDateTime timestamp, Double totalConsumption, Double averageConsumption, boolean isAggregated) {
        this.id = id;
        this.smartMeterId = smartMeterId;
        this.timestamp = timestamp;
        this.totalConsumption = totalConsumption;
        this.averageConsumption = averageConsumption;
        this.isAggregated = isAggregated;
    }
}
