package cz.muni.fi.pa165.epsilon.statistics.data.repository;

import cz.muni.fi.pa165.epsilon.statistics.data.model.Statistic;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface StatisticRepository extends JpaRepository<Statistic, String> {

    @Query("SELECT s FROM Statistic s WHERE s.smartMeterId = :meterId AND s.timestamp >= :startTime AND s.timestamp <= :endTime")
    List<Statistic> findByMeterIdAndTimestampBetween(String meterId, LocalDateTime startTime, LocalDateTime endTime);

    @Modifying
    @Transactional
    void deleteAllBySmartMeterIdAndTimestampBetween(String meterId, LocalDateTime startDate, LocalDateTime endDate);

    List<Statistic> findBySmartMeterId(String meterId);

    @Modifying
    @Transactional
    void deleteBySmartMeterId(String meterId);
}

