package cz.muni.fi.pa165.epsilon.statistics.database;

import cz.muni.fi.pa165.epsilon.statistics.data.repository.StatisticRepository;
import jakarta.annotation.PostConstruct;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Transactional
@AllArgsConstructor
@Profile("database-clear")
public class StatisticClearDatabase {

    private final StatisticRepository statisticRepository;

    @PostConstruct
    public void init() {
        System.out.println("Initializing Database Clearer...");
        clearDatabase();
    }

    private void clearDatabase() {
        statisticRepository.deleteAll();
        System.out.println("Database cleared");
    }
}
