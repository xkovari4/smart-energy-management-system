package cz.muni.fi.pa165.epsilon.statistics.database;

import com.github.javafaker.Faker;
import cz.muni.fi.pa165.epsilon.statistics.data.model.Statistic;
import cz.muni.fi.pa165.epsilon.statistics.data.repository.StatisticRepository;
import cz.muni.fi.pa165.epsilon.statistics.service.smartMeterApi.SmartMeterApiServiceImpl;
import jakarta.annotation.PostConstruct;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Transactional
@AllArgsConstructor
@Profile("database-start")
public class StatisticSeedDatabase {
    private static final int MAX_STATISTICS = 3;
    private final StatisticRepository statisticRepository;
    private final SmartMeterApiServiceImpl smartMeterApiService;
    private final Faker faker = new Faker();

    @PostConstruct
    public void init() {
        System.out.println("Initializing Database Seeder...");
        if (statisticRepository.count() == 0) {
            seedStatistics();
        } else {
            System.out.println("Statistics database already seeded");
        }
    }

    private void seedStatistics() {
        for (int j = 0; j < 100; j++) {
            for (int i = 0; i < faker.number().numberBetween(1, MAX_STATISTICS); i++) {
                Statistic statistic = new Statistic();
                statistic.setSmartMeterId(String.valueOf(i));
                statistic.setTimestamp(LocalDateTime.now().minusDays(faker.number().numberBetween(1, 365)));
                statistic.setTotalConsumption(faker.number().randomDouble(2, 100, 1000));
                statistic.setAverageConsumption(faker.number().randomDouble(2, 10, 100));
                statistic.setAggregated(faker.bool().bool());
                statisticRepository.save(statistic);
            }
        }
        System.out.println("Statistics database seeded");
    }
}
