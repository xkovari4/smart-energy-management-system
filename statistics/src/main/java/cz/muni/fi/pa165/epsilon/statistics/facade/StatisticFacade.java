package cz.muni.fi.pa165.epsilon.statistics.facade;

import cz.muni.fi.pa165.epsilon.statistics.api.StatisticDTO;
import java.time.LocalDateTime;
import java.util.List;

public interface StatisticFacade {

    StatisticDTO generatePeriodicMeterConsumptionStatistics(String meterId, LocalDateTime startTime, LocalDateTime endTime);

    StatisticDTO aggregateMeterConsumptionStatistics(String meterId);

    StatisticDTO takeStatisticsSnapshot(String meterId);

    List<StatisticDTO> listAll();

    List<StatisticDTO> removeStatisticsByMeterId(String meterId);

    List<StatisticDTO> findStatisticsByMeterId(String meterId);

    List<StatisticDTO> findStatisticsByIdAndTimestamp(String meterId, LocalDateTime startDate, LocalDateTime endDate);

    List<StatisticDTO> removeStatisticsByMeterIdAndTimestamp(String meterId, LocalDateTime startDate, LocalDateTime endDate);
}
