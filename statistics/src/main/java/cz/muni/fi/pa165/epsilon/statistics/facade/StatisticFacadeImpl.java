package cz.muni.fi.pa165.epsilon.statistics.facade;

import cz.muni.fi.pa165.epsilon.statistics.api.StatisticDTO;
import cz.muni.fi.pa165.epsilon.statistics.data.model.Statistic;
import cz.muni.fi.pa165.epsilon.statistics.mappers.StatisticMapper;
import cz.muni.fi.pa165.epsilon.statistics.service.StatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class StatisticFacadeImpl implements StatisticFacade {
    private final StatisticService statisticService;
    private final StatisticMapper statisticMapper;

    @Autowired
    public StatisticFacadeImpl(StatisticService statisticService, StatisticMapper statisticMapper) {
        this.statisticService = statisticService;
        this.statisticMapper = statisticMapper;
    }

    @Override
    @Transactional(readOnly = true)
    public StatisticDTO generatePeriodicMeterConsumptionStatistics(String meterId, LocalDateTime startTime, LocalDateTime endTime) {
        if (meterId == null || startTime == null || endTime == null || startTime.isAfter(endTime)) {
            throw new IllegalArgumentException("Invalid input parameters.");
        }
        Statistic result = statisticService.generatePeriodicMeterConsumptionStatistics(meterId, startTime, endTime);
        return statisticMapper.mapToDto(result);
    }

    @Override
    @Transactional(readOnly = true)
    public StatisticDTO aggregateMeterConsumptionStatistics(String meterId) {
        if (meterId == null) {
            throw new IllegalArgumentException("Meter ID cannot be null.");
        }
        Statistic result = statisticService.aggregateMeterConsumptionStatistics(meterId);
        return statisticMapper.mapToDto(result);
    }

    @Override
    @Transactional(readOnly = true)
    public StatisticDTO takeStatisticsSnapshot(String meterId) {
        if (meterId == null) {
            throw new IllegalArgumentException("Meter ID cannot be null.");
        }
        return statisticMapper.mapToDto(statisticService.takeStatisticsSnapshot(meterId));
    }

    @Override
    @Transactional(readOnly = true)
    public List<StatisticDTO> listAll() {
        List<Statistic> statistics = statisticService.listAll();
        return statistics.stream().map(statisticMapper::mapToDto).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public List<StatisticDTO> removeStatisticsByMeterId(String meterId) {
        if (meterId == null) {
            throw new IllegalArgumentException("Meter ID cannot be null.");
        }
        List<Statistic> statistics = statisticService.removeStatisticsByMeterId(meterId);
        return statistics.stream().map(statisticMapper::mapToDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<StatisticDTO> findStatisticsByMeterId(String meterId) {
        if (meterId == null) {
            throw new IllegalArgumentException("Meter ID cannot be null.");
        }
        List<Statistic> statistics = statisticService.findStatisticsByMeterId(meterId);
        return statistics.stream().map(statisticMapper::mapToDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<StatisticDTO> findStatisticsByIdAndTimestamp(String meterId, LocalDateTime startDate, LocalDateTime endDate) {
        if (meterId == null || startDate == null || endDate == null || startDate.isAfter(endDate)) {
            throw new IllegalArgumentException("Invalid input parameters.");
        }
        List<Statistic> statistic = statisticService.findByMeterIdAndTimestamp(meterId, startDate, endDate);
        return statistic.stream().map(statisticMapper::mapToDto).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public List<StatisticDTO> removeStatisticsByMeterIdAndTimestamp(String meterId, LocalDateTime startDate, LocalDateTime endDate) {
        if (meterId == null || startDate == null || endDate == null || startDate.isAfter(endDate)) {
            throw new IllegalArgumentException("Invalid input parameters.");
        }
        List<Statistic> statistic = statisticService.removeStatisticsByMeterIdAndTimestamp(meterId, startDate, endDate);
        return statistic.stream().map(statisticMapper::mapToDto).collect(Collectors.toList());
    }
}
