package cz.muni.fi.pa165.epsilon.statistics.mappers;

import cz.muni.fi.pa165.epsilon.statistics.api.StatisticDTO;
import cz.muni.fi.pa165.epsilon.statistics.data.model.Statistic;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface StatisticMapper {
    Statistic mapToStatistic(StatisticDTO statisticDTO);

    StatisticDTO mapToDto(Statistic user);

    List<StatisticDTO> mapToList(List<Statistic> users);

}
