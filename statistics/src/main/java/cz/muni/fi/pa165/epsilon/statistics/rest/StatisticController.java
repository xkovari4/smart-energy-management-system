package cz.muni.fi.pa165.epsilon.statistics.rest;

import cz.muni.fi.pa165.epsilon.statistics.api.StatisticDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDateTime;
import java.util.List;

public interface StatisticController {

    ResponseEntity<List<StatisticDTO>> listAll();

    ResponseEntity<StatisticDTO> aggregateStatistics(
            @RequestParam String meterId,
            @RequestParam("start") LocalDateTime start,
            @RequestParam("end") LocalDateTime end);

    ResponseEntity<List<StatisticDTO>> findStatisticsByMeterId(@PathVariable String meterId);

    ResponseEntity<List<StatisticDTO>> removeStatisticsByMeterId(@PathVariable String meterId);

    ResponseEntity<List<StatisticDTO>> findStatisticsByIdAndTimestamp(@PathVariable String meterId, @PathVariable LocalDateTime startDate, @PathVariable LocalDateTime endDate);

    ResponseEntity<List<StatisticDTO>> removeStatisticsByMeterIdAndTimestamp(@PathVariable String meterId, @PathVariable LocalDateTime startDate, @PathVariable LocalDateTime endDate);

    ResponseEntity<StatisticDTO> createStatistic(@PathVariable String meterId);
}
