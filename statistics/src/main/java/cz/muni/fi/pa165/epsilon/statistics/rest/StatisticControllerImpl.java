package cz.muni.fi.pa165.epsilon.statistics.rest;

import cz.muni.fi.pa165.epsilon.statistics.StatisticsApplication;
import cz.muni.fi.pa165.epsilon.statistics.api.StatisticDTO;
import cz.muni.fi.pa165.epsilon.statistics.facade.StatisticFacade;
import cz.muni.fi.pa165.epsilon.statistics.rest.exceptionhandling.ApiError;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@OpenAPIDefinition(
        info = @Info(
                title = "Smart Energy Management OpenAPI Service",
                description = "Statistics Service",
                version = "1.0.0"
        ),
        servers = {@Server(url = "http://statistics:8080", description = "Docker network server"),
                @Server(url = "http://localhost:8083", description = "Localhost network server")}
)
@Tag(name = "StatisticService", description = "API service for managing energy statistics")
@RequestMapping(path = "/statistics", produces = MediaType.APPLICATION_JSON_VALUE)
public class StatisticControllerImpl implements StatisticController {

    private final StatisticFacade statisticFacade;

    @Autowired
    public StatisticControllerImpl(StatisticFacade statisticFacade) {
        this.statisticFacade = statisticFacade;
    }

    @Override
    @Operation(summary = "List all statistics", description = "Returns a list of all statistics",
            security = {@SecurityRequirement(name = StatisticsApplication.SECURITY_SCHEME_OAUTH2, scopes = {"test_read"}),
                    @SecurityRequirement(name = StatisticsApplication.SECURITY_SCHEME_BEARER, scopes = {"test_read"})})
    @GetMapping("/statistics")
    @CrossOrigin(origins = "*")
    @Transactional(readOnly = true)
    public ResponseEntity<List<StatisticDTO>> listAll() {
        List<StatisticDTO> statistics = statisticFacade.listAll();
        return ResponseEntity.ok(statistics);
    }

    @Override
    @Operation(summary = "Aggregate statistics", description = "Aggregates statistics for a given meter within a time range",
            security = {@SecurityRequirement(name = StatisticsApplication.SECURITY_SCHEME_OAUTH2, scopes = {"test_write"}),
                    @SecurityRequirement(name = StatisticsApplication.SECURITY_SCHEME_BEARER, scopes = {"test_write"})},
            responses = {
                    @ApiResponse(responseCode = "201", description = "Statistics aggregated",
                            content = @Content(schema = @Schema(implementation = StatisticDTO.class))),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_write", content = @Content()),
                    @ApiResponse(responseCode = "404", description = "Statistics not found",
                            content = @Content(schema = @Schema(implementation = ApiError.class)))
            })
    @PostMapping("/aggregate")
    @CrossOrigin(origins = "*")
    @Transactional
    public ResponseEntity<StatisticDTO> aggregateStatistics(
            @RequestParam String meterId,
            @RequestParam("start") LocalDateTime start,
            @RequestParam("end") LocalDateTime end) {
        StatisticDTO aggregatedStatistics = statisticFacade.generatePeriodicMeterConsumptionStatistics(meterId, start, end);
        return ResponseEntity.status(HttpStatus.CREATED).body(aggregatedStatistics);
    }

    @Override
    @Operation(summary = "Find statistics by MeterId", description = "Finds statistics by their MeterId",
            security = {@SecurityRequirement(name = StatisticsApplication.SECURITY_SCHEME_OAUTH2, scopes = {"test_read"}),
                    @SecurityRequirement(name = StatisticsApplication.SECURITY_SCHEME_BEARER, scopes = {"test_read"})},
            responses = {
                    @ApiResponse(responseCode = "200", description = "Statistics found",
                            content = @Content(schema = @Schema(implementation = StatisticDTO.class))),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_read", content = @Content()),
                    @ApiResponse(responseCode = "404", description = "Statistics not found",
                            content = @Content(schema = @Schema(implementation = ApiError.class)))
            })
    @GetMapping("/{meterId}")
    @CrossOrigin(origins = "*")
    @Transactional(readOnly = true)
    public ResponseEntity<List<StatisticDTO>> findStatisticsByMeterId(@PathVariable String meterId) {
        List<StatisticDTO> statistics = statisticFacade.findStatisticsByMeterId(meterId);
        return ResponseEntity.ok(statistics);
    }

    @Override
    @Operation(summary = "Remove statistics by MeterId", description = "Removes statistics by their MeterId",
            security = {@SecurityRequirement(name = StatisticsApplication.SECURITY_SCHEME_OAUTH2, scopes = {"test_1"}),
                    @SecurityRequirement(name = StatisticsApplication.SECURITY_SCHEME_BEARER, scopes = {"test_1"})},
            responses = {
                    @ApiResponse(responseCode = "200", description = "Statistics removed",
                            content = @Content(schema = @Schema(implementation = StatisticDTO.class))),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_1", content = @Content()),
                    @ApiResponse(responseCode = "404", description = "Statistics not found",
                            content = @Content(schema = @Schema(implementation = ApiError.class)))
            })
    @DeleteMapping("/{meterId}")
    @CrossOrigin(origins = "*")
    @Transactional
    public ResponseEntity<List<StatisticDTO>> removeStatisticsByMeterId(@PathVariable String meterId) {
        List<StatisticDTO> removedStatistics = statisticFacade.removeStatisticsByMeterId(meterId);
        return ResponseEntity.ok(removedStatistics);
    }

    @Override
    @Operation(summary = "Find statistics by MeterId, start and end timestamp", description = "Find statistics by its MeterId, start and end timestamp",
            security = {@SecurityRequirement(name = StatisticsApplication.SECURITY_SCHEME_OAUTH2, scopes = {"test_read"}),
                    @SecurityRequirement(name = StatisticsApplication.SECURITY_SCHEME_BEARER, scopes = {"test_read"})},
            responses = {
                    @ApiResponse(responseCode = "200", description = "Statistics found",
                            content = @Content(schema = @Schema(implementation = StatisticDTO.class))),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_read", content = @Content()),
                    @ApiResponse(responseCode = "404", description = "Statistics not found",
                            content = @Content(schema = @Schema(implementation = ApiError.class)))
            })
    @GetMapping("/{meterId}/{startDate}/{endDate}")
    @CrossOrigin(origins = "*")
    @Transactional(readOnly = true)
    public ResponseEntity<List<StatisticDTO>> findStatisticsByIdAndTimestamp(@PathVariable String meterId, @PathVariable LocalDateTime startDate, @PathVariable LocalDateTime endDate) {
        List<StatisticDTO> statistics = statisticFacade.findStatisticsByIdAndTimestamp(meterId, startDate, endDate);
        return ResponseEntity.ok(statistics);
    }

    @Override
    @Operation(summary = "Remove statistics by MeterId, start and end timestamp", description = "Removes statistics by its MeterId, start and end timestamp",
            security = {@SecurityRequirement(name = StatisticsApplication.SECURITY_SCHEME_OAUTH2, scopes = {"test_1"}),
                    @SecurityRequirement(name = StatisticsApplication.SECURITY_SCHEME_BEARER, scopes = {"test_1"})},
            responses = {
                    @ApiResponse(responseCode = "200", description = "Statistics removed",
                            content = @Content(schema = @Schema(implementation = StatisticDTO.class))),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_1", content = @Content()),
                    @ApiResponse(responseCode = "404", description = "Statistics not found",
                            content = @Content(schema = @Schema(implementation = ApiError.class)))
            })
    @DeleteMapping("/{meterId}/{startDate}/{endDate}")
    @CrossOrigin(origins = "*")
    @Transactional
    public ResponseEntity<List<StatisticDTO>> removeStatisticsByMeterIdAndTimestamp(@PathVariable String meterId, @PathVariable LocalDateTime startDate, @PathVariable LocalDateTime endDate) {
        List<StatisticDTO> statistics = statisticFacade.removeStatisticsByMeterIdAndTimestamp(meterId, startDate, endDate);
        return ResponseEntity.ok(statistics);
    }

    @Override
    @Operation(summary = "Create statistic for MeterId", description = "Create statistic for MeterId",
            security = {@SecurityRequirement(name = StatisticsApplication.SECURITY_SCHEME_OAUTH2, scopes = {"test_write"}),
                    @SecurityRequirement(name = StatisticsApplication.SECURITY_SCHEME_BEARER, scopes = {"test_write"})},
            responses = {
                    @ApiResponse(responseCode = "201", description = "Statistics created",
                            content = @Content(schema = @Schema(implementation = StatisticDTO.class))),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_write", content = @Content()),
                    @ApiResponse(responseCode = "404", description = "MeterId not found",
                            content = @Content(schema = @Schema(implementation = ApiError.class)))
            })
    @PostMapping("/{meterId}")
    @CrossOrigin(origins = "*")
    @Transactional
    public ResponseEntity<StatisticDTO> createStatistic(@PathVariable String meterId) {
        StatisticDTO statistic = statisticFacade.takeStatisticsSnapshot(meterId);
        return ResponseEntity.status(HttpStatus.CREATED).body(statistic);
    }
}
