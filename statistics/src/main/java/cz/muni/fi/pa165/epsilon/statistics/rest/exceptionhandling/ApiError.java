package cz.muni.fi.pa165.epsilon.statistics.rest.exceptionhandling;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

import java.time.Clock;
import java.time.LocalDateTime;

@Setter
@Getter
@ToString
@EqualsAndHashCode
public class ApiError {
    private LocalDateTime timestamp;
    private HttpStatus status;
    private String message;
    private String path;

    public ApiError(HttpStatus status, String message, String path) {
        this.timestamp = LocalDateTime.now(Clock.systemUTC());
        this.status = status;
        this.message = message;
        this.path = path;
    }

    public ApiError(LocalDateTime timestamp, HttpStatus status, String message, String path) {
        this.timestamp = timestamp;
        this.status = status;
        this.message = message;
        this.path = path;
    }
}
