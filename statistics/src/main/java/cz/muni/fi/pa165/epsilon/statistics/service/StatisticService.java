package cz.muni.fi.pa165.epsilon.statistics.service;

import cz.muni.fi.pa165.epsilon.statistics.data.model.Statistic;
import cz.muni.fi.pa165.epsilon.statistics.exceptions.ResourceNotFoundException;

import java.time.LocalDateTime;
import java.util.List;

public interface StatisticService {

    /**
     * Generates statistics for a specified meter over a given period.
     * @param meterId the ID of the meter
     * @param startTime start time of the period
     * @param endTime end time of the period
     * @return aggregated statistics for the period
     * @throws ResourceNotFoundException if no statistics are found
     */
    Statistic generatePeriodicMeterConsumptionStatistics(String meterId, LocalDateTime startTime, LocalDateTime endTime);

    /**
     * Aggregates all available statistics for a specific meter.
     * @param meterId the ID of the meter
     * @return aggregated statistics
     * @throws ResourceNotFoundException if no statistics are found
     */
    Statistic aggregateMeterConsumptionStatistics(String meterId);

    /**
     * Takes a snapshot of the current statistics for a meter and saves it. The statistic has totalConsumption and
     * computes the averageConsumption.
     * @param meterId the ID of the meter
     * @return the new statistic snapshot
     * @throws ResourceNotFoundException if no measures are found
     */
    Statistic takeStatisticsSnapshot(String meterId);

    /**
     * Periodically takes snapshots of statistics for all meters.
     */
    void takePeriodicSnapshots();

    /**
     * Retrieves all statistics records.
     * @return list of all statistics
     */
    List<Statistic> listAll();

    /**
     * Removes all statistics associated with a specific meter ID.
     * @param meterId the ID of the meter
     * @return the statistics that were removed
     * @throws ResourceNotFoundException if no statistics are found
     */
    List<Statistic> removeStatisticsByMeterId(String meterId);

    /**
     * Finds statistics for a specific meter.
     * @param meterId the ID of the meter
     * @return list of statistics within the time range
     * @throws ResourceNotFoundException if no statistics are found
     */
    List<Statistic> findStatisticsByMeterId(String meterId);

    /**
     * Finds statistics for a specific meter within a specified time range.
     * @param id the ID of the meter
     * @param startDate start date of the time range
     * @param endDate end date of the time range
     * @return list of statistics within the time range
     * @throws ResourceNotFoundException if no statistics are found
     */
    List<Statistic> findByMeterIdAndTimestamp(String id, LocalDateTime startDate, LocalDateTime endDate);

    /**
     * Removes all statistics associated with a specific meter ID within a specified time range.
     * @param id the ID of the meter
     * @param startDate start date of the time range
     * @param endDate end date of the time range
     * @return the statistics that were removed
     * @throws ResourceNotFoundException if no statistics are found
     */
    List<Statistic> removeStatisticsByMeterIdAndTimestamp(String id, LocalDateTime startDate, LocalDateTime endDate);
}


