package cz.muni.fi.pa165.epsilon.statistics.service;

import cz.muni.fi.pa165.epsilon.smart_meter_management.client.model.MeasureDTO;
import cz.muni.fi.pa165.epsilon.statistics.data.model.Statistic;
import cz.muni.fi.pa165.epsilon.statistics.data.repository.StatisticRepository;
import cz.muni.fi.pa165.epsilon.statistics.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.epsilon.statistics.exceptions.ValidationException;
import cz.muni.fi.pa165.epsilon.statistics.service.measureApi.MeasureApiService;
import cz.muni.fi.pa165.epsilon.statistics.service.smartMeterApi.SmartMeterApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.OptionalDouble;
import java.util.UUID;

@Service
public class StatisticServiceImpl implements StatisticService {
    private final StatisticRepository statisticRepository;
    private final SmartMeterApiService smartMeterApiServiceImpl;
    private final MeasureApiService measureApiServiceImpl;

    @Autowired
    public StatisticServiceImpl(StatisticRepository statisticRepository, SmartMeterApiService smartMeterApiServiceImpl, MeasureApiService measureApiServiceImpl) {
        this.statisticRepository = statisticRepository;
        this.smartMeterApiServiceImpl = smartMeterApiServiceImpl;
        this.measureApiServiceImpl = measureApiServiceImpl;
    }

    @Override
    @Transactional
    public Statistic generatePeriodicMeterConsumptionStatistics(String meterId, LocalDateTime startTime, LocalDateTime endTime) {
        validateDateRange(startTime, endTime);
        List<Statistic> stats = statisticRepository.findByMeterIdAndTimestampBetween(meterId, startTime, endTime);
        if (stats.isEmpty()) {
            throw new ResourceNotFoundException("No statistics found for meter ID between the specified dates: " + meterId);
        }
        return aggregateStatistics(stats, meterId);
    }

    @Override
    @Transactional
    public Statistic aggregateMeterConsumptionStatistics(String meterId) {
        List<Statistic> stats = statisticRepository.findBySmartMeterId(meterId);
        if (stats.isEmpty()) {
            throw new ResourceNotFoundException("No statistics found for meter ID: " + meterId);
        }
        return aggregateStatistics(stats, meterId);
    }

    @Override
    @Transactional
    public Statistic takeStatisticsSnapshot(String meterId) {
        List<MeasureDTO> measures = measureApiServiceImpl.listAllForSmartMeterId(meterId);
        if (measures.isEmpty()) {
            throw new ResourceNotFoundException("No measures found for meter ID: " + meterId);
        }

        LocalDateTime latestTimestamp = LocalDateTime.now();
        double totalConsumption = measures.stream().mapToDouble(MeasureDTO::getMeasuredValue).sum();
        double averageConsumption = measures.stream().mapToDouble(MeasureDTO::getMeasuredValue).average().orElse(0.0);

        Statistic statistic = new Statistic(UUID.randomUUID().toString(), meterId, latestTimestamp, totalConsumption, averageConsumption, false);
        statisticRepository.saveAndFlush(statistic);
        return statistic;
    }

    @Scheduled(fixedRate = 43200000) // 12 hours in milliseconds
    @Override
    @Transactional
    public void takePeriodicSnapshots() {
        List<String> meterIds = smartMeterApiServiceImpl.findAllDistinctMeterIds();
        for (String meterId : meterIds) {
            takeStatisticsSnapshot(meterId);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<Statistic> listAll() {
        return statisticRepository.findAll();
    }

    @Override
    @Transactional
    public List<Statistic> removeStatisticsByMeterId(String meterId) {
        List<Statistic> statistics = statisticRepository.findBySmartMeterId(meterId);
        if (statistics.isEmpty()) {
            throw new ResourceNotFoundException("No statistics found for meter ID: " + meterId);
        }
        statisticRepository.deleteBySmartMeterId(meterId);
        return statistics;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Statistic> findStatisticsByMeterId(String meterId) {
        List<Statistic> statistics = statisticRepository.findBySmartMeterId(meterId);
        if (statistics.isEmpty()) {
            throw new ResourceNotFoundException("No statistics found for meter ID: " + meterId);
        }
        return statistics;
    }

    private Statistic aggregateStatistics(List<Statistic> statistics, String meterId) {
        double totalConsumption = statistics.stream().mapToDouble(Statistic::getTotalConsumption).sum();
        OptionalDouble averageConsumptionOpt = statistics.stream().mapToDouble(Statistic::getTotalConsumption).average();
        double averageConsumption = averageConsumptionOpt.isPresent() ? averageConsumptionOpt.getAsDouble() : 0.0;

        LocalDateTime aggregationTimestamp = LocalDateTime.now();
        String id = UUID.randomUUID().toString();

        return new Statistic(id, meterId, aggregationTimestamp, totalConsumption, averageConsumption, true);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Statistic> findByMeterIdAndTimestamp(String id, LocalDateTime startDate, LocalDateTime endDate) {
        validateDateRange(startDate, endDate);
        List<Statistic> statistics = statisticRepository.findByMeterIdAndTimestampBetween(id, startDate, endDate);
        if (statistics.isEmpty()) {
            throw new ResourceNotFoundException("No statistics found for the specified meter ID and date range.");
        }
        return statistics;
    }

    @Override
    @Transactional
    public List<Statistic> removeStatisticsByMeterIdAndTimestamp(String id, LocalDateTime startDate, LocalDateTime endDate) {
        validateDateRange(startDate, endDate);
        List<Statistic> statistics = statisticRepository.findByMeterIdAndTimestampBetween(id, startDate, endDate);
        if (statistics.isEmpty()) {
            throw new ResourceNotFoundException("No statistics found for the specified meter ID and date range to remove.");
        }
        statisticRepository.deleteAllBySmartMeterIdAndTimestampBetween(id, startDate, endDate);
        return statistics;
    }

    private void validateDateRange(LocalDateTime startTime, LocalDateTime endTime) {
        if (startTime.isAfter(endTime)) {
            throw new ValidationException("Start time must be before end time.");
        }
    }
}
