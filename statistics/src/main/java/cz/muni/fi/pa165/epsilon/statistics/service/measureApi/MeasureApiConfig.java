package cz.muni.fi.pa165.epsilon.statistics.service.measureApi;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.epsilon.smart_meter_management.client.MeasureApi;
import cz.muni.fi.pa165.epsilon.smart_meter_management.client.invoker.ApiClient;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.net.http.HttpClient;
import java.time.Duration;

@Configuration
public class MeasureApiConfig {

    private final ApplicationContext context;

    public MeasureApiConfig(ApplicationContext context) {
        this.context = context;
    }

    @Bean
    public MeasureApi MeasureApi() {
        HttpClient.Builder httpClientBuilder = HttpClient.newBuilder()
                .connectTimeout(Duration.ofSeconds(30))
                .version(HttpClient.Version.HTTP_2);

        ApiClient apiClient = new ApiClient(httpClientBuilder, new ObjectMapper(), null);
        apiClient.setRequestInterceptor(request -> request.header("Authorization", "Bearer " + getBearerToken()));

        return new MeasureApi(apiClient);
    }

    private String getBearerToken() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String token = request.getHeader("Authorization");
        if (token != null && token.startsWith("Bearer ")) {
            return token.substring(7);
        }
        return null;
    }
}
