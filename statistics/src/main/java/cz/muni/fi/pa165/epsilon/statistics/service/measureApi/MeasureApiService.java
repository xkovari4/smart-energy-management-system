package cz.muni.fi.pa165.epsilon.statistics.service.measureApi;

import cz.muni.fi.pa165.epsilon.smart_meter_management.client.model.MeasureDTO;
import cz.muni.fi.pa165.epsilon.statistics.exceptions.ResourceNotFoundException;

import java.util.List;

public interface MeasureApiService {

    /**
     * Retrieves all measures associated with a specified smart meter ID.
     * @param meterId the ID of the smart meter
     * @return list of MeasureDTOs representing the measures for the specified meter
     * @throws ResourceNotFoundException if the API call fails or no measures are found
     */
    List<MeasureDTO> listAllForSmartMeterId(String meterId);
}
