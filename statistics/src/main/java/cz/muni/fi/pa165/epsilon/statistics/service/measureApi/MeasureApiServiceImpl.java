package cz.muni.fi.pa165.epsilon.statistics.service.measureApi;

import cz.muni.fi.pa165.epsilon.smart_meter_management.client.MeasureApi;
import cz.muni.fi.pa165.epsilon.smart_meter_management.client.invoker.ApiException;
import cz.muni.fi.pa165.epsilon.smart_meter_management.client.model.MeasureDTO;
import cz.muni.fi.pa165.epsilon.statistics.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MeasureApiServiceImpl implements MeasureApiService {
    private final MeasureApi measureApi;

    @Autowired
    public MeasureApiServiceImpl(MeasureApi measureApi) {
        this.measureApi = measureApi;
    }

    @Override
    @Transactional(readOnly = true)
    public List<MeasureDTO> listAllForSmartMeterId(String meterId) {
        try {
            return measureApi.listAllForSmartMeterId(meterId);
        } catch (ApiException e) {
            throw new ResourceNotFoundException(e.getMessage());
        }
    }
}
