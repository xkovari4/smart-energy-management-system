package cz.muni.fi.pa165.epsilon.statistics.service.smartMeterApi;

import cz.muni.fi.pa165.epsilon.statistics.exceptions.ResourceNotFoundException;

import java.util.List;

public interface SmartMeterApiService {

    /**
     * Retrieves all distinct meter IDs from all available smart meters.
     * @return a list of unique meter IDs
     * @throws ResourceNotFoundException if the API call fails
     */
    List<String> findAllDistinctMeterIds();
}
