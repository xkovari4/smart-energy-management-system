package cz.muni.fi.pa165.epsilon.statistics.service.smartMeterApi;

import cz.muni.fi.pa165.epsilon.smart_meter_management.client.SmartMeterApi;
import cz.muni.fi.pa165.epsilon.smart_meter_management.client.model.SmartMeterDTO;
import cz.muni.fi.pa165.epsilon.smart_meter_management.client.invoker.ApiException;
import cz.muni.fi.pa165.epsilon.statistics.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SmartMeterApiServiceImpl implements SmartMeterApiService {

    private final SmartMeterApi smartMeterApi;

    @Autowired
    public SmartMeterApiServiceImpl(SmartMeterApi smartMeterApi) {
        this.smartMeterApi = smartMeterApi;
    }

    @Override
    @Transactional(readOnly = true)
    public List<String> findAllDistinctMeterIds() {
        try {
            var allSmartMeters = smartMeterApi.listAll();
            return allSmartMeters.stream()
                    .map(SmartMeterDTO::getId).distinct().collect(Collectors.toList());
        } catch (ApiException e) {
            throw new ResourceNotFoundException(e.getMessage());
        }
    }
}
