package cz.muni.fi.pa165.epsilon.statistics.api;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests for StatisticDTO.
 *
 * @author Lukas Kovarik
 */
class StatisticDTOTest {
    @Test
    void setId() {
        var statisticDto = new StatisticDTO();
        statisticDto.setId("1");

        assertThat(statisticDto.getId()).isEqualTo("1");
    }

    @Test
    void setSmartMeterId() {
        var statisticDto = new StatisticDTO();
        statisticDto.setSmartMeterId("1");

        assertThat(statisticDto.getSmartMeterId()).isNotEmpty();
        assertThat(statisticDto.getSmartMeterId()).isEqualTo("1");
    }

    @Test
    void setTimestamp() {
        var statisticDto = new StatisticDTO();
        var now = LocalDateTime.now();
        statisticDto.setTimestamp(now);

        assertThat(statisticDto.getTimestamp()).isEqualTo(now);
    }

    @Test
    void setTotalConsumption() {
        var statisticDto = new StatisticDTO();
        statisticDto.setTotalConsumption(1.0);

        assertThat(statisticDto.getTotalConsumption()).isEqualTo(1.0);
    }

    @Test
    void setAverageConsumption() {
        var statisticDto = new StatisticDTO();
        statisticDto.setAverageConsumption(1.0);

        assertThat(statisticDto.getAverageConsumption()).isEqualTo(1.0);
    }
}