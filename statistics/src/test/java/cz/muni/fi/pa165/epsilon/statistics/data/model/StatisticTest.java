package cz.muni.fi.pa165.epsilon.statistics.data.model;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests for Statistic.
 *
 * @author Lukas Kovarik
 */
class StatisticTest {

    @Test
    void setId() {
        var statistic = new Statistic("1", "1", LocalDateTime.now(), 10.0, 5.0, false);
        statistic.setId("2");

        assertThat(statistic.getId()).isEqualTo("2");
    }

    @Test
    void setSmartMeterId() {
        var statistic = new Statistic("1", "1", LocalDateTime.now(), 10.0, 5.0, false);
        statistic.setSmartMeterId("2");

        assertThat(statistic.getSmartMeterId()).isNotEmpty();
        assertThat(statistic.getSmartMeterId()).isEqualTo("2");
    }

    @Test
    void setTimestamp() {
        var statistic = new Statistic("1", "1", LocalDateTime.now(), 10.0, 5.0, false);
        var now = LocalDateTime.now();
        statistic.setTimestamp(now);

        assertThat(statistic.getTimestamp()).isEqualTo(now);
    }

    @Test
    void setTotalConsumption() {
        var statistic = new Statistic("1", "1", LocalDateTime.now(), 10.0, 5.0, false);
        statistic.setTotalConsumption(1.0);

        assertThat(statistic.getTotalConsumption()).isEqualTo(1.0);
    }

    @Test
    void setAverageConsumption() {
        var statistic = new Statistic("1", "1", LocalDateTime.now(), 10.0, 5.0, false);
        statistic.setAverageConsumption(1.0);

        assertThat(statistic.getAverageConsumption()).isEqualTo(1.0);
    }
}