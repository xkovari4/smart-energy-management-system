package cz.muni.fi.pa165.epsilon.statistics.data.repository;

import cz.muni.fi.pa165.epsilon.statistics.data.model.Statistic;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class StatisticRepositoryTest {
    @Autowired
    private StatisticRepository statisticRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    private final LocalDateTime TEST_TIME = LocalDateTime.now();
    private static final String TEST_ID_1 = "123e4567-e89b-12d3-a456-426614174001";
    private static final String TEST_ID_2 = "123e4567-e89b-12d3-a456-426614174002";
    private Statistic statistic1;
    private Statistic statistic2;
    private Statistic statistic3;
    private Statistic statistic4;

    @BeforeEach
    void setUp() {
        statistic1 = new Statistic();
        statistic1.setSmartMeterId(TEST_ID_1);
        statistic1.setTimestamp(TEST_TIME);
        statistic1.setTotalConsumption(1.0);
        statistic1.setAverageConsumption(2.0);
        statistic1.setAggregated(true);

        statistic2 = new Statistic();
        statistic2.setSmartMeterId(TEST_ID_1);
        statistic2.setTimestamp(TEST_TIME.plusHours(1));
        statistic2.setTotalConsumption(2.0);
        statistic2.setAverageConsumption(3.0);
        statistic2.setAggregated(true);

        statistic3 = new Statistic();
        statistic3.setSmartMeterId(TEST_ID_2);
        statistic3.setTimestamp(TEST_TIME.plusHours(2));
        statistic3.setTotalConsumption(4.0);
        statistic3.setAverageConsumption(5.0);
        statistic3.setAggregated(true);

        statistic4 = new Statistic();
        statistic4.setSmartMeterId(TEST_ID_2);
        statistic4.setTimestamp(TEST_TIME.plusHours(3));
        statistic4.setTotalConsumption(6.0);
        statistic4.setAverageConsumption(7.0);
        statistic4.setAggregated(true);

        testEntityManager.persist(statistic1);
        testEntityManager.persist(statistic2);
        testEntityManager.persist(statistic3);
        testEntityManager.persist(statistic4);
    }

    @Test
    void findByMeterIdAndTimestampBetween_withinInterval_returnsStatistics() {
        var result = statisticRepository.findByMeterIdAndTimestampBetween(TEST_ID_1, TEST_TIME.minusDays(1), TEST_TIME.plusDays(1));

        assertThat(result.size()).isEqualTo(2);
        assertThat(result).containsAll(List.of(statistic1, statistic2));
    }

    @Test
    void findByMeterIdAndTimestampBetween_outsideInterval_returnsEmptyList() {
        var result = statisticRepository.findByMeterIdAndTimestampBetween(TEST_ID_1, TEST_TIME.plusDays(1), TEST_TIME.plusDays(2));

        assertThat(result).isEmpty();
    }

    @Test
    void deleteStatisticsByMeterIdAndTimestamp_statisticExists_deletesSuccessfully() {
        var id1 = testEntityManager.getId(statistic1);
        var id2 = testEntityManager.getId(statistic2);
        var id3 = testEntityManager.getId(statistic3);
        var id4 = testEntityManager.getId(statistic4);

        statisticRepository.deleteAllBySmartMeterIdAndTimestampBetween(TEST_ID_1, TEST_TIME.minusDays(1), TEST_TIME.plusDays(1));

        assertThat(testEntityManager.find(Statistic.class, id1)).isNull();
        assertThat(testEntityManager.find(Statistic.class, id2)).isNull();
        assertThat(testEntityManager.find(Statistic.class, id3)).isEqualTo(statistic3);
        assertThat(testEntityManager.find(Statistic.class, id4)).isEqualTo(statistic4);
    }

    @Test
    void deleteStatisticsByMeterIdAndTimestamp_statisticNotExists_TODO() {
        var id1 = testEntityManager.getId(statistic1);
        var id2 = testEntityManager.getId(statistic2);
        var id3 = testEntityManager.getId(statistic3);
        var id4 = testEntityManager.getId(statistic4);

        statisticRepository.deleteAllBySmartMeterIdAndTimestampBetween("123e4567-e89b-12d3-a456-4266141740123", TEST_TIME.plusDays(1), TEST_TIME.plusDays(2));

        assertThat(testEntityManager.find(Statistic.class, id1)).isEqualTo(statistic1);
        assertThat(testEntityManager.find(Statistic.class, id2)).isEqualTo(statistic2);
        assertThat(testEntityManager.find(Statistic.class, id3)).isEqualTo(statistic3);
        assertThat(testEntityManager.find(Statistic.class, id4)).isEqualTo(statistic4);
    }

    @Test
    void findBySmartMeterId_statisticsExists_returnsStatistics() {
        var result = statisticRepository.findBySmartMeterId(TEST_ID_1);

        assertThat(result.size()).isEqualTo(2);
        assertThat(result).containsAll(List.of(statistic1, statistic2));
    }

    @Test
    void findBySmartMeterId_statisticsNotExists_returnsEmptyList() {
        var result = statisticRepository.findBySmartMeterId("123e4567-e89b-12d3-a456-4266141740123");

        assertThat(result).isEmpty();
    }

    @Test
    void deleteBySmartMeterId_statisticsExists_deletesSuccessfully() {
        var id1 = testEntityManager.getId(statistic1);
        var id2 = testEntityManager.getId(statistic2);
        var id3 = testEntityManager.getId(statistic3);
        var id4 = testEntityManager.getId(statistic4);

        statisticRepository.deleteBySmartMeterId(TEST_ID_1);

        assertThat(testEntityManager.find(Statistic.class, id1)).isNull();
        assertThat(testEntityManager.find(Statistic.class, id2)).isNull();
        assertThat(testEntityManager.find(Statistic.class, id3)).isEqualTo(statistic3);
        assertThat(testEntityManager.find(Statistic.class, id4)).isEqualTo(statistic4);
    }

    @Test
    void deleteBySmartMeterId_statisticsNotExists_TODO() {
        var id1 = testEntityManager.getId(statistic1);
        var id2 = testEntityManager.getId(statistic2);
        var id3 = testEntityManager.getId(statistic3);
        var id4 = testEntityManager.getId(statistic4);

        statisticRepository.deleteBySmartMeterId("123e4567-e89b-12d3-a456-4266141740123");

        assertThat(testEntityManager.find(Statistic.class, id1)).isEqualTo(statistic1);
        assertThat(testEntityManager.find(Statistic.class, id2)).isEqualTo(statistic2);
        assertThat(testEntityManager.find(Statistic.class, id3)).isEqualTo(statistic3);
        assertThat(testEntityManager.find(Statistic.class, id4)).isEqualTo(statistic4);
    }
}