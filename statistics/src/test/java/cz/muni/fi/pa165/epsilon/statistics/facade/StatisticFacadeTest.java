package cz.muni.fi.pa165.epsilon.statistics.facade;

import cz.muni.fi.pa165.epsilon.statistics.api.StatisticDTO;
import cz.muni.fi.pa165.epsilon.statistics.data.model.Statistic;
import cz.muni.fi.pa165.epsilon.statistics.mappers.StatisticMapper;
import cz.muni.fi.pa165.epsilon.statistics.service.StatisticService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Tests for StatisticFacade.
 *
 * @author Lukas Kovarik
 */
@ExtendWith(MockitoExtension.class)
class StatisticFacadeTest {
    @Mock
    private StatisticMapper statisticMapper;

    @Mock
    private StatisticService statisticService;

    @InjectMocks
    private StatisticFacadeImpl statisticFacade;

    private Statistic statistic;
    private StatisticDTO statisticDto;

    @BeforeEach
    void setUp() {
        statistic = new Statistic("1", "1", LocalDateTime.now(), 10.0, 5.0, false);
        statisticDto = new StatisticDTO();
        statisticDto.setId("1");
        statisticDto.setSmartMeterId("2");
        statisticDto.setTimestamp(LocalDateTime.now());
        statisticDto.setTotalConsumption(15.0);
        statisticDto.setAverageConsumption(10.0);
        statisticDto.setAggregated(false);

    }

    @Test
    void findStatisticsByMeterId_StatisticsFound_ReturnsStatistics() {
        var id = "1";

        // Arrange
        Mockito.when(statisticService.findStatisticsByMeterId(id)).thenReturn(List.of(statistic));
        Mockito.when(statisticMapper.mapToDto(statistic)).thenReturn(statisticDto);

        // Act
        List<StatisticDTO> foundStatistics = statisticFacade.findStatisticsByMeterId(id);

        // Assert
        assertThat(foundStatistics).isNotEmpty();
        assertThat(foundStatistics).isEqualTo(List.of(statisticDto));
    }

    @Test
    void findStatisticsByMeterId_StatisticsNotFound_ReturnsEmptyList() {
        var id = "1";

        // Arrange
        Mockito.when(statisticService.findStatisticsByMeterId(id)).thenReturn(List.of());

        // Act
        List<StatisticDTO> foundStatistics = statisticFacade.findStatisticsByMeterId(id);

        // Assert
        assertThat(foundStatistics).isEmpty();
        Mockito.verify(statisticMapper, Mockito.never()).mapToDto(Mockito.any(Statistic.class));  // Verify mapper is never called
    }

    @Test
    void findStatisticsByMeterId_NullId_ThrowsException() {
        // Act & Assert
        assertThrows(IllegalArgumentException.class, () -> {
            statisticFacade.findStatisticsByMeterId(null);
        });
    }

    @Test
    void listAll_emptyRepository_returnsEmptyList() {
        // Arrange
        Mockito.when(statisticService.listAll()).thenReturn(List.of());

        // Act
        List<StatisticDTO> statistics = statisticFacade.listAll();

        // Assert
        assertThat(statistics).hasSize(0);
    }

    @Test
    void listAll_oneElement_returnsOneElementList() {
        // Arrange
        Mockito.when(statisticService.listAll()).thenReturn(List.of(statistic));
        Mockito.when(statisticMapper.mapToDto(statistic)).thenReturn(statisticDto);

        // Act
        List<StatisticDTO> statistics = statisticFacade.listAll();

        // Assert
        assertThat(statistics).hasSize(1);
        assertThat(statistics).containsExactly(statisticDto);
    }

    @Test
    void removeStatisticsByMeterId_StatisticsExist_ReturnsRemovedStatistics() {
        var id = "1";

        // Arrange
        Mockito.when(statisticService.removeStatisticsByMeterId(id)).thenReturn(List.of(statistic));
        Mockito.when(statisticMapper.mapToDto(statistic)).thenReturn(statisticDto);

        // Act
        List<StatisticDTO> removedStatistics = statisticFacade.removeStatisticsByMeterId(id);

        // Assert
        assertThat(removedStatistics).isNotEmpty();
        assertThat(removedStatistics).isEqualTo(List.of(statisticDto));
    }

    @Test
    void removeStatisticsByMeterId_NoStatisticsExist_ReturnsEmptyList() {
        var id = "1";

        // Arrange
        Mockito.when(statisticService.removeStatisticsByMeterId(id)).thenReturn(List.of());

        // Act
        List<StatisticDTO> removedStatistics = statisticFacade.removeStatisticsByMeterId(id);

        // Assert
        assertThat(removedStatistics).isEmpty();
    }

    @Test
    void removeStatisticsByMeterId_NullId_ThrowsException() {
        // Act & Assert
        assertThrows(IllegalArgumentException.class, () -> {
            statisticFacade.removeStatisticsByMeterId(null);
        });
    }

    @Test
    void generatePeriodicMeterConsumptionStatistics_validStatistic_returnsAggregatedPeriodicSmartMeterStatistic() {
        var meterId = "1";
        var now = LocalDateTime.now();

        // Arrange
        Mockito.when(statisticService.generatePeriodicMeterConsumptionStatistics(meterId, now, now)).thenReturn(statistic);
        Mockito.when(statisticMapper.mapToDto(statistic)).thenReturn(statisticDto);

        // Act
        StatisticDTO statisticOut = statisticFacade.generatePeriodicMeterConsumptionStatistics(meterId, now, now);

        // Assert
        assertThat(statisticOut).isEqualTo(statisticDto);
    }

    @Test
    void generatePeriodicMeterConsumptionStatistics_InvalidPeriod_ThrowsException() {
        var meterId = "1";
        var now = LocalDateTime.now();

        // Act & Assert
        assertThrows(IllegalArgumentException.class, () -> {
            statisticFacade.generatePeriodicMeterConsumptionStatistics(meterId, now, now.minusDays(1));
        });
    }

    @Test
    void aggregateMeterConsumptionStatistics_validStatistic_returnsAggregatedSmartMeterStatistic() {
        var meterId = "1";

        // Arrange
        Mockito.when(statisticService.aggregateMeterConsumptionStatistics(meterId)).thenReturn(statistic);
        Mockito.when(statisticMapper.mapToDto(statistic)).thenReturn(statisticDto);

        // Act
        StatisticDTO statisticOut = statisticFacade.aggregateMeterConsumptionStatistics(meterId);

        // Assert
        assertThat(statisticOut).isEqualTo(statisticDto);
    }

    @Test
    void aggregateMeterConsumptionStatistics_NullMeterId_ThrowsException() {
        // Act & Assert
        assertThrows(IllegalArgumentException.class, () -> {
            statisticFacade.aggregateMeterConsumptionStatistics(null);
        });
    }

    @Test
    void takeStatisticsSnapshot_validId_serviceCalled() {
        var meterId = "1";

        // Act
        statisticFacade.takeStatisticsSnapshot(meterId);

        // Assert
        Mockito.verify(statisticService).takeStatisticsSnapshot(meterId);
    }

    @Test
    void takeStatisticsSnapshot_NullId_ThrowsException() {
        // Act & Assert
        assertThrows(IllegalArgumentException.class, () -> {
            statisticFacade.takeStatisticsSnapshot(null);
        });
    }

    @Test
    void findStatisticsByIdAndTimestamp_validData_serviceCalled() {
        var meterId = "1";
        var timestampFrom = LocalDateTime.now().minusDays(1);
        var timestampTo = LocalDateTime.now().minusDays(1);

        // Act
        statisticFacade.findStatisticsByIdAndTimestamp(meterId, timestampFrom, timestampTo);

        // Assert
        Mockito.verify(statisticService).findByMeterIdAndTimestamp(meterId, timestampFrom, timestampTo);
    }

    @Test
    void findStatisticsByIdAndTimestamp_InvalidPeriod_ThrowsException() {
        var meterId = "1";
        var timestampFrom = LocalDateTime.now();
        var timestampTo = LocalDateTime.now().minusDays(1);

        // Act & Assert
        assertThrows(IllegalArgumentException.class, () -> {
            statisticFacade.findStatisticsByIdAndTimestamp(meterId, timestampFrom, timestampTo);
        });
    }

    @Test
    void removeStatisticsByMeterIdAndTimestamp_validData_serviceCalled() {
        var meterId = "1";
        var timestampFrom = LocalDateTime.now().minusDays(1);
        var timestampTo = LocalDateTime.now().minusDays(1);

        // Act
        statisticFacade.removeStatisticsByMeterIdAndTimestamp(meterId, timestampFrom, timestampTo);

        // Assert
        Mockito.verify(statisticService).removeStatisticsByMeterIdAndTimestamp(meterId, timestampFrom, timestampTo);
    }

    @Test
    void removeStatisticsByMeterIdAndTimestamp_InvalidPeriod_ThrowsException() {
        var meterId = "1";
        var timestampFrom = LocalDateTime.now();
        var timestampTo = LocalDateTime.now().minusDays(1);

        // Act & Assert
        assertThrows(IllegalArgumentException.class, () -> {
            statisticFacade.removeStatisticsByMeterIdAndTimestamp(meterId, timestampFrom, timestampTo);
        });
    }
}