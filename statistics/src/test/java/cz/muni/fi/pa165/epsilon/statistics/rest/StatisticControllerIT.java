package cz.muni.fi.pa165.epsilon.statistics.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import cz.muni.fi.pa165.epsilon.statistics.api.StatisticDTO;
import cz.muni.fi.pa165.epsilon.statistics.data.model.Statistic;
import cz.muni.fi.pa165.epsilon.statistics.data.repository.StatisticRepository;
import cz.muni.fi.pa165.epsilon.statistics.mappers.StatisticMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@WithMockUser(authorities = {"SCOPE_test_write", "SCOPE_test_read", "SCOPE_test_1", "SCOPE_test_2"})
@AutoConfigureMockMvc
@Transactional
class StatisticControllerIT {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private StatisticRepository statisticRepository;

    @Autowired
    private StatisticMapper statisticMapper;

    private static Statistic TEST_STATISTIC_1;
    private static Statistic TEST_STATISTIC_2;
    private static final String TEST_STAT_ID_1 = "123e4567-e89b-12d3-a456-426614174000";
    private static final String TEST_STAT_ID_2 = "123e4567-e89b-12d3-a456-426614174001";
    private static final String TEST_SMART_METER_ID_1 = "123e4567-e89b-12d3-a456-426614174002";

    private final LocalDateTime TEST_DATE = LocalDateTime.now();

    @BeforeEach
    void setUp() {
        statisticRepository.deleteAll();

        TEST_STATISTIC_1 = new Statistic(TEST_STAT_ID_1, TEST_SMART_METER_ID_1, TEST_DATE, 1.0, 2.0, true);
        TEST_STATISTIC_2 = new Statistic(TEST_STAT_ID_2, "123e4567-e89b-12d3-a456-426614174003", TEST_DATE, 3.0, 4.0, true);

        statisticRepository.saveAndFlush(TEST_STATISTIC_1);
        statisticRepository.saveAndFlush(TEST_STATISTIC_2);

        var x = statisticRepository.findAll();
        TEST_STATISTIC_1.setId(x.get(0).getId());
        TEST_STATISTIC_2.setId(x.get(1).getId());
    }

    @Test
    void listAll_statisticsPresent_returnsStatistics() throws Exception {
        String responseJson = mockMvc.perform(get("/statistics/statistics")
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        TypeReference<List<StatisticDTO>> listType = new TypeReference<>() {};
        List<StatisticDTO> jacksonList = objectMapper.readValue(responseJson, listType);

        assertThat(jacksonList).size().isEqualTo(2);
        assertThat(jacksonList.get(0)).isEqualTo(statisticMapper.mapToDto(TEST_STATISTIC_1));
        assertThat(jacksonList.get(1)).isEqualTo(statisticMapper.mapToDto(TEST_STATISTIC_2));
    }

    @Test
    void findStatisticsByMeterId_statisticExists_returnsStatistic() throws Exception {
        String responseJson = mockMvc.perform(get("/statistics/{meterId}", TEST_SMART_METER_ID_1)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        TypeReference<List<StatisticDTO>> listType = new TypeReference<>() {};
        List<StatisticDTO> jacksonList = objectMapper.readValue(responseJson, listType);

        assertThat(jacksonList).size().isEqualTo(1);
        assertThat(jacksonList.get(0)).isEqualTo(statisticMapper.mapToDto(TEST_STATISTIC_1));
    }
}
