package cz.muni.fi.pa165.epsilon.statistics.rest;

import cz.muni.fi.pa165.epsilon.statistics.api.StatisticDTO;
import cz.muni.fi.pa165.epsilon.statistics.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.epsilon.statistics.facade.StatisticFacadeImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Tests for StatisticController.
 *
 * @author Lukas Kovarik
 */
@ExtendWith(MockitoExtension.class)
class StatisticControllerTest {
    @Mock
    private StatisticFacadeImpl statisticFacade;

    @InjectMocks
    private StatisticControllerImpl statisticController;

    private StatisticDTO statisticDto;

    @BeforeEach
    void setUp() {
        statisticDto = new StatisticDTO();
        statisticDto.setId("1");
        statisticDto.setSmartMeterId("2");
        statisticDto.setTimestamp(LocalDateTime.now());
        statisticDto.setTotalConsumption(15.0);
        statisticDto.setAverageConsumption(10.0);
    }

    @Test
    void findStatisticsByMeterId_WhenStatisticsFound_ReturnsStatistics() {
        var id = "1";

        // Arrange
        Mockito.when(statisticFacade.findStatisticsByMeterId(id)).thenReturn(List.of(statisticDto));

        // Act
        ResponseEntity<List<StatisticDTO>> response = statisticController.findStatisticsByMeterId(id);

        // Assert
        assertThat(response.getBody()).isEqualTo(List.of(statisticDto));
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void findStatisticsByMeterId_WhenNoStatisticsFound_ThrowsResourceNotFoundException() {
        var id = "1";

        // Arrange
        Mockito.when(statisticFacade.findStatisticsByMeterId(id))
                .thenThrow(new ResourceNotFoundException("No statistics found for meter ID: " + id));

        // Act & Assert
        Exception exception = assertThrows(ResourceNotFoundException.class, () -> {
            statisticController.findStatisticsByMeterId(id);
        });
        assertThat(exception.getMessage()).contains("No statistics found for meter ID: " + id);
    }

    @Test
    void listAll_emptyRepository_returnsEmptyList() {
        // Arrange
        Mockito.when(statisticFacade.listAll()).thenReturn(List.of());

        // Act
        ResponseEntity<List<StatisticDTO>> response = statisticController.listAll();

        // Assert
        assertThat(response.hasBody()).isEqualTo(true);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(List.of());
    }

    @Test
    void listAll_oneElement_returnsOneElementList() {
        // Arrange
        Mockito.when(statisticFacade.listAll()).thenReturn(List.of(statisticDto));

        // Act
        ResponseEntity<List<StatisticDTO>> response = statisticController.listAll();

        // Assert
        assertThat(response.hasBody()).isEqualTo(true);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(List.of(statisticDto));
    }

    @Test
    void removeStatisticsByMeterId_WhenStatisticsExist_ReturnsOkWithStatistics() {
        var id = "1";

        // Arrange
        Mockito.when(statisticFacade.removeStatisticsByMeterId(id)).thenReturn(List.of(statisticDto));

        // Act
        ResponseEntity<List<StatisticDTO>> response = statisticController.removeStatisticsByMeterId(id);

        // Assert
        assertThat(response.getBody()).isEqualTo(List.of(statisticDto));
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void removeStatisticsByMeterId_WhenNoStatisticsExist_ReturnsNotFound() {
        var id = "1";

        // Arrange
        Mockito.when(statisticFacade.removeStatisticsByMeterId(id))
                .thenThrow(new ResourceNotFoundException("No statistics found for meter ID: " + id));

        // Act & Assert
        Exception exception = assertThrows(ResourceNotFoundException.class, () -> {
            statisticController.removeStatisticsByMeterId(id);
        });
        assertThat(exception.getMessage()).contains("No statistics found for meter ID: " + id);
    }

    @Test
    void generateStatistics_validInput_returnsGeneratedStatistics() {
        var id = "1";
        var now = LocalDateTime.now();

        // Arrange
        Mockito.when(statisticFacade.generatePeriodicMeterConsumptionStatistics(id, now, now)).thenReturn(statisticDto);

        // Act
        ResponseEntity<StatisticDTO> response = statisticController.aggregateStatistics(id, now, now);

        // Assert
        assertThat(response.hasBody()).isEqualTo(true);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(response.getBody()).isEqualTo(statisticDto);
    }

    @Test
    void generateStatistics_invalidInput_throwsResourceNotFoundException() {
        var id = "1";
        var now = LocalDateTime.now();

        // Arrange
        Mockito.when(statisticFacade.generatePeriodicMeterConsumptionStatistics(id, now, now))
                .thenThrow(new ResourceNotFoundException("No statistics found for meter ID between the specified dates: " + id));

        // Act & Assert
        Exception exception = assertThrows(ResourceNotFoundException.class, () -> {
            statisticController.aggregateStatistics(id, now, now);
        });
        assertThat(exception.getMessage()).contains("No statistics found for meter ID between the specified dates: " + id);
    }

    @Test
    void findStatisticsByIdAndTimestamp_statisticsExists_returnsStatistics() {
        var id = "1";
        var now = LocalDateTime.now();

        // Arrange
        Mockito.when(statisticFacade.findStatisticsByIdAndTimestamp(id, now, now)).thenReturn(List.of(statisticDto));

        // Act
        ResponseEntity<List<StatisticDTO>> response = statisticController.findStatisticsByIdAndTimestamp(id, now, now);

        // Assert
        assertThat(response.hasBody()).isEqualTo(true);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(List.of(statisticDto));
    }

    @Test
    void findStatisticsByIdAndTimestamp_noStatistics_returnsNotFound() {
        var id = "1";
        var now = LocalDateTime.now();

        // Arrange
        Mockito.when(statisticFacade.findStatisticsByIdAndTimestamp(id, now, now)).thenThrow(ResourceNotFoundException.class);

        // Act
        assertThrows(ResourceNotFoundException.class, () -> statisticController.findStatisticsByIdAndTimestamp(id, now, now));
    }

    @Test
    void removeStatisticsByMeterIdAndTimestamp_statisticsExists_deletesAndReturnsStatistics() {
        var id = "1";
        var now = LocalDateTime.now();

        // Arrange
        Mockito.when(statisticFacade.removeStatisticsByMeterIdAndTimestamp(id, now, now)).thenReturn(List.of(statisticDto));

        // Act
        ResponseEntity<List<StatisticDTO>> response = statisticController.removeStatisticsByMeterIdAndTimestamp(id, now, now);

        // Assert
        assertThat(response.hasBody()).isEqualTo(true);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(List.of(statisticDto));
    }

    @Test
    void removeStatisticsByMeterIdAndTimestamp_noStatistics_returnsNotFound() {
        var id = "1";
        var now = LocalDateTime.now();

        // Arrange
        Mockito.when(statisticFacade.removeStatisticsByMeterIdAndTimestamp(id, now, now)).thenThrow(ResourceNotFoundException.class);

        // Act
        assertThrows(ResourceNotFoundException.class, () -> statisticController.removeStatisticsByMeterIdAndTimestamp(id, now, now));
    }

    @Test
    void createStatistic_validInput_returnsCreatedStatistic() {
        var id = "1";
        var now = LocalDateTime.now();

        // Arrange
        Mockito.when(statisticFacade.takeStatisticsSnapshot(id)).thenReturn(statisticDto);

        // Act
        ResponseEntity<StatisticDTO> response = statisticController.createStatistic(id);

        // Assert
        assertThat(response.hasBody()).isEqualTo(true);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(response.getBody()).isEqualTo(statisticDto);
    }

    @Test
    void createStatistic_smartMeterNotExists_returnsNotFound() {
        var id = "1";
        var now = LocalDateTime.now();

        // Arrange
        Mockito.when(statisticFacade.takeStatisticsSnapshot(id)).thenThrow(ResourceNotFoundException.class);

        // Act
        assertThrows(ResourceNotFoundException.class, () -> statisticController.createStatistic(id));
    }
}