package cz.muni.fi.pa165.epsilon.statistics.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import cz.muni.fi.pa165.epsilon.statistics.api.StatisticDTO;
import cz.muni.fi.pa165.epsilon.statistics.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.epsilon.statistics.facade.StatisticFacade;
import cz.muni.fi.pa165.epsilon.statistics.utils.ObjectConverter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/**
 * MVC tests for StatisticController.
 *
 * @author Lukas Kovarik
 */
@SpringBootTest
@WithMockUser(authorities = {"SCOPE_test_write", "SCOPE_test_read", "SCOPE_test_1", "SCOPE_test_2"})
@AutoConfigureMockMvc
public class StatisticControllerWebMvcTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StatisticFacade statisticFacade;

    private StatisticDTO statisticDto;

    @BeforeEach
    void setUp() {
        statisticDto = new StatisticDTO();
        statisticDto.setId("1");
        statisticDto.setTimestamp(LocalDateTime.now());
    }

    @Test
    void findStatisticsByMeterId_WhenStatisticsFound_ReturnsStatistics() throws Exception {
        var id = "1";

        // Arrange
        Mockito.when(statisticFacade.findStatisticsByMeterId(id)).thenReturn(List.of(statisticDto));

        // Act
        String responseJson = mockMvc.perform(get("/statistics/{meterId}", id)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        List<StatisticDTO> responseList = ObjectConverter.convertJsonToObject(responseJson, new TypeReference<List<StatisticDTO>>(){});

        // Assert
        assertThat(responseList).isNotEmpty();
        assertThat(responseList).containsExactly(statisticDto);
    }

    @Test
    void findStatisticsByMeterId_WhenNoStatisticsFound_ReturnsNotFound() throws Exception {
        var id = "1";

        // Arrange
        Mockito.when(statisticFacade.findStatisticsByMeterId(id))
                .thenThrow(new ResourceNotFoundException("No statistics found for meter ID: " + id));

        // Act
        mockMvc.perform(get("/statistics/{meterId}", id)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andReturn().getResponse();
    }

    @Test
    void listAll_oneElement_returnsOneElementList() throws Exception {
        // Arrange
        Mockito.when(statisticFacade.listAll()).thenReturn(List.of(statisticDto));

        // Act
         String responseJson = mockMvc.perform(get("/statistics/statistics")
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        StatisticDTO response = ObjectConverter.convertJsonToObject(responseJson.replace('[', ' ').replace(']', ' '), StatisticDTO.class);

        // Assert
        assertThat(response).isEqualTo(statisticDto);
    }

    @Test
    void removeStatisticsByMeterId_WhenStatisticsExist_ReturnsOk() throws Exception {
        var id = "1";

        // Arrange
        Mockito.when(statisticFacade.removeStatisticsByMeterId(id)).thenReturn(List.of(statisticDto));

        // Act
        mockMvc.perform(delete("/statistics/{meterId}", id)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn().getResponse();
    }

    @Test
    void removeStatisticsByMeterId_WhenNoStatisticsExist_ReturnsNotFound() throws Exception {
        var id = "1";

        // Arrange
        Mockito.when(statisticFacade.removeStatisticsByMeterId(id))
                .thenThrow(new ResourceNotFoundException("No statistics found for meter ID: " + id));

        // Act
        mockMvc.perform(delete("/statistics/{meterId}", id)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andReturn().getResponse();
    }

    @Test
    void generateStatistics_validInput_returnsGeneratedStatistics() throws Exception {
        var id = "1";
        var now = LocalDateTime.now();

        // Arrange
        Mockito.when(statisticFacade.generatePeriodicMeterConsumptionStatistics(id, now, now)).thenReturn(statisticDto);

        // Act
        String responseJson = mockMvc.perform(post("/statistics/aggregate")
                        .param("meterId", id)
                        .param("start", now.toString())
                        .param("end", now.toString())
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        StatisticDTO response = ObjectConverter.convertJsonToObject(responseJson, StatisticDTO.class);

        // Assert
        assertThat(response).isEqualTo(statisticDto);
    }

    @Test
    void generateStatistics_invalidInput_throwsResourceNotFoundException() throws Exception {
        var id = "1";
        var now = LocalDateTime.now();

        // Arrange
        Mockito.when(statisticFacade.generatePeriodicMeterConsumptionStatistics(id, now, now))
                .thenThrow(new ResourceNotFoundException("No statistics found for meter ID between the specified dates: " + id));

        // Act
        mockMvc.perform(post("/statistics/aggregate")
                        .param("meterId", id)
                        .param("start", now.toString())
                        .param("end", now.toString())
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andReturn().getResponse();
    }

    @Test
    void findStatisticsByIdAndTimestamp_statisticsExists_returnsStatistics() throws Exception {
        var id = "1";
        var now = LocalDateTime.now();

        // Arrange
        Mockito.when(statisticFacade.findStatisticsByIdAndTimestamp(id, now, now)).thenReturn(List.of(statisticDto));

        // Act
        String responseJson = mockMvc.perform(get("/statistics/{meterId}/{startDate}/{endDate}", id, now, now)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        List<StatisticDTO> responseList = ObjectConverter.convertJsonToObject(responseJson, new TypeReference<List<StatisticDTO>>(){});

        // Assert
        assertThat(responseList).isNotEmpty();
        assertThat(responseList).containsExactly(statisticDto);
    }

    @Test
    void findStatisticsByIdAndTimestamp_noStatistics_returnsNotFound() throws Exception {
        var id = "1";
        var now = LocalDateTime.now();

        // Arrange
        Mockito.when(statisticFacade.findStatisticsByIdAndTimestamp(id, now, now)).thenThrow(ResourceNotFoundException.class);

        // Act
        mockMvc.perform(get("/statistics/{meterId}/{startDate}/{endDate}", id, now, now)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andReturn().getResponse();
    }

    @Test
    void removeStatisticsByMeterIdAndTimestamp_statisticsExists_deletesAndReturnsStatistics() throws Exception {
        var id = "1";
        var now = LocalDateTime.now();

        // Arrange
        Mockito.when(statisticFacade.removeStatisticsByMeterIdAndTimestamp(id, now, now)).thenReturn(List.of(statisticDto));

        // Act
        String responseJson = mockMvc.perform(delete("/statistics/{meterId}/{startDate}/{endDate}", id, now, now)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        List<StatisticDTO> responseList = ObjectConverter.convertJsonToObject(responseJson, new TypeReference<List<StatisticDTO>>(){});

        // Assert
        assertThat(responseList).isNotEmpty();
        assertThat(responseList).containsExactly(statisticDto);
    }

    @Test
    void removeStatisticsByMeterIdAndTimestamp_noStatistics_returnsNotFound() throws Exception {
        var id = "1";
        var now = LocalDateTime.now();

        // Arrange
        Mockito.when(statisticFacade.removeStatisticsByMeterIdAndTimestamp(id, now, now)).thenThrow(ResourceNotFoundException.class);

        // Act
        mockMvc.perform(delete("/statistics/{meterId}/{startDate}/{endDate}", id, now, now)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andReturn().getResponse();
    }

    @Test
    void createStatistic_validInput_returnsCreatedStatistic() throws Exception {
        var id = "1";

        // Arrange
        Mockito.when(statisticFacade.takeStatisticsSnapshot(id)).thenReturn(statisticDto);

        // Act
        String responseJson = mockMvc.perform(post("/statistics/{meterId}", id)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        StatisticDTO response = ObjectConverter.convertJsonToObject(responseJson, StatisticDTO.class);

        // Assert
        assertThat(response).isEqualTo(statisticDto);
    }

    @Test
    void createStatistic_smartMeterNotExists_returnsNotFound() throws Exception {
        var id = "1";

        // Arrange
        Mockito.when(statisticFacade.takeStatisticsSnapshot(id)).thenThrow(ResourceNotFoundException.class);

        // Act
        mockMvc.perform(post("/statistics/{meterId}", id)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andReturn().getResponse();
    }
}
