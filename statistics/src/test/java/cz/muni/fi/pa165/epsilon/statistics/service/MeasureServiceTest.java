package cz.muni.fi.pa165.epsilon.statistics.service;

import cz.muni.fi.pa165.epsilon.smart_meter_management.client.MeasureApi;
import cz.muni.fi.pa165.epsilon.smart_meter_management.client.invoker.ApiException;
import cz.muni.fi.pa165.epsilon.smart_meter_management.client.model.MeasureDTO;
import cz.muni.fi.pa165.epsilon.smart_meter_management.client.model.SmartMeterDTO;
import cz.muni.fi.pa165.epsilon.statistics.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.epsilon.statistics.service.measureApi.MeasureApiServiceImpl;
import cz.muni.fi.pa165.epsilon.statistics.service.smartMeterApi.SmartMeterApiServiceImpl;
import cz.muni.fi.pa165.epsilon.smart_meter_management.client.SmartMeterApi;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

/**
 * Dummy tests for mocked implementation.
 * Will be covered in smart-meter-management service.
 *
 * @author Lukas Kovarik
 */
@ExtendWith(MockitoExtension.class)
class MeasureServiceTest {

    @Mock
    private SmartMeterApi smartMeterApi;

    @Mock
    private MeasureApi measureApi;

    @InjectMocks
    private MeasureApiServiceImpl measureApiServiceImpl;

    @InjectMocks
    private SmartMeterApiServiceImpl smartMeterApiServiceImpl;

    @Test
    void listAllForSmartMeterId_dummyValue_returnsNull() {
        var id = "1";

        // Act
        List<MeasureDTO> measureList = measureApiServiceImpl.listAllForSmartMeterId(id);

        // Assert
        assertThat(measureList).isEqualTo(List.of());
    }

    @Test
    void listAllForSmartMeterId_successfulResponse_returnsMeasureList() throws Exception {
        var meterId = "1";
        var measure = new MeasureDTO();
        measure.setId("measure1");
        measure.setSmartMeterId(meterId);

        // Arrange
        when(measureApi.listAllForSmartMeterId(meterId)).thenReturn(List.of(measure));

        // Act
        List<MeasureDTO> measureList = measureApiServiceImpl.listAllForSmartMeterId(meterId);

        // Assert
        assertThat(measureList).isNotEmpty();
        assertThat(measureList).containsExactly(measure);
    }

    @Test
    void listAllForSmartMeterId_apiException_throwsResourceNotFoundException() throws Exception {
        var meterId = "1";

        // Arrange
        when(measureApi.listAllForSmartMeterId(meterId)).thenThrow(new ApiException("API error"));

        // Act & Assert
        Exception exception = assertThrows(ResourceNotFoundException.class, () -> {
            measureApiServiceImpl.listAllForSmartMeterId(meterId);
        });

        assertThat(exception.getMessage()).isEqualTo("API error");
    }

    @Test
    void findAllDistinctMeterIds_dummyValue_returnsNull() {
        // Act
        List<String> idList = smartMeterApiServiceImpl.findAllDistinctMeterIds();

        // Assert
        assertThat(idList).isEqualTo(List.of());
    }

    @Test
    void findAllDistinctMeterIds_successfulResponse_returnsMeterIdList() throws Exception {
        var smartMeter = new SmartMeterDTO();
        smartMeter.setId("meter1");

        // Arrange
        when(smartMeterApi.listAll()).thenReturn(List.of(smartMeter));

        // Act
        List<String> idList = smartMeterApiServiceImpl.findAllDistinctMeterIds();

        // Assert
        assertThat(idList).isNotEmpty();
        assertThat(idList).containsExactly("meter1");
    }

    @Test
    void findAllDistinctMeterIds_apiException_throwsResourceNotFoundException() throws Exception {
        // Arrange
        when(smartMeterApi.listAll()).thenThrow(new ApiException("API error"));

        // Act & Assert
        Exception exception = assertThrows(ResourceNotFoundException.class, () -> {
            smartMeterApiServiceImpl.findAllDistinctMeterIds();
        });

        assertThat(exception.getMessage()).isEqualTo("API error");
    }
}