package cz.muni.fi.pa165.epsilon.statistics.service;

import cz.muni.fi.pa165.epsilon.smart_meter_management.client.model.MeasureDTO;
import cz.muni.fi.pa165.epsilon.statistics.data.model.Statistic;
import cz.muni.fi.pa165.epsilon.statistics.data.repository.StatisticRepository;
import cz.muni.fi.pa165.epsilon.statistics.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.epsilon.statistics.exceptions.ValidationException;
import cz.muni.fi.pa165.epsilon.statistics.service.measureApi.MeasureApiServiceImpl;
import cz.muni.fi.pa165.epsilon.statistics.service.smartMeterApi.SmartMeterApiServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

/**
 * Tests for StatisticService.
 *
 * @author Lukas Kovarik
 */
@ExtendWith(MockitoExtension.class)
class StatisticServiceTest {
    @Mock
    private StatisticRepository statisticRepository;
    @Mock
    private MeasureApiServiceImpl measureApiServiceImpl;
    @Mock
    private SmartMeterApiServiceImpl smartMeterApiServiceImpl;

    @InjectMocks
    private StatisticServiceImpl statisticService;

    private Statistic statistic;
    private Statistic statistic2;

    @BeforeEach
    void setUp() {
        statistic = new Statistic("1", "1", LocalDateTime.now(), 10.0, 10.0, false);
        statistic2 = new Statistic("2", "2", LocalDateTime.now(), 20.0, 20.0, false);
    }

    @Test
    void findById_statisticFound_returnsStatistic() {
        var id = "1";

        // Arrange
        Mockito.when(statisticRepository.findBySmartMeterId(id)).thenReturn(List.of(statistic));

        // Act
        List<Statistic> foundStatistic = statisticService.findStatisticsByMeterId(id);

        // Assert
        assertThat(foundStatistic).isNotEmpty();
        assertThat(foundStatistic).isEqualTo(List.of(statistic));
    }

    @Test
    void findById_statisticNotFound_throwsResourceNotFoundException() {
        var id = "1";

        // Arrange
        Mockito.when(statisticRepository.findBySmartMeterId(id)).thenReturn(List.of());

        // Act & Assert
        Exception exception = assertThrows(ResourceNotFoundException.class, () -> {
            statisticService.findStatisticsByMeterId(id);
        });

        assertThat(exception.getMessage()).isEqualTo("No statistics found for meter ID: 1");
    }


    @Test
    void findById_nullId_throwsNullPointerException() {
        String id = null;

        // Arrange
        Mockito.when(statisticRepository.findBySmartMeterId(id)).thenThrow(NullPointerException.class);

        // Act
        assertThrows(NullPointerException.class, () -> {
            statisticService.findStatisticsByMeterId(id);
        });
    }

    @Test
    void listAll_emptyRepository_returnsEmptyList() {
        // Arrange
        Mockito.when(statisticRepository.findAll()).thenReturn(List.of());

        // Act
        List<Statistic> statistics = statisticService.listAll();

        // Assert
        assertThat(statistics).hasSize(0);
    }

    @Test
    void listAll_oneElement_returnsOneElementList() {
        // Arrange
        Mockito.when(statisticRepository.findAll()).thenReturn(List.of(statistic));

        // Act
        List<Statistic> statistics = statisticService.listAll();

        // Assert
        assertThat(statistics).hasSize(1);
        assertThat(statistics).containsExactly(statistic);
    }

    @Test
    void listAll_multipleElements_returnsMultipleElementsList() {
        // Arrange
        Mockito.when(statisticRepository.findAll()).thenReturn(List.of(statistic, statistic2));

        // Act
        List<Statistic> statistics = statisticService.listAll();

        // Assert
        assertThat(statistics).hasSize(2);
        assertThat(statistics).containsExactly(statistic, statistic2);
    }

    @Test
    void removeStatistic_statisticExists_statisticRemoved() {
        var id = "1";

        // Arrange
        Mockito.when(statisticRepository.findBySmartMeterId(id)).thenReturn(List.of(statistic));
        Mockito.doNothing().when(statisticRepository).deleteBySmartMeterId(id);

        // Act
        List<Statistic> removedStatistic = statisticService.removeStatisticsByMeterId(id);

        // Assert
        assertThat(removedStatistic).isNotEmpty();
        assertThat(removedStatistic).isEqualTo(List.of(statistic));
        Mockito.verify(statisticRepository).deleteBySmartMeterId(id);
    }

    @Test
    void removeStatistic_statisticNotExists_throwsResourceNotFoundException() {
        var id = "1";

        // Arrange
        Mockito.when(statisticRepository.findBySmartMeterId(id)).thenReturn(List.of());

        // Act & Assert
        Exception exception = assertThrows(ResourceNotFoundException.class, () -> {
            statisticService.removeStatisticsByMeterId(id);
        });

        //Assert
        assertThat(exception.getMessage()).isEqualTo("No statistics found for meter ID: 1");
        Mockito.verify(statisticRepository).findBySmartMeterId(id);
        Mockito.verify(statisticRepository, Mockito.never()).deleteBySmartMeterId(id);  // Verify that delete is never called
    }

    @Test
    void generatePeriodicMeterConsumptionStatistics_invalidDateRange_throwsValidationException() {
        LocalDateTime now = LocalDateTime.now();
        String meterId = "1";

        // Act & Assert
        ValidationException exception = assertThrows(ValidationException.class, () -> {
            statisticService.generatePeriodicMeterConsumptionStatistics(meterId, now, now.minusDays(1));
        });

        assertThat(exception.getMessage()).isEqualTo("Start time must be before end time.");
    }

    @Test
    void generatePeriodicMeterConsumptionStatistics_noStatistics_returnsStatisticWithZeros() {
        LocalDateTime now = LocalDateTime.now();
        String meterId = "1";

        // Arrange
        Mockito.when(statisticRepository.findByMeterIdAndTimestampBetween(meterId, now, now)).thenReturn(List.of());

        // Act
        Exception exception = assertThrows(ResourceNotFoundException.class, () -> {
            statisticService.generatePeriodicMeterConsumptionStatistics(meterId, now, now);
        });

        // Assert
        assertThat(exception.getMessage()).isEqualTo("No statistics found for meter ID between the specified dates: 1");
    }

    @Test
    void generatePeriodicMeterConsumptionStatistics_oneStatistic_returnsPopulatedStatistic() {
        LocalDateTime now = LocalDateTime.now();
        String meterId = "1";

        // Arrange
        Mockito.when(statisticRepository.findByMeterIdAndTimestampBetween(meterId, now, now)).thenReturn(List.of(statistic));

        // Act
        Statistic generatedPeriodicStatistics = statisticService.generatePeriodicMeterConsumptionStatistics(meterId, now, now);

        // Assert
        assertThat(generatedPeriodicStatistics.getTotalConsumption()).isEqualTo(10.0);
        assertThat(generatedPeriodicStatistics.getAverageConsumption()).isEqualTo(10.0);
    }

    @Test
    void generatePeriodicMeterConsumptionStatistics_multipleStatistics_returnsPopulatedStatistic() {
        LocalDateTime now = LocalDateTime.now();
        String meterId = "1";

        // Arrange
        Mockito.when(statisticRepository.findByMeterIdAndTimestampBetween(meterId, now, now)).thenReturn(List.of(statistic, statistic2));

        // Act
        Statistic generatedPeriodicStatistics = statisticService.generatePeriodicMeterConsumptionStatistics(meterId, now, now);

        // Assert
        assertThat(generatedPeriodicStatistics.getTotalConsumption()).isEqualTo(30.0);
        assertThat(generatedPeriodicStatistics.getAverageConsumption()).isEqualTo(15.0);
    }

    @Test
    void aggregateMeterConsumptionStatistics_noStatistics_returnsStatistic() {
        var id = UUID.randomUUID();

        // Arrange
        Mockito.when(statisticRepository.findBySmartMeterId(id.toString())).thenReturn(List.of());

        // Act
        Exception exception = assertThrows(ResourceNotFoundException.class, () -> {
            statisticService.aggregateMeterConsumptionStatistics(id.toString());
        });

        // Assert
        assertThat(exception.getMessage()).isEqualTo("No statistics found for meter ID: " + id);
    }

    @Test
    void aggregateMeterConsumptionStatistics_oneStatistic_returnsStatistic() {
        var id = UUID.randomUUID();

        // Arrange
        Mockito.when(statisticRepository.findBySmartMeterId(id.toString())).thenReturn(List.of(statistic));

        // Act
        Statistic generatedPeriodicStatistics = statisticService.aggregateMeterConsumptionStatistics(id.toString());

        // Assert
        assertThat(generatedPeriodicStatistics.getTotalConsumption()).isEqualTo(statistic.getTotalConsumption());
        assertThat(generatedPeriodicStatistics.getAverageConsumption()).isEqualTo(statistic.getAverageConsumption());
    }

    @Test
    void aggregateMeterConsumptionStatistics_moreStatistics_returnsStatistic() {
        var id = UUID.randomUUID();

        // Arrange
        Mockito.when(statisticRepository.findBySmartMeterId(id.toString())).thenReturn(List.of(statistic, statistic2));

        // Act
        Statistic generatedPeriodicStatistics = statisticService.aggregateMeterConsumptionStatistics(id.toString());

        // Assert
        assertThat(generatedPeriodicStatistics.getTotalConsumption()).isEqualTo(statistic2.getTotalConsumption() + statistic.getTotalConsumption());
        assertThat(generatedPeriodicStatistics.getAverageConsumption()).isEqualTo((statistic2.getTotalConsumption() + statistic.getTotalConsumption()) / 2);
    }

    @Test
    void takeStatisticsSnapshot_noMeasures_returnsStatistic() {
        var id = UUID.randomUUID();

        // Arrange
        Mockito.when(measureApiServiceImpl.listAllForSmartMeterId(id.toString())).thenReturn(List.of());

        // Act
        Exception exception = assertThrows(ResourceNotFoundException.class, () -> {
            statisticService.takeStatisticsSnapshot(id.toString());
        });

        assertThat(exception.getMessage()).isEqualTo("No measures found for meter ID: " + id);
        var inv = Mockito.mockingDetails(statisticRepository).getInvocations();
        assertThat(inv.size()).isEqualTo(0);
    }

    @Test
    void takeStatisticsSnapshot_oneMeasure_returnsStatistic() {
        var id = UUID.randomUUID();
        MeasureDTO measure1 = new MeasureDTO();
        measure1.setId(UUID.randomUUID().toString());
        measure1.setSmartMeterId(id.toString());
        measure1.setTimestamp(OffsetDateTime.now());
        measure1.setMeasuredValue(100.00);


        // Arrange
        Mockito.when(measureApiServiceImpl.listAllForSmartMeterId(id.toString())).thenReturn(List.of(measure1));

        // Act
        statisticService.takeStatisticsSnapshot(id.toString());

        // Assert
        var inv = Mockito.mockingDetails(statisticRepository).getInvocations();
        assertThat(inv.size()).isEqualTo(1);
    }

    @Test
    void takeStatisticsSnapshot_multipleMeasures_returnsStatistic() {
        var id = UUID.randomUUID();
        MeasureDTO measure1 = new MeasureDTO();
        measure1.setId(UUID.randomUUID().toString());
        measure1.setSmartMeterId(id.toString());
        measure1.setTimestamp(OffsetDateTime.now());
        measure1.setMeasuredValue(100.00);
        MeasureDTO measure2 = new MeasureDTO();
        measure2.setId(UUID.randomUUID().toString());
        measure2.setSmartMeterId(id.toString());
        measure2.setTimestamp(OffsetDateTime.now());
        measure2.setMeasuredValue(200.00);

        // Arrange
        Mockito.when(measureApiServiceImpl.listAllForSmartMeterId(id.toString())).thenReturn(List.of(measure1, measure2));
        ;

        // Act
        statisticService.takeStatisticsSnapshot(id.toString());

        // Assert
        var inv = Mockito.mockingDetails(statisticRepository).getInvocations();
        assertThat(inv.size()).isEqualTo(1);
    }

    @Test
    void takePeriodicSnapshots_noSmartMeters_returnsStatistic() {
        // Arrange
        Mockito.when(smartMeterApiServiceImpl.findAllDistinctMeterIds()).thenReturn(List.of());

        // Act
        statisticService.takePeriodicSnapshots();

        // Assert
        Mockito.verifyNoInteractions(statisticRepository);
    }

    @Test
    void takePeriodicSnapshots_oneSmartMeter_returnsStatistic() {
        MeasureDTO measure1 = new MeasureDTO();
        measure1.setId(UUID.randomUUID().toString());
        measure1.setSmartMeterId("smartMeterId1");
        measure1.setTimestamp(OffsetDateTime.now());
        measure1.setMeasuredValue(100.00);
        // Arrange
        Mockito.when(smartMeterApiServiceImpl.findAllDistinctMeterIds()).thenReturn(List.of("smartMeterId1"));
        Mockito.when(measureApiServiceImpl.listAllForSmartMeterId("smartMeterId1")).thenReturn(List.of(measure1));

        // Act
        statisticService.takePeriodicSnapshots();

        // Assert
        var inv = Mockito.mockingDetails(statisticRepository).getInvocations();
        assertThat(inv.size()).isEqualTo(1);
    }

    @Test
    void takePeriodicSnapshots_multipleSmartMeters_returnsStatistic() {
        MeasureDTO measure1 = new MeasureDTO();
        measure1.setId(UUID.randomUUID().toString());
        measure1.setSmartMeterId("smartMeterId1");
        measure1.setTimestamp(OffsetDateTime.now());
        measure1.setMeasuredValue(100.00);
        MeasureDTO measure2 = new MeasureDTO();
        measure2.setId(UUID.randomUUID().toString());
        measure2.setSmartMeterId("smartMeterId2");
        measure2.setTimestamp(OffsetDateTime.now());
        measure2.setMeasuredValue(200.00);
        // Arrange
        Mockito.when(smartMeterApiServiceImpl.findAllDistinctMeterIds()).thenReturn(List.of("smartMeterId1", "smartMeterId2"));
        Mockito.when(measureApiServiceImpl.listAllForSmartMeterId("smartMeterId1")).thenReturn(List.of(measure1));
        Mockito.when(measureApiServiceImpl.listAllForSmartMeterId("smartMeterId2")).thenReturn(List.of(measure2));

        // Act
        statisticService.takePeriodicSnapshots();

        var inv = Mockito.mockingDetails(statisticRepository).getInvocations();
        assertThat(inv.size()).isEqualTo(2);
    }

    @Test
    void findByMeterIdAndTimestamp_statisticsExists_returnsStatistics() {
        var statistic = new Statistic();
        Mockito.when(statisticRepository.findByMeterIdAndTimestampBetween(any(), any(), any())).thenReturn(List.of(statistic));

        var id = "1";
        var time = LocalDateTime.now();
        var output = statisticService.findByMeterIdAndTimestamp(id, time, time);

        Mockito.verify(statisticRepository).findByMeterIdAndTimestampBetween(id, time, time);
        assertThat(output.size()).isEqualTo(1);
        assertThat(output).containsExactly(statistic);
    }

    @Test
    void findByMeterIdAndTimestamp_statisticsNotExists_throwsResourceNotFoundException() {
        Mockito.when(statisticRepository.findByMeterIdAndTimestampBetween(any(), any(), any())).thenReturn(List.of());

        var id = "1";
        var time = LocalDateTime.now();
        assertThrows(ResourceNotFoundException.class, () -> statisticService.findByMeterIdAndTimestamp(id, time, time));
        Mockito.verify(statisticRepository).findByMeterIdAndTimestampBetween(id, time, time);
    }

    @Test
    void removeStatisticsByMeterIdAndTimestamp_statisticsExists_callsDeleteAndReturnsStatistics() {
        var statistic = new Statistic();
        Mockito.when(statisticRepository.findByMeterIdAndTimestampBetween(any(), any(), any())).thenReturn(List.of(statistic));
        Mockito.doNothing().when(statisticRepository).deleteAllBySmartMeterIdAndTimestampBetween(any(), any(), any());

        var id = "1";
        var time = LocalDateTime.now();
        var output = statisticService.removeStatisticsByMeterIdAndTimestamp(id, time, time);

        Mockito.verify(statisticRepository).findByMeterIdAndTimestampBetween(id, time, time);
        Mockito.verify(statisticRepository).deleteAllBySmartMeterIdAndTimestampBetween(id, time, time);
        assertThat(output.size()).isEqualTo(1);
        assertThat(output).containsExactly(statistic);
    }

    @Test
    void removeStatisticsByMeterIdAndTimestamp_statisticsNotExists_throwsResourceNotFoundException() {
        Mockito.when(statisticRepository.findByMeterIdAndTimestampBetween(any(), any(), any())).thenReturn(List.of());

        var id = "1";
        var time = LocalDateTime.now();
        assertThrows(ResourceNotFoundException.class, () -> statisticService.removeStatisticsByMeterIdAndTimestamp(id, time, time));
        Mockito.verify(statisticRepository).findByMeterIdAndTimestampBetween(id, time, time);
    }

}