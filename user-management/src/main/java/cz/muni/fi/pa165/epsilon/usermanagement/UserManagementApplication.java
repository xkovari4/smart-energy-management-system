package cz.muni.fi.pa165.epsilon.usermanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserManagementApplication {
    public static final String SECURITY_SCHEME_OAUTH2 = "MUNI";
    public static final String SECURITY_SCHEME_BEARER = "Bearer";

    public static void main(String[] args) {
        SpringApplication.run(UserManagementApplication.class, args);
    }

}
