package cz.muni.fi.pa165.epsilon.usermanagement.api;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * DTO representation of Address.
 *
 * @author Lukas Kovarik
 */
@Schema(
        title = "Address DTO",
        description = "DTO representation of Address"
)
@Setter
@Getter
@ToString
public class AddressDTO {
    @NotBlank
    @Schema(description = "Street", example = "Česká")
    private String street;

    @NotBlank
    @Schema(description = "House number", example = "17/370")
    private String houseNumber;


    @NotBlank
    @Schema(description = "City", example = "Brno")
    private String city;

    @NotBlank
    @Schema(description = "Zip code", example = "602 00")
    private String zipCode;
}
