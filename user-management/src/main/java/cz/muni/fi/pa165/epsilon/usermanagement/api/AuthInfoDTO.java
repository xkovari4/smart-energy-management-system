package cz.muni.fi.pa165.epsilon.usermanagement.api;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * DTO representation of User's auth info.
 *
 * @author Lukas Kovarik
 */
@Schema(
        title = "User Auth DTO",
        description = "DTO representation of User's auth info"
)
@Getter
@Setter
@ToString
public class AuthInfoDTO {
    @NotBlank
    @Schema(description = "User ID", example = "550e8400-e29b-41d4-a716-446655440000")
    private String userId;

    @NotBlank
    @Schema(description = "JWT token", example = "<...jwt-token...>")
    private String token;
}
