package cz.muni.fi.pa165.epsilon.usermanagement.api;

import cz.muni.fi.pa165.epsilon.usermanagement.data.model.UserRole;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

/**
 * DTO representation of User.
 *
 * @author Lukas Kovarik
 */
@Schema(
        title = "User DTO",
        description = "DTO representation of User"
)
@Getter
@Setter
@ToString
public class UserDTO {
    @NotBlank
    @Schema(description = "User ID", example = "550e8400-e29b-41d4-a716-446655440000")
    private String userId;

    @NotBlank
    @Schema(description = "Email", example = "johndoe@gmail.com")
    private String email;

    @NotBlank
    @Schema(description = "Given name", example = "John")
    private String givenName;

    @NotBlank
    @Schema(description = "Family name", example = "Doe")
    private String familyName;

    @NotNull
    @Schema(description = "User role", example = "OWNER")
    private UserRole userRole;

    @NonNull
    @Schema(description = "List of user's addresses", example = "[{\"street\": \"Česká\", \"houseNumber\":\"17/370\", \"city\":\"Brno\", \"zipCode\":\"602 00\"}]")
    private List<AddressDTO> addresses = new ArrayList<>();
}
