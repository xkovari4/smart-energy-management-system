package cz.muni.fi.pa165.epsilon.usermanagement.config;

import cz.muni.fi.pa165.epsilon.usermanagement.UserManagementApplication;
import io.swagger.v3.oas.models.security.OAuthFlow;
import io.swagger.v3.oas.models.security.OAuthFlows;
import io.swagger.v3.oas.models.security.Scopes;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springdoc.core.customizers.OpenApiCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class UserManagementSecurityConfig {
    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests(x -> x
                        .requestMatchers(HttpMethod.GET, "/user-management/users").hasAuthority("SCOPE_test_read")
                        .requestMatchers(HttpMethod.PUT, "/user-management/users").hasAuthority("SCOPE_test_write")
                        // .requestMatchers(HttpMethod.POST, "/user-management/users").hasAuthority("SCOPE_test_write") - allow to register without token
                        .requestMatchers(HttpMethod.GET, "/user-management/users/{userId}").hasAuthority("SCOPE_test_read")
                        .requestMatchers(HttpMethod.DELETE, "/user-management/users/{userId}").hasAuthority("SCOPE_test_1")
                        .requestMatchers(HttpMethod.GET, "/user-management/authInfo").hasAuthority("SCOPE_test_read")
                        .anyRequest().permitAll()
                )
                .oauth2ResourceServer(oauth2 -> oauth2.opaqueToken(Customizer.withDefaults()))
                .csrf().disable();
        return http.build();
    }

    @Bean
    public OpenApiCustomizer openAPICustomizer() {
        return openApi ->
                openApi.getComponents()
                        .addSecuritySchemes(UserManagementApplication.SECURITY_SCHEME_OAUTH2,
                                new SecurityScheme()
                                        .type(SecurityScheme.Type.OAUTH2)
                                        .description("Get access token with OAuth 2 Authorization Code Grant")
                                        .flows(new OAuthFlows()
                                                .authorizationCode(new OAuthFlow()
                                                        .authorizationUrl("https://oidc.muni.cz/oidc/authorize")
                                                        .tokenUrl("https://oidc.muni.cz/oidc/token")
                                                        .scopes(new Scopes()
                                                                .addString("test_read", "Listing")
                                                                .addString("test_write", "Creating and updating")
                                                                .addString("test_1", "Deleting")
                                                                .addString("test_2", "Turning on and off")
                                                        )
                                                )
                                        )
                        )
                        .addSecuritySchemes(UserManagementApplication.SECURITY_SCHEME_BEARER,
                                new SecurityScheme()
                                        .type(SecurityScheme.Type.HTTP)
                                        .scheme("bearer")
                                        .description("Provide a valid access token")
                        );
    }
}
