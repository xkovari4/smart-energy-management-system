package cz.muni.fi.pa165.epsilon.usermanagement.data.model;

/**
 * User role.
 *
 * @author Lukas Kovarik
 */
public enum UserRole {
    OWNER,
    COMPANY
}
