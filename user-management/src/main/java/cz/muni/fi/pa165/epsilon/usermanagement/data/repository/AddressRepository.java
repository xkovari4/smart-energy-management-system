package cz.muni.fi.pa165.epsilon.usermanagement.data.repository;

import cz.muni.fi.pa165.epsilon.usermanagement.data.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Address JPA repository.
 *
 * @author Lukas Kovarik
 */
public interface AddressRepository extends JpaRepository<Address, String> {
}
