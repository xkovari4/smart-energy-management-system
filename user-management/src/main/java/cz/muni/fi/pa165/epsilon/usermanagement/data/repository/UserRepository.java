package cz.muni.fi.pa165.epsilon.usermanagement.data.repository;

import cz.muni.fi.pa165.epsilon.usermanagement.data.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * User JPA repository.
 *
 * @author Lukas Kovarik
 */
@Repository
public interface UserRepository extends JpaRepository<User, String> {
    @Query("SELECT u FROM User u LEFT JOIN FETCH u.addresses a WHERE u.userId = :user_id")
    Optional<User> findById(@Param("user_id") String userId);

    @Query("SELECT u FROM User u LEFT JOIN FETCH u.addresses a WHERE u.ssoId = :sso_id")
    Optional<User> findBySsoId(@Param("sso_id") String ssoId);
}
