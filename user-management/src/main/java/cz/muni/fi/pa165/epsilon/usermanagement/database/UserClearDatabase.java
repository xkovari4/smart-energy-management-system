package cz.muni.fi.pa165.epsilon.usermanagement.database;

import cz.muni.fi.pa165.epsilon.usermanagement.data.repository.UserRepository;
import cz.muni.fi.pa165.epsilon.usermanagement.data.repository.AddressRepository;
import jakarta.annotation.PostConstruct;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Transactional
@AllArgsConstructor
@Profile("database-clear")
public class UserClearDatabase {

    private final UserRepository userRepository;
    private final AddressRepository addressRepository;

    @PostConstruct
    public void init() {
        System.out.println("Initializing Database Clearer...");
        clearDatabase();
    }

    private void clearDatabase() {
        addressRepository.deleteAll();
        userRepository.deleteAll();
        System.out.println("Database cleared");
    }
}

