package cz.muni.fi.pa165.epsilon.usermanagement.database;

import com.github.javafaker.Faker;
import cz.muni.fi.pa165.epsilon.usermanagement.data.model.Address;
import cz.muni.fi.pa165.epsilon.usermanagement.data.model.User;
import cz.muni.fi.pa165.epsilon.usermanagement.data.model.UserRole;
import cz.muni.fi.pa165.epsilon.usermanagement.data.repository.UserRepository;
import cz.muni.fi.pa165.epsilon.usermanagement.data.repository.AddressRepository;
import jakarta.annotation.PostConstruct;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@AllArgsConstructor
@Profile("database-start")
public class UserSeedDatabase {

    private static final int NUMBER_OF_USERS = 100;
    private static final int MAX_ADDRESS = 3;
    private final UserRepository userRepository;
    private final AddressRepository addressRepository;
    private final Faker faker = new Faker();
    private final EntityManager entityManager; // Inject EntityManager

    @PostConstruct
    public void init() {
        System.out.println("Initializing Database Seeder...");
        if (userRepository.count() == 0) {
            seedUsersAndAddresses();
        } else {
            System.out.println("Statistics database already seeded");
        }
    }

    private void seedUsersAndAddresses() {
        for (int i = 0; i < NUMBER_OF_USERS; i++) {
            User user = new User();
            user.setEmail(faker.internet().emailAddress());
            user.setGivenName(faker.name().firstName());
            user.setFamilyName(faker.name().lastName());
            user.setUserRole(faker.options().option(UserRole.class));

            int numberOfAddresses = faker.number().numberBetween(1, MAX_ADDRESS);
            var addresses = new ArrayList<Address>();
            for (int j = 0; j < numberOfAddresses; j++) {
                Address address = new Address();
                address.setStreet(faker.address().streetName());
                address.setHouseNumber(faker.address().buildingNumber());
                address.setCity(faker.address().city());
                address.setZipCode(faker.address().zipCode());
                address.setUser(user);
                addresses.add(address);
            }
            user.setAddresses(addresses);
            userRepository.save(user);
        }
        System.out.println("Database seeded");
    }
}


