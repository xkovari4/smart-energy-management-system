package cz.muni.fi.pa165.epsilon.usermanagement.exceptions;

/**
 * Runtime exception representing non-existing resource.
 *
 * @author Lukas Kovarik
 */
public class ResourceNotFoundException extends RuntimeException {
    public ResourceNotFoundException(String message) {
        super(message);
    }
}

