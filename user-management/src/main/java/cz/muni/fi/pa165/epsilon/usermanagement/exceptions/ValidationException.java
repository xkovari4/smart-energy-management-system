package cz.muni.fi.pa165.epsilon.usermanagement.exceptions;

/**
 * Validation exception.
 *
 * @author Lukas Kovarik
 */
public class ValidationException extends RuntimeException {
    public ValidationException(String message) {
        super(message);
    }
}
