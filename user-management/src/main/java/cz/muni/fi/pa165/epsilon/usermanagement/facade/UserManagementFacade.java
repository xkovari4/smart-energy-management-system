package cz.muni.fi.pa165.epsilon.usermanagement.facade;

import cz.muni.fi.pa165.epsilon.usermanagement.api.UserDTO;
import cz.muni.fi.pa165.epsilon.usermanagement.mappers.UserMapper;
import cz.muni.fi.pa165.epsilon.usermanagement.rest.RegisterUserRequest;
import cz.muni.fi.pa165.epsilon.usermanagement.rest.UpdateUserRequest;
import cz.muni.fi.pa165.epsilon.usermanagement.service.UserManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * User management facade.
 */
@Service
@Transactional
public class UserManagementFacade {
    private final UserManagementService userManagementService;
    private final UserMapper userMapper;

    @Autowired
    public UserManagementFacade(UserManagementService userManagementService, UserMapper userMapper) {
        this.userManagementService = userManagementService;
        this.userMapper = userMapper;
    }

    public UserDTO registerUser(RegisterUserRequest registerUserRequest) {
        var user = userMapper.mapToUser(registerUserRequest);
        return userMapper.mapToDto(userManagementService.registerUser(user));
    }

    public UserDTO updateUser(UpdateUserRequest updateUserRequest) {
        var user = userMapper.mapToUserUpdate(updateUserRequest);
        return userMapper.mapToDto(userManagementService.updateUser(user));
    }

    public UserDTO removeUser(String userId) {
        return userMapper.mapToDto(userManagementService.removeUser(userId));
    }

    public UserDTO findOrRegisterUser(String aud) {
        return userMapper.mapToDto(userManagementService.findOrRegisterUser(aud));
    }

    @Transactional(readOnly = true)
    public UserDTO findById(String userId) {
        return userMapper.mapToDto(userManagementService.findById(userId));
    }

    @Transactional(readOnly = true)
    public List<UserDTO> listAll() {
        return userMapper.mapToList(userManagementService.listAll());
    }
}
