package cz.muni.fi.pa165.epsilon.usermanagement.mappers;

import cz.muni.fi.pa165.epsilon.usermanagement.api.AddressDTO;
import cz.muni.fi.pa165.epsilon.usermanagement.data.model.Address;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * Mapper for {@link Address}.
 *
 * @author Lukas Kovarik
 */
@Mapper(componentModel = "spring")
public interface AddressMapper {
    AddressDTO mapToDto(Address address);

    Address mapToAddress(AddressDTO addressDTO);

    List<Address> mapToList(List<AddressDTO> addresses);
}
