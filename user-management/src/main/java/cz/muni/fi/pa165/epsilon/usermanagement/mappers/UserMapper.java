package cz.muni.fi.pa165.epsilon.usermanagement.mappers;

import cz.muni.fi.pa165.epsilon.usermanagement.api.UserDTO;
import cz.muni.fi.pa165.epsilon.usermanagement.data.model.User;
import cz.muni.fi.pa165.epsilon.usermanagement.rest.RegisterUserRequest;
import cz.muni.fi.pa165.epsilon.usermanagement.rest.UpdateUserRequest;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * Mapper for {@link User}.
 *
 * @author Lukas Kovarik
 */
@Mapper(componentModel = "spring")
public interface UserMapper {
    UserDTO mapToDto(User user);

    User mapToUser(RegisterUserRequest userDTO);

    User mapToUserUpdate(UpdateUserRequest userDTO);

    List<UserDTO> mapToList(List<User> users);
}
