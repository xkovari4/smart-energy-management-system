package cz.muni.fi.pa165.epsilon.usermanagement.rest;

import cz.muni.fi.pa165.epsilon.usermanagement.api.AddressDTO;
import cz.muni.fi.pa165.epsilon.usermanagement.data.model.UserRole;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

/**
 * Register user request.
 *
 * @author Lukas Kovarik
 */
@Schema(
        title = "Register user request",
        description = "Represents request for registering users",
        discriminatorProperty = "requestType"
)
@Getter
@Setter
@ToString
public class RegisterUserRequest {
    @Email
    @NotBlank
    @Schema(description = "Email", example = "johndoe@gmail.com")
    private String email;

    @NotBlank
    @Schema(description = "Given name", example = "John")
    private String givenName;

    @NotBlank
    @Schema(description = "Family name", example = "Doe")
    private String familyName;

    @NotNull
    @Schema(description = "User name", example = "OWNER")
    private UserRole userRole;

    @NotNull
    @Schema(description = "User's addresses", example = "[{\"street\": \"Česká\", \"houseNumber\":\"17/370\", \"city\":\"Brno\", \"zipCode\":\"602 00\"}]")
    private List<@Valid AddressDTO> addresses = new ArrayList<>();
}
