package cz.muni.fi.pa165.epsilon.usermanagement.rest;

import cz.muni.fi.pa165.epsilon.usermanagement.UserManagementApplication;
import cz.muni.fi.pa165.epsilon.usermanagement.api.AuthInfoDTO;
import cz.muni.fi.pa165.epsilon.usermanagement.api.UserDTO;
import cz.muni.fi.pa165.epsilon.usermanagement.exceptions.ValidationException;
import cz.muni.fi.pa165.epsilon.usermanagement.facade.UserManagementFacade;
import cz.muni.fi.pa165.epsilon.usermanagement.rest.exceptionhandling.ApiError;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Base64;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * REST controller for user management.
 */
@RestController
@OpenAPIDefinition(
        info = @Info(
                title = "Smart Energy Management OpenAPI Service",
                description = "User Management Service",
                version = "1.0.0"
        ),
        servers = {@Server(url = "http://user-management:8080", description = "Docker network server"),
                @Server(url = "http://localhost:8090", description = "Localhost network server")}
)
@Tag(name = "UserManagementService", description = "API service for user management")
@RequestMapping(path = "/user-management", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserManagementRestController {
    private final UserManagementFacade userManagementFacade;

    @Autowired
    public UserManagementRestController(UserManagementFacade userManagementFacade) {
        this.userManagementFacade = userManagementFacade;
    }

    @Operation(
            summary = "Get user",
            description = "Returns user by given ID",
            security = {@SecurityRequirement(name = UserManagementApplication.SECURITY_SCHEME_OAUTH2, scopes = {"test_read"}),
                    @SecurityRequirement(name = UserManagementApplication.SECURITY_SCHEME_BEARER, scopes = {"test_read"})},
            responses = {
                    @ApiResponse(responseCode = "200", description = "User found"),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_read", content = @Content()),
                    @ApiResponse(responseCode = "404", description = "User not found",
                            content = @Content(schema = @Schema(implementation = ApiError.class)))
            }
    )
    @GetMapping("/users/{userId}")
    @CrossOrigin(origins = "*")
    @Transactional(readOnly = true)
    public ResponseEntity<UserDTO> findById(@PathVariable("userId") String userId) {
        return ResponseEntity.ok(userManagementFacade.findById(userId));
    }

    @Operation(
            summary = "Get current token [DEBUG]",
            description = "Returns token of currently logged user",
            security = {@SecurityRequirement(name = UserManagementApplication.SECURITY_SCHEME_OAUTH2, scopes = {"test_read"}),
                    @SecurityRequirement(name = UserManagementApplication.SECURITY_SCHEME_BEARER, scopes = {"test_read"})}
    )
    @GetMapping("/authInfo")
    @CrossOrigin(origins = "*")
    @Transactional
    public ResponseEntity<AuthInfoDTO> getToken(HttpServletRequest request) {
        var token = request.getHeader("Authorization").split(" ")[1];

        Base64.Decoder decoder = Base64.getUrlDecoder();

        String[] chunks = token.split("\\.");
        String payload = new String(decoder.decode(chunks[1]));

        String regex = "\"aud\":\"([^\"]*)\"";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(payload);

        String aud = matcher.find() ? matcher.group(1) : "";

        UserDTO user = userManagementFacade.findOrRegisterUser(aud);

        AuthInfoDTO authInfo = new AuthInfoDTO();
        authInfo.setToken(token);
        authInfo.setUserId(user.getUserId());

        return ResponseEntity.ok(authInfo);
    }

    @Operation(
            summary = "Get all users",
            description = "Lists all existing users",
            security = {@SecurityRequirement(name = UserManagementApplication.SECURITY_SCHEME_OAUTH2, scopes = {"test_read"}),
                    @SecurityRequirement(name = UserManagementApplication.SECURITY_SCHEME_BEARER, scopes = {"test_read"})}
    )
    @GetMapping("/users")
    @CrossOrigin(origins = "*")
    @Transactional(readOnly = true)
    public ResponseEntity<List<UserDTO>> listAll() {
        return ResponseEntity.ok(userManagementFacade.listAll());
    }

    @Operation(
            summary = "Register user",
            description = "Registers new user",
            responses = {
                    @ApiResponse(responseCode = "201", description = "User registered"),
                    @ApiResponse(responseCode = "400", description = "User validation failed",
                            content = @Content(schema = @Schema(implementation = ApiError.class)))
            }
    )
    @PostMapping("/users")
    @CrossOrigin(origins = "*")
    @ResponseStatus(HttpStatus.CREATED)
    @Transactional
    public ResponseEntity<UserDTO> registerUser(@Valid @RequestBody RegisterUserRequest request, BindingResult bindingResult) {
        checkFieldError(bindingResult);

        UserDTO newUser = userManagementFacade.registerUser(request);
        return new ResponseEntity<>(newUser, HttpStatus.CREATED);
    }

    @Operation(
            summary = "Update user",
            description = "Updates user with given data",
            security = {@SecurityRequirement(name = UserManagementApplication.SECURITY_SCHEME_OAUTH2, scopes = {"test_write"}),
                    @SecurityRequirement(name = UserManagementApplication.SECURITY_SCHEME_BEARER, scopes = {"test_write"})},
            responses = {
                    @ApiResponse(responseCode = "200", description = "User updated"),
                    @ApiResponse(responseCode = "400", description = "User validation failed",
                            content = @Content(schema = @Schema(implementation = ApiError.class))),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_write", content = @Content()),
                    @ApiResponse(responseCode = "404", description = "User not found",
                            content = @Content(schema = @Schema(implementation = ApiError.class)))
            }
    )
    @PutMapping("/users")
    @CrossOrigin(origins = "*")
    @Transactional
    public ResponseEntity<UserDTO> updateUser(@Valid @RequestBody UpdateUserRequest request, BindingResult bindingResult) {
        checkFieldError(bindingResult);

        UserDTO updatedUser = userManagementFacade.updateUser(request);
        return ResponseEntity.ok(updatedUser);
    }

    @Operation(
            summary = "Remove user",
            description = "Removes user with given ID",
            security = {@SecurityRequirement(name = UserManagementApplication.SECURITY_SCHEME_OAUTH2, scopes = {"test_1"}),
                    @SecurityRequirement(name = UserManagementApplication.SECURITY_SCHEME_BEARER, scopes = {"test_1"})},
            responses = {
                    @ApiResponse(responseCode = "204", description = "User removed"),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_1", content = @Content()),
                    @ApiResponse(responseCode = "404", description = "User not found",
                            content = @Content(schema = @Schema(implementation = ApiError.class)))
            }
    )
    @DeleteMapping("/users/{userId}")
    @CrossOrigin(origins = "*")
    @Transactional
    public ResponseEntity<UserDTO> removeUser(@PathVariable("userId") String userId) {
        UserDTO removedUser = userManagementFacade.removeUser(userId);
        return new ResponseEntity<>(removedUser, HttpStatus.NO_CONTENT);
    }

    private void checkFieldError(BindingResult bindingResult) throws ValidationException {
        if (bindingResult.hasErrors()) {
            for (FieldError fe : bindingResult.getFieldErrors()) {
                throw new ValidationException(String.format("FieldError: %s; %s", fe.getField(), fe.getDefaultMessage()));
            }
        }
    }
}
