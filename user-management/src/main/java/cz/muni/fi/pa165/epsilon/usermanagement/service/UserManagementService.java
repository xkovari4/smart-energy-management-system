package cz.muni.fi.pa165.epsilon.usermanagement.service;

import cz.muni.fi.pa165.epsilon.usermanagement.data.model.User;
import cz.muni.fi.pa165.epsilon.usermanagement.data.model.UserRole;
import cz.muni.fi.pa165.epsilon.usermanagement.data.repository.AddressRepository;
import cz.muni.fi.pa165.epsilon.usermanagement.data.repository.UserRepository;
import cz.muni.fi.pa165.epsilon.usermanagement.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service for user management.
 */
@Service
public class UserManagementService {
    private final UserRepository userRepository;
    private final AddressRepository addressRepository;

    @Autowired
    public UserManagementService(UserRepository userRepository, AddressRepository addressRepository) {
        this.userRepository = userRepository;
        this.addressRepository = addressRepository;
    }

    @Transactional
    public User registerUser(User newUser) {
        newUser.getAddresses().forEach(address -> address.setUser(newUser));
        return userRepository.save(newUser);
    }

    @Transactional
    public User updateUser(User updatedUser) {
        User user = findById(updatedUser.getUserId());

        user.setEmail(updatedUser.getEmail());
        user.setGivenName(updatedUser.getGivenName());
        user.setFamilyName(updatedUser.getFamilyName());
        user.setUserRole(updatedUser.getUserRole());

        addressRepository.deleteAll(user.getAddresses());
        updatedUser.getAddresses().forEach(address -> address.setUser(user));
        user.setAddresses(updatedUser.getAddresses());

        return userRepository.save(user);
    }

    @Transactional
    public User removeUser(String userId) {
        User userToDelete = findById(userId);
        userRepository.deleteById(userId);
        return userToDelete;
    }

    @Transactional(readOnly = true)
    public User findById(String userId) {
        return userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("User with id: %s not found.", userId)));
    }

    @Transactional(readOnly = true)
    public User findBySsoId(String ssoId) {
        return userRepository.findBySsoId(ssoId)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("User with sso id: %s not found.", ssoId)));
    }

    @Transactional(readOnly = true)
    public List<User> listAll() {
        return userRepository.findAll();
    }

    @Transactional
    public User findOrRegisterUser(String aud) {
        try {
            return findBySsoId(aud);
        } catch (ResourceNotFoundException e) {
            var newUser = new User(aud + "@anonymousemail.com", "Anonymous", "Anonymous", UserRole.OWNER);
            newUser.setSsoId(aud);
            newUser.setAddresses(List.of());

            return userRepository.save(newUser);
        }
    }
}
