package cz.muni.fi.pa165.epsilon.usermanagement.api;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserDTOTests {

    @Test
    public void getUserId_WhenUserIdIsSet_ReturnsCorrectValue() {
        // Given
        String userId = "550e8400-e29b-41d4-a716-446655440000";
        String email = "johndoe@gmail.com";
        String givenName = "John";
        String familyName = "Doe";
        UserDTO userDTO = new UserDTO();

        // When
        userDTO.setUserId(userId);
        userDTO.setEmail(email);
        userDTO.setGivenName(givenName);
        userDTO.setFamilyName(familyName);

        // Then
        assertEquals(userId, userDTO.getUserId(), "UserId should match the set value.");
        assertEquals(email, userDTO.getEmail(), "Email should match the set value.");
        assertEquals(givenName, userDTO.getGivenName(), "GivenName should match the set value.");
        assertEquals(familyName, userDTO.getFamilyName(), "FamilyName should match the set value.");
    }

    @Test
    public void toString_WhenCalled_ReturnsExpectedString() {
        // Given
        UserDTO userDTO = prepareUserDTO(
                "550e8400-e29b-41d4-a716-446655440000",
                "johndoe@gmail.com",
                "John",
                "Doe"
        );

        // Expected string representation of UserDTO
        String expectedToString = "UserDTO(userId=550e8400-e29b-41d4-a716-446655440000, email=johndoe@gmail.com, givenName=John, familyName=Doe, userRole=null, addresses=[])";

        // When
        String actualToString = userDTO.toString();

        // Then
        assertEquals(expectedToString, actualToString, "toString method should return the expected string representation.");
    }

    // Utility method for creating a UserDTO with provided details
    private UserDTO prepareUserDTO(String userId, String email, String givenName, String familyName) {
        UserDTO userDTO = new UserDTO();
        userDTO.setUserId(userId);
        userDTO.setEmail(email);
        userDTO.setGivenName(givenName);
        userDTO.setFamilyName(familyName);
        return userDTO;
    }
}
