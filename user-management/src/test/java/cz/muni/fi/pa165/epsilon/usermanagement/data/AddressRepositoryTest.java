package cz.muni.fi.pa165.epsilon.usermanagement.data;

import cz.muni.fi.pa165.epsilon.usermanagement.data.model.Address;
import cz.muni.fi.pa165.epsilon.usermanagement.data.model.User;
import cz.muni.fi.pa165.epsilon.usermanagement.data.model.UserRole;
import cz.muni.fi.pa165.epsilon.usermanagement.data.repository.AddressRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hibernate.validator.internal.util.Contracts.assertTrue;

@DataJpaTest
public class AddressRepositoryTest {

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    private Address address1;
    private Address address2;

    @BeforeEach
    void setUp() {
        User user = new User();
        user.setEmail("test@example.com");
        user.setGivenName("John");
        user.setFamilyName("Doe");
        user.setUserRole(UserRole.OWNER);
        testEntityManager.persistAndFlush(user);

        address1 = new Address();
        address1.setCity("Test City 1");
        address1.setStreet("Test Street 1");
        address1.setHouseNumber("101");
        address1.setZipCode("12345");
        address1.setUser(user);

        address2 = new Address();
        address2.setCity("Test City 2");
        address2.setStreet("Test Street 2");
        address2.setHouseNumber("102");
        address2.setZipCode("67890");
        address2.setUser(user);

        testEntityManager.persistAndFlush(address1);
        testEntityManager.persistAndFlush(address2);
        testEntityManager.clear();
    }

    @Test
    void findById_WhenValidId_ThenAddressShouldBeFound() {
        Address found = addressRepository.findById(address1.getAddressId()).orElse(null);
        assertThat(found).isEqualTo(address1);
    }

    @Test
    void findById_WhenInvalidId_ThenAddressShouldNotBeFound() {
        var found = addressRepository.findById("999");
        assertThat(found).isNotPresent();
    }

    @Test
    void updateAddress_WhenValidAddress_ThenUpdated() {
        Optional<Address> found = addressRepository.findById(address1.getAddressId());
        assertThat(found).isPresent();

        found.ifPresent(addr -> {
            addr.setCity("Updated City");
            addressRepository.save(addr);
        });

        Optional<Address> updated = addressRepository.findById(address1.getAddressId());
        assertThat(updated).isPresent();
        assertThat(updated.get().getCity()).isEqualTo("Updated City");
    }

    @Test
    void deleteAddress_WhenValidAddress_ThenRemoved() {
        addressRepository.deleteById(address2.getAddressId());
        testEntityManager.flush();

        Optional<Address> deleted = addressRepository.findById(address2.getAddressId());
        assertThat(deleted).isNotPresent();
    }

    @Test
    void deleteById_WhenNonExistingAddress_ThenEnsureNoException() {
        try {
            addressRepository.deleteById("nonexistentid");
            testEntityManager.flush();  // To check for any database operations
        } catch (Exception e) {
            assertTrue(false, "Exception should not be thrown when deleting non-existing address");
        }
    }
}
