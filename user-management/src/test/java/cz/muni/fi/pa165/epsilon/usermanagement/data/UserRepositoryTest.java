package cz.muni.fi.pa165.epsilon.usermanagement.data;

import cz.muni.fi.pa165.epsilon.usermanagement.data.model.Address;
import cz.muni.fi.pa165.epsilon.usermanagement.data.model.User;
import cz.muni.fi.pa165.epsilon.usermanagement.data.model.UserRole;
import cz.muni.fi.pa165.epsilon.usermanagement.data.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.Arrays;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    private User user1;
    private Address address1;
    private Address address2;

    @BeforeEach
    void setUp() {
        user1 = new User();
        user1.setEmail("johndoe@example.com");
        user1.setGivenName("John");
        user1.setFamilyName("Doe");
        user1.setUserRole(UserRole.OWNER);

        address1 = new Address();
        address1.setCity("Test City 1");
        address1.setStreet("Test Street 1");
        address1.setHouseNumber("101");
        address1.setZipCode("12345");
        address1.setUser(user1);

        address2 = new Address();
        address2.setCity("Test City 2");
        address2.setStreet("Test Street 2");
        address2.setHouseNumber("102");
        address2.setZipCode("67890");
        address2.setUser(user1);

        user1.setAddresses(Arrays.asList(address1, address2));

        testEntityManager.persistAndFlush(user1);
    }

    @Test
    void findById_WhenValidUserId_ThenUserShouldBeFoundWithAddresses() {
        Optional<User> found = userRepository.findById(user1.getUserId());
        assertThat(found).isPresent();
        assertThat(found.get().getAddresses()).hasSize(2);
    }

    @Test
    void findById_WhenInvalidUserId_ThenNoUserShouldBeFound() {
        Optional<User> found = userRepository.findById("nonExistingUserId");
        assertThat(found).isNotPresent();
    }

    @Test
    void deleteById_WhenUserDeleted_ThenAddressesShouldAlsoBeRemoved() {
        userRepository.deleteById(user1.getUserId());
        testEntityManager.flush();

        assertThat(testEntityManager.find(User.class, user1.getUserId())).isNull();
        assertThat(testEntityManager.find(Address.class, address1.getAddressId())).isNull();
        assertThat(testEntityManager.find(Address.class, address2.getAddressId())).isNull();
    }
}
