package cz.muni.fi.pa165.epsilon.usermanagement.data;

import cz.muni.fi.pa165.epsilon.usermanagement.data.model.Address;
import cz.muni.fi.pa165.epsilon.usermanagement.data.model.User;
import cz.muni.fi.pa165.epsilon.usermanagement.data.model.UserRole;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserTests {

    @Test
    void createUser_WhenValidUserDetails_ThenFieldsAreCorrectlySet() {
        // Given
        String userId = "1";
        String email = "user@example.com";
        String givenName = "John";
        String familyName = "Doe";
        UserRole userRole = UserRole.OWNER;

        // When
        User user = new User(email, givenName, familyName, userRole);
        user.setUserId(userId);
        user.setAddresses(List.of(new Address()));

        // Then
        assertEquals(userId, user.getUserId(), "UserId should match the provided userId");
        assertEquals(email, user.getEmail(), "Email should match the provided email");
        assertEquals(givenName, user.getGivenName(), "GivenName should match the provided givenName");
        assertEquals(familyName, user.getFamilyName(), "FamilyName should match the provided familyName");
        assertEquals(userRole, user.getUserRole(), "UserRole should match the provided userRole");
        assertEquals(1, user.getAddresses().size(), "Addresses should match the provided addresses");
    }

    @Test
    void setUserDetails_WhenSettingNewDetails_ThenFieldsAreUpdated() {
        // Given
        User user = new User("user@example.com", "John", "Doe", UserRole.OWNER);
        user.setUserId("1");
        user.setAddresses(List.of(new Address()));

        // New details
        String newUserId = "2";
        String newEmail = "newuser@example.com";
        String newGivenName = "Jane";
        String newFamilyName = "Smith";
        UserRole newUserRole = UserRole.COMPANY;

        // When
        user.setUserId(newUserId);
        user.setEmail(newEmail);
        user.setGivenName(newGivenName);
        user.setFamilyName(newFamilyName);
        user.setUserRole(newUserRole);

        var addr1 = new Address();
        addr1.setAddressId("1");
        var addr2 = new Address();
        addr2.setAddressId("2");
        user.setAddresses(List.of(addr1, addr2));

        // Then
        assertEquals(newUserId, user.getUserId(), "UserId should be updated to the new userId");
        assertEquals(newEmail, user.getEmail(), "Email should be updated to the new email");
        assertEquals(newGivenName, user.getGivenName(), "GivenName should be updated to the new givenName");
        assertEquals(newFamilyName, user.getFamilyName(), "FamilyName should be updated to the new familyName");
        assertEquals(newUserRole, user.getUserRole(), "UserRole should be updated to the new userRole");
        assertEquals(2, user.getAddresses().size(), "Addresses count should be updated");
    }
}
