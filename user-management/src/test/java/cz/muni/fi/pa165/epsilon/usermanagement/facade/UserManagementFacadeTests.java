package cz.muni.fi.pa165.epsilon.usermanagement.facade;

import cz.muni.fi.pa165.epsilon.usermanagement.api.UserDTO;
import cz.muni.fi.pa165.epsilon.usermanagement.data.model.User;
import cz.muni.fi.pa165.epsilon.usermanagement.data.model.UserRole;
import cz.muni.fi.pa165.epsilon.usermanagement.mappers.AddressMapper;
import cz.muni.fi.pa165.epsilon.usermanagement.mappers.UserMapper;
import cz.muni.fi.pa165.epsilon.usermanagement.rest.RegisterUserRequest;
import cz.muni.fi.pa165.epsilon.usermanagement.rest.UpdateUserRequest;
import cz.muni.fi.pa165.epsilon.usermanagement.service.UserManagementService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UserManagementFacadeTests {

    @Mock
    private UserManagementService userManagementService;

    @Mock
    private UserMapper userMapper;

    @Mock
    private AddressMapper addressMapper;

    @InjectMocks
    private UserManagementFacade userManagementFacade;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void registerUser_WhenValidUserDetails_ThenSuccess() {
        // Given
        RegisterUserRequest registerUserRequest = new RegisterUserRequest();
        registerUserRequest.setEmail("johndoe@gmail.com");
        registerUserRequest.setGivenName("John");
        registerUserRequest.setFamilyName("Doe");
        registerUserRequest.setUserRole(UserRole.OWNER);
        registerUserRequest.setAddresses(Collections.emptyList());
        User user = new User("johndoe@gmail.com", "John", "Doe", UserRole.OWNER);
        user.setUserId("1");
        UserDTO userDTO = prepareUserDTO("1", "johndoe@gmail.com", "John", "Doe");

        // When
        when(userMapper.mapToUser(registerUserRequest)).thenReturn(user);
        when(userManagementService.registerUser(any(User.class))).thenReturn(user);
        when(userMapper.mapToDto(any(User.class))).thenReturn(userDTO);

        // Then
        UserDTO result = userManagementFacade.registerUser(registerUserRequest);
        assertEquals(userDTO, result);
        verify(userMapper).mapToUser(registerUserRequest);
        verify(userManagementService).registerUser(user);
        verify(userMapper).mapToDto(user);
    }

    @Test
    public void registerUser_WhenInvalidUserDetails_ThenExceptionThrown() {
        // Given
        RegisterUserRequest registerUserRequest = new RegisterUserRequest();
        registerUserRequest.setEmail("invalid email");
        registerUserRequest.setGivenName("");
        registerUserRequest.setFamilyName("");
        registerUserRequest.setUserRole(UserRole.OWNER);
        registerUserRequest.setAddresses(Collections.emptyList());

        // When
        when(userMapper.mapToUser(registerUserRequest)).thenThrow(new IllegalArgumentException("Invalid user details"));

        // Then
        assertThrows(IllegalArgumentException.class, () -> {
            userManagementFacade.registerUser(registerUserRequest);
        });
        verify(userMapper).mapToUser(registerUserRequest);
    }

    @Test
    public void updateUser_WhenValidUserId_ThenUserUpdated() {
        // Given
        UpdateUserRequest updateUserRequest = new UpdateUserRequest();
        updateUserRequest.setUserId("1");
        updateUserRequest.setEmail("updatedjohndoe@gmail.com");
        updateUserRequest.setGivenName("John");
        updateUserRequest.setFamilyName("UpdatedDoe");
        updateUserRequest.setUserRole(UserRole.OWNER);
        updateUserRequest.setAddresses(Collections.emptyList());
        UserDTO userDTO = prepareUserDTO("1", "updatedjohndoe@gmail.com", "John", "UpdatedDoe");
        User updatedUser = new User("updatedjohndoe@gmail.com", "John", "UpdatedDoe", UserRole.OWNER);
        updatedUser.setUserId("1");

        // When
        when(userMapper.mapToUserUpdate(updateUserRequest)).thenReturn(updatedUser);
        when(userManagementService.updateUser(any(User.class))).thenReturn(updatedUser);
        when(userMapper.mapToDto(any(User.class))).thenReturn(userDTO);

        // Then
        UserDTO result = userManagementFacade.updateUser(updateUserRequest);
        assertEquals(userDTO, result);
        verify(userMapper).mapToUserUpdate(updateUserRequest);
        verify(userManagementService).updateUser(updatedUser);
        verify(userMapper).mapToDto(updatedUser);
    }

    @Test
    public void updateUser_WhenInvalidUserId_ThenExceptionThrown() {
        // Given
        UpdateUserRequest updateUserRequest = new UpdateUserRequest();
        updateUserRequest.setUserId("invalid");
        updateUserRequest.setEmail("email");
        updateUserRequest.setGivenName("name");
        updateUserRequest.setFamilyName("name");
        updateUserRequest.setUserRole(UserRole.OWNER);
        updateUserRequest.setAddresses(Collections.emptyList());

        // When
        when(userMapper.mapToUserUpdate(updateUserRequest)).thenThrow(new IllegalArgumentException("Invalid user ID"));

        // Then
        assertThrows(IllegalArgumentException.class, () -> {
            userManagementFacade.updateUser(updateUserRequest);
        });
        verify(userMapper).mapToUserUpdate(updateUserRequest);
    }

    @Test
    public void removeUser_WhenValidUserId_ThenUserRemoved() {
        // Given
        User removedUser = new User("johndoe@gmail.com", "John", "Doe", UserRole.OWNER);
        removedUser.setUserId("1");
        UserDTO removedUserDTO = prepareUserDTO("1", "johndoe@gmail.com", "John", "Doe");

        // When
        when(userManagementService.removeUser("1")).thenReturn(removedUser);
        when(userMapper.mapToDto(any(User.class))).thenReturn(removedUserDTO);

        // Then
        UserDTO result = userManagementFacade.removeUser("1");
        assertEquals(removedUserDTO, result);
        verify(userManagementService).removeUser("1");
        verify(userMapper).mapToDto(removedUser);
    }

    @Test
    public void removeUser_WhenInvalidUserId_ThenExceptionThrown() {
        // Given
        when(userManagementService.removeUser("invalid")).thenThrow(new IllegalArgumentException("Invalid user ID"));

        // Then
        assertThrows(IllegalArgumentException.class, () -> {
            userManagementFacade.removeUser("invalid");
        });
        verify(userManagementService).removeUser("invalid");
    }

    @Test
    public void findById_WhenValidUserId_ThenUserFound() {
        // Given
        User foundUser = new User("johndoe@gmail.com", "John", "Doe", UserRole.OWNER);
        foundUser.setUserId("1");
        UserDTO foundUserDTO = prepareUserDTO("1", "johndoe@gmail.com", "John", "Doe");

        // When
        when(userManagementService.findById("1")).thenReturn(foundUser);
        when(userMapper.mapToDto(any(User.class))).thenReturn(foundUserDTO);

        // Then
        UserDTO result = userManagementFacade.findById("1");
        assertEquals(foundUserDTO, result);
        verify(userManagementService).findById("1");
        verify(userMapper).mapToDto(foundUser);
    }

    @Test
    public void findById_WhenInvalidUserId_ThenExceptionThrown() {
        // Given
        when(userManagementService.findById("invalid")).thenThrow(new IllegalArgumentException("User not found"));

        // Then
        assertThrows(IllegalArgumentException.class, () -> {
            userManagementFacade.findById("invalid");
        });
        verify(userManagementService).findById("invalid");
    }

    @Test
    public void listAll_WhenUsersExist_ThenAllUsersListed() {
        var user1 = new User("user1@example.com", "User", "One", UserRole.OWNER);
        user1.setUserId("1");
        var user2 = new User("user2@example.com", "User", "Two", UserRole.OWNER);
        user2.setUserId("2");

        // Given
        List<User> users = Arrays.asList(
                user1, user2
        );

        // When
        when(userManagementService.listAll()).thenReturn(users);
        when(userMapper.mapToList(anyList())).thenReturn(Arrays.asList(new UserDTO(), new UserDTO())); // Simplification for the purpose of the test

        // Then
        List<UserDTO> result = userManagementFacade.listAll();
        assertEquals(2, result.size());
        verify(userManagementService).listAll();
        verify(userMapper).mapToList(users);
    }

    @Test
    public void listAll_WhenNoUsersExist_ThenEmptyListReturned() {
        // Given
        when(userManagementService.listAll()).thenReturn(Collections.emptyList());
        when(userMapper.mapToList(anyList())).thenReturn(Collections.emptyList());

        // Then
        List<UserDTO> result = userManagementFacade.listAll();
        assertEquals(0, result.size());
        verify(userManagementService).listAll();
        verify(userMapper).mapToList(Collections.emptyList());
    }

    private UserDTO prepareUserDTO(String userId, String email, String givenName, String familyName) {
        UserDTO userDTO = new UserDTO();
        userDTO.setUserId(userId);
        userDTO.setEmail(email);
        userDTO.setGivenName(givenName);
        userDTO.setFamilyName(familyName);
        return userDTO;
    }
}
