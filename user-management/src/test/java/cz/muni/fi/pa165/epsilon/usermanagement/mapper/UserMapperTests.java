package cz.muni.fi.pa165.epsilon.usermanagement.mapper;

import cz.muni.fi.pa165.epsilon.usermanagement.api.UserDTO;
import cz.muni.fi.pa165.epsilon.usermanagement.data.model.Address;
import cz.muni.fi.pa165.epsilon.usermanagement.data.model.User;
import cz.muni.fi.pa165.epsilon.usermanagement.data.model.UserRole;
import cz.muni.fi.pa165.epsilon.usermanagement.mappers.UserMapper;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.util.Arrays;
import java.util.List;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class UserMapperTests {

    private final UserMapper mapper = Mappers.getMapper(UserMapper.class);

    @Test
    public void mapToDto_WhenValidUser_ThenCorrectDtoReturned() {
        // Given
        User user = new User("john.doe@example.com", "John", "Doe", UserRole.OWNER);
        user.setUserId("1");
        user.setAddresses(List.of(new Address()));

        // When
        UserDTO userDTO = mapper.mapToDto(user);

        // Then
        assertEquals(user.getUserId(), userDTO.getUserId(), "UserId should match after mapping");
        assertEquals(user.getEmail(), userDTO.getEmail(), "Email should match after mapping");
        assertEquals(user.getGivenName(), userDTO.getGivenName(), "GivenName should match after mapping");
        assertEquals(user.getFamilyName(), userDTO.getFamilyName(), "FamilyName should match after mapping");
        assertEquals(user.getUserRole(), userDTO.getUserRole(), "UserRole should match after mapping");
        assertEquals(1, userDTO.getAddresses().size(), "Addresses should match after mapping");
    }

    @Test
    public void mapToListDto_WhenValidUserList_ThenCorrectDtoListReturned() {
        var user1 = new User("john.doe@example.com", "John", "Doe", UserRole.OWNER);
        user1.setUserId("1");
        user1.setAddresses(List.of(new Address()));

        var user2 = new User("jane.doe@example.com", "Jane", "Doe", UserRole.OWNER);
        user2.setUserId("2");
        user2.setAddresses(List.of(new Address()));

        // Given
        List<User> users = Arrays.asList(user1, user2);

        // When
        List<UserDTO> userDTOs = mapper.mapToList(users);

        // Then
        assertEquals(users.size(), userDTOs.size(), "List sizes should match after mapping");
        for (int i = 0; i < users.size(); i++) {
            User user = users.get(i);
            UserDTO userDTO = userDTOs.get(i);

            assertEquals(user.getUserId(), userDTO.getUserId(), "UserId should match for user " + i);
            assertEquals(user.getEmail(), userDTO.getEmail(), "Email should match for user " + i);
            assertEquals(user.getGivenName(), userDTO.getGivenName(), "GivenName should match for user " + i);
            assertEquals(user.getFamilyName(), userDTO.getFamilyName(), "FamilyName should match for user " + i);
            assertEquals(user.getUserRole(), userDTO.getUserRole(), "UserRole should match for user " + i);
            assertEquals(1, userDTO.getAddresses().size(), "Addresses should match for user " + i);
        }
    }

    @Test
    public void mapToDto_WhenNullUser_ThenNullReturned() {
        assertNull(mapper.mapToDto(null), "Mapping null User should return null UserDTO");
    }

    @Test
    public void mapToListDto_WhenNullUserList_ThenNullReturned() {
        assertNull(mapper.mapToList(null), "Mapping null User list should return null UserDTO list");
    }

    @Test
    void mapToDto_WhenUserWithMinimalDetails_ThenSuccess() {
        // Given minimal user details (assuming email and userId are mandatory)
        User user = new User("minimal@example.com", null, null, null);
        user.setUserId("1");
        user.setAddresses(List.of(new Address()));

        // When
        UserDTO userDTO = mapper.mapToDto(user);

        // Then
        assertEquals("1", userDTO.getUserId(), "UserId should match for minimal details user");
        assertEquals("minimal@example.com", userDTO.getEmail(), "Email should match for minimal details user");
        assertNull(userDTO.getGivenName(), "GivenName should be null for minimal details user");
        assertNull(userDTO.getFamilyName(), "FamilyName should be null for minimal details user");
        assertNull(userDTO.getUserRole(), "UserRole should be null for minimal details user");
        assertEquals(1, userDTO.getAddresses().size(), "Addresses should match for minimal details user");
    }

    @Test
    void mapToDto_WhenUserWithNullFamilyName_ThenHandledGracefully() {
        // Given
        User userWithNullFamilyName = new User("email@example.com", "John", null, UserRole.OWNER);
        userWithNullFamilyName.setUserId("1");
        userWithNullFamilyName.setAddresses(List.of(new Address()));

        // When
        UserDTO resultDTO = mapper.mapToDto(userWithNullFamilyName);

        // Then
        assertNotNull(resultDTO, "Resulting DTO should not be null.");
        assertEquals("John", resultDTO.getGivenName(), "GivenName should match.");
        assertNull(resultDTO.getFamilyName(), "FamilyName should be null indicating graceful handling.");
        assertEquals(1, resultDTO.getAddresses().size(), "Addresses should match.");
    }
}
