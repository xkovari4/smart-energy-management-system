package cz.muni.fi.pa165.epsilon.usermanagement.rest;

import cz.muni.fi.pa165.epsilon.usermanagement.api.AddressDTO;
import cz.muni.fi.pa165.epsilon.usermanagement.data.model.UserRole;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class RegisterUserRequestTest {
    private static Validator validator;

    @BeforeAll
    public static void setupValidatorInstance() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void validateRegisterUserRequest_WhenBlankEmail_ThenValidationFails() {
        RegisterUserRequest request = new RegisterUserRequest();
        request.setEmail("");
        request.setGivenName("John");
        request.setFamilyName("Doe");
        request.setUserRole(UserRole.OWNER);
        request.setAddresses(List.of(new AddressDTO()));

        Set<?> constraintViolations = validator.validate(request);

        assertFalse(constraintViolations.isEmpty(), "Validation should fail due to blank email.");
    }

    @Test
    public void validateRegisterUserRequest_WhenBlankGivenName_ThenValidationFails() {
        RegisterUserRequest request = new RegisterUserRequest();
        request.setEmail("johndoe@gmail.com");
        request.setGivenName(""); // Intentionally blank
        request.setFamilyName("Doe");
        request.setUserRole(UserRole.OWNER);
        request.setAddresses(List.of(new AddressDTO()));

        Set<?> constraintViolations = validator.validate(request);

        assertFalse(constraintViolations.isEmpty(), "Validation should fail due to blank given name.");
    }

    @Test
    public void validateRegisterUserRequest_WhenBlankFamilyName_ThenValidationFails() {
        RegisterUserRequest request = new RegisterUserRequest();
        request.setEmail("johndoe@gmail.com");
        request.setGivenName("John");
        request.setFamilyName(""); // Intentionally blank
        request.setUserRole(UserRole.OWNER);
        request.setAddresses(List.of(new AddressDTO()));

        Set<?> constraintViolations = validator.validate(request);

        assertFalse(constraintViolations.isEmpty(), "Validation should fail due to blank family name.");
    }

    @Test
    public void validateRegisterUserRequest_WhenNullUserRole_ThenValidationFails() {
        RegisterUserRequest request = new RegisterUserRequest();
        request.setEmail("johndoe@gmail.com");
        request.setGivenName("John");
        request.setFamilyName("Doe");
        request.setUserRole(null); // Intentionally null
        request.setAddresses(List.of(new AddressDTO()));

        Set<?> constraintViolations = validator.validate(request);

        assertFalse(constraintViolations.isEmpty(), "Validation should fail due to null user role.");
    }

    @Test
    public void validateRegisterUserRequest_WhenNullAddresses_ThenValidationFails() {
        RegisterUserRequest request = new RegisterUserRequest();
        request.setEmail("johndoe@gmail.com");
        request.setGivenName("John");
        request.setFamilyName("Doe");
        request.setUserRole(UserRole.OWNER);
        request.setAddresses(null); // Intentionally null

        Set<?> constraintViolations = validator.validate(request);

        assertFalse(constraintViolations.isEmpty(), "Validation should fail due to null addresses.");
    }

    @Test
    public void validateRegisterUserRequest_WhenValidRequest_ThenValidationSucceeds() {
        RegisterUserRequest request = new RegisterUserRequest();
        request.setEmail("johndoe@gmail.com");
        request.setGivenName("John");
        request.setFamilyName("Doe");
        request.setUserRole(UserRole.OWNER);
        request.setAddresses(Collections.emptyList());

        Set<?> constraintViolations = validator.validate(request);

        assertTrue(constraintViolations.isEmpty(), "Validation should succeed for a valid request.");
    }

    @Test
    public void settersAndGetters_WhenUsed_ThenValuesMatchExpectations() {
        RegisterUserRequest request = new RegisterUserRequest();
        request.setEmail("user@example.com");
        request.setGivenName("FirstName");
        request.setFamilyName("LastName");
        request.setUserRole(UserRole.OWNER);
        request.setAddresses(List.of(new AddressDTO()));

        assertAll(
                () -> assertEquals("user@example.com", request.getEmail(), "Email should match the set value."),
                () -> assertEquals("FirstName", request.getGivenName(), "GivenName should match the set value."),
                () -> assertEquals("LastName", request.getFamilyName(), "FamilyName should match the set value."),
                () -> assertEquals(UserRole.OWNER, request.getUserRole(), "UserRole should match the set value."),
                () -> assertEquals(1, request.getAddresses().size(), "Count on addresses should be 1.")
        );

        String expectedString = "RegisterUserRequest(email=user@example.com, givenName=FirstName, familyName=LastName, userRole=OWNER, addresses=[AddressDTO(street=null, houseNumber=null, city=null, zipCode=null)])";
        assertEquals(expectedString, request.toString(), "ToString should match the expected value.");
    }
}
