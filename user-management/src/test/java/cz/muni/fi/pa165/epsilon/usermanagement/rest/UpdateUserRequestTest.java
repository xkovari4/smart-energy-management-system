package cz.muni.fi.pa165.epsilon.usermanagement.rest;

import cz.muni.fi.pa165.epsilon.usermanagement.api.AddressDTO;
import cz.muni.fi.pa165.epsilon.usermanagement.data.model.UserRole;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class UpdateUserRequestTest {

    private static Validator validator;

    @BeforeAll
    public static void setupValidatorInstance() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void validateUpdateUserRequest_WhenBlankUserId_ThenValidationFails() {
        UpdateUserRequest request = createUpdateUserRequest("", "johndoe@gmail.com", "John", "Doe", UserRole.OWNER);

        Set<?> constraintViolations = validator.validate(request);

        assertFalse(constraintViolations.isEmpty(), "Validation should fail due to blank userId.");
    }

    @Test
    public void validateUpdateUserRequest_WhenBlankEmail_ThenValidationFails() {
        UpdateUserRequest request = createUpdateUserRequest("1", "", "John", "Doe", UserRole.OWNER);

        Set<?> constraintViolations = validator.validate(request);

        assertFalse(constraintViolations.isEmpty(), "Validation should fail due to blank email.");
    }

    @Test
    public void validateUpdateUserRequest_WhenBlankGivenName_ThenValidationFails() {
        UpdateUserRequest request = createUpdateUserRequest("1", "johndoe@gmail.com", "", "Doe", UserRole.OWNER);

        Set<?> constraintViolations = validator.validate(request);

        assertFalse(constraintViolations.isEmpty(), "Validation should fail due to blank givenName.");
    }

    @Test
    public void validateUpdateUserRequest_WhenBlankFamilyName_ThenValidationFails() {
        UpdateUserRequest request = createUpdateUserRequest("1", "johndoe@gmail.com", "John", "", UserRole.OWNER);

        Set<?> constraintViolations = validator.validate(request);

        assertFalse(constraintViolations.isEmpty(), "Validation should fail due to blank familyName.");
    }

    @Test
    public void validateUpdateUserRequest_WhenNullUserRole_ThenValidationFails() {
        UpdateUserRequest request = createUpdateUserRequest("1", "johndoe@gmail.com", "John", "Doe", null);

        Set<?> constraintViolations = validator.validate(request);

        assertFalse(constraintViolations.isEmpty(), "Validation should fail due to null UserRole.");
    }

    @Test
    public void validateUpdateUserRequest_WhenNullAddresses_ThenValidationFails() {
        UpdateUserRequest request = new UpdateUserRequest();
        request.setUserId("1");
        request.setEmail("johndoe@gmail.com");
        request.setGivenName("John");
        request.setFamilyName("Doe");
        request.setUserRole(UserRole.OWNER);
        request.setAddresses(null);

        Set<?> constraintViolations = validator.validate(request);

        assertFalse(constraintViolations.isEmpty(), "Validation should fail due to null addresses.");
    }

    @Test
    public void validateUpdateUserRequest_WhenEmptyAddresses_ThenValidationSucceeds() {
        UpdateUserRequest request = createUpdateUserRequest("1", "johndoe@gmail.com", "John", "Doe", UserRole.OWNER);
        request.setAddresses(new ArrayList<>());

        Set<?> constraintViolations = validator.validate(request);

        assertTrue(constraintViolations.isEmpty(), "Validation should succeed for a request with empty addresses.");
    }

    @Test
    public void validateUpdateUserRequest_WhenValidRequest_ThenValidationSucceeds() {
        UpdateUserRequest request = createUpdateUserRequest("1", "johndoe@gmail.com", "John", "Doe", UserRole.OWNER);

        Set<?> constraintViolations = validator.validate(request);

        assertTrue(constraintViolations.isEmpty(), "Validation should succeed for a valid UpdateUserRequest.");
    }

    @Test
    public void settersAndGetters_WhenUsed_ThenValuesMatchExpectations() {
        UpdateUserRequest request = new UpdateUserRequest();
        request.setUserId("1");
        request.setEmail("updateuser@example.com");
        request.setGivenName("UpdatedFirstName");
        request.setFamilyName("UpdatedLastName");
        request.setUserRole(UserRole.OWNER);

        AddressDTO address = new AddressDTO();
        address.setStreet("Test Street");
        address.setHouseNumber("123");
        address.setCity("Test City");
        address.setZipCode("12345");
        List<AddressDTO> addresses = new ArrayList<>();
        addresses.add(address);
        request.setAddresses(addresses);

        assertAll(
                () -> assertEquals("1", request.getUserId(), "UserId should match."),
                () -> assertEquals("updateuser@example.com", request.getEmail(), "Email should match."),
                () -> assertEquals("UpdatedFirstName", request.getGivenName(), "GivenName should match."),
                () -> assertEquals("UpdatedLastName", request.getFamilyName(), "FamilyName should match."),
                () -> assertEquals(UserRole.OWNER, request.getUserRole(), "UserRole should match."),
                () -> assertEquals(1, request.getAddresses().size(), "Addresses size should match."),
                () -> assertEquals("Test Street", request.getAddresses().get(0).getStreet(), "Address street should match.")
        );
    }

    private UpdateUserRequest createUpdateUserRequest(String userId, String email, String givenName, String familyName, UserRole userRole) {
        UpdateUserRequest request = new UpdateUserRequest();
        request.setUserId(userId);
        request.setEmail(email);
        request.setGivenName(givenName);
        request.setFamilyName(familyName);
        request.setUserRole(userRole);
        request.setAddresses(new ArrayList<>());
        return request;
    }
}
