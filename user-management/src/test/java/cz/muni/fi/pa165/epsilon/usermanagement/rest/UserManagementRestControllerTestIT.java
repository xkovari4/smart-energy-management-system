package cz.muni.fi.pa165.epsilon.usermanagement.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import cz.muni.fi.pa165.epsilon.usermanagement.api.UserDTO;
import cz.muni.fi.pa165.epsilon.usermanagement.data.model.Address;
import cz.muni.fi.pa165.epsilon.usermanagement.data.model.User;
import cz.muni.fi.pa165.epsilon.usermanagement.data.model.UserRole;
import cz.muni.fi.pa165.epsilon.usermanagement.data.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@WithMockUser(authorities = {"SCOPE_test_write", "SCOPE_test_read", "SCOPE_test_1", "SCOPE_test_2"})
@AutoConfigureMockMvc
@Transactional
class UserManagementRestControllerTestIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;

    private static User user1;
    private static User user2;

    @BeforeEach
    void setUp() {
        userRepository.deleteAll();

        user1 = new User();
        user1.setEmail("test@example.com");
        user1.setGivenName("John");
        user1.setFamilyName("Doe");
        user1.setUserRole(UserRole.OWNER);

        Address address1 = new Address();
        address1.setCity("Test City 1");
        address1.setStreet("Test Street 1");
        address1.setHouseNumber("101");
        address1.setZipCode("12345");
        address1.setUser(user1);

        user1.setAddresses(List.of(address1));

        user2 = new User();
        user2.setEmail("test2@example.com");
        user2.setGivenName("Jane");
        user2.setFamilyName("Doe");
        user2.setUserRole(UserRole.OWNER);

        Address address2 = new Address();
        address2.setCity("Test City 2");
        address2.setStreet("Test Street 2");
        address2.setHouseNumber("102");
        address2.setZipCode("12346");
        address2.setUser(user2);

        user2.setAddresses(List.of(address2));

        userRepository.save(user1);
        userRepository.save(user2);
    }

    @Test
    void findById_WhenUserPresent_ReturnsUser() throws Exception {
        String responseJson = mockMvc.perform(get("/user-management/users/{userId}", user1.getUserId())
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        UserDTO resultUser = objectMapper.readValue(responseJson, UserDTO.class);

        assertThat(resultUser.getEmail()).isEqualTo(user1.getEmail());
        assertThat(resultUser.getUserId()).isEqualTo(user1.getUserId());
    }

    @Test
    void listAll_WhenUsersPresent_ReturnsUsers() throws Exception {
        String responseJson = mockMvc.perform(get("/user-management/users")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        TypeReference<List<UserDTO>> listType = new TypeReference<>() {};
        List<UserDTO> resultList = objectMapper.readValue(responseJson, listType);

        assertThat(resultList).hasSize(2);
        assertThat(resultList.get(0).getUserId()).isEqualTo(user1.getUserId());
        assertThat(resultList.get(1).getUserId()).isEqualTo(user2.getUserId());
    }
}
