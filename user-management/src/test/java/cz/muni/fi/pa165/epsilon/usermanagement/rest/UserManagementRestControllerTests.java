package cz.muni.fi.pa165.epsilon.usermanagement.rest;

import cz.muni.fi.pa165.epsilon.usermanagement.api.UserDTO;
import cz.muni.fi.pa165.epsilon.usermanagement.data.model.UserRole;
import cz.muni.fi.pa165.epsilon.usermanagement.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.epsilon.usermanagement.exceptions.ValidationException;
import cz.muni.fi.pa165.epsilon.usermanagement.facade.UserManagementFacade;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@WithMockUser(authorities = {"SCOPE_test_write", "SCOPE_test_read", "SCOPE_test_1", "SCOPE_test_2"})
@AutoConfigureMockMvc
@ExtendWith(MockitoExtension.class)
public class UserManagementRestControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserManagementFacade userManagementFacade;

    @Test
    void findById_WhenUserExists_ThenReturnUser() throws Exception {
        UserDTO userDTO = new UserDTO();
        userDTO.setUserId("1");
        userDTO.setEmail("johndoe@gmail.com");
        userDTO.setGivenName("John");
        userDTO.setFamilyName("Doe");

        when(userManagementFacade.findById("1")).thenReturn(userDTO);

        mockMvc.perform(get("/user-management/users/1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.userId").value("1"))
                .andExpect(jsonPath("$.email").value("johndoe@gmail.com"))
                .andExpect(jsonPath("$.givenName").value("John"))
                .andExpect(jsonPath("$.familyName").value("Doe"));
    }

    @Test
    void findById_WhenUserDoesNotExist_ThenReturn404() throws Exception {
        when(userManagementFacade.findById(anyString())).thenThrow(new ResourceNotFoundException("User not found"));

        mockMvc.perform(get("/user-management/users/1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status").value("NOT_FOUND"))
                .andExpect(jsonPath("$.message").value("User not found"));
    }

    @Test
    void listAll_WhenUsersExist_ThenReturnUserList() throws Exception {
        UserDTO user1 = new UserDTO();
        user1.setUserId("1");
        user1.setEmail("user1@example.com");
        user1.setGivenName("User1");
        user1.setFamilyName("One");
        UserDTO user2 = new UserDTO();
        user2.setUserId("2");
        user2.setEmail("user2@example.com");
        user2.setGivenName("User2");
        user2.setFamilyName("Two");

        List<UserDTO> users = Arrays.asList(user1, user2);

        when(userManagementFacade.listAll()).thenReturn(users);

        mockMvc.perform(get("/user-management/users")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.size()").value(2))
                .andExpect(jsonPath("$[0].userId").value("1"))
                .andExpect(jsonPath("$[1].userId").value("2"));
    }

    @Test
    void registerUser_WhenValidRequest_ThenCreateUser() throws Exception {
        UserDTO newUser = new UserDTO();
        newUser.setUserId("1");
        newUser.setEmail("newuser@example.com");
        newUser.setGivenName("New");
        newUser.setFamilyName("User");

        RegisterUserRequest registerUserRequest = new RegisterUserRequest();
        registerUserRequest.setEmail("newuser@example.com");
        registerUserRequest.setGivenName("New");
        registerUserRequest.setFamilyName("User");
        registerUserRequest.setUserRole(UserRole.OWNER);
        registerUserRequest.setAddresses(Collections.emptyList());

        when(userManagementFacade.registerUser(any(RegisterUserRequest.class))).thenReturn(newUser);

        mockMvc.perform(post("/user-management/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"email\":\"newuser@example.com\",\"givenName\":\"New\",\"familyName\":\"User\",\"userRole\":\"OWNER\",\"addresses\":[]}")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.userId").value("1"))
                .andExpect(jsonPath("$.email").value("newuser@example.com"))
                .andExpect(jsonPath("$.givenName").value("New"))
                .andExpect(jsonPath("$.familyName").value("User"));
    }

    @Test
    void getRegisterUser_WhenValidationFailsOnEmailWithoutAtCharacter_Return400() throws Exception {
        mockMvc.perform(post("/user-management/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"email\":\"invalid-email.com\",\"givenName\":\"jozo\",\"familyName\":\"some-family\",\"userRole\":\"OWNER\",\"addresses\":[]}")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status").value("BAD_REQUEST"))
                .andExpect(jsonPath("$.message").value("FieldError: email; must be a well-formed email address"));
    }

    @Test
    void registerUser_WhenValidationFailOnUserWithoutRequiredFields_ThenReturn400() throws Exception {
        mockMvc.perform(post("/user-management/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"email\":\"\",\"givenName\":\"\",\"familyName\":\"\",\"userRole\":\"OWNER\",\"addresses\":[]}")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status").value("BAD_REQUEST"));
    }

    @Test
    void registerUser_WhenValidationFails_ThenReturn400() throws Exception {
        mockMvc.perform(post("/user-management/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"email\":\"valid@email\",\"givenName\":\"jozo\",\"familyName\":\"\",\"userRole\":\"OWNER\",\"addresses\":[]}")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status").value("BAD_REQUEST"))
                .andExpect(jsonPath("$.message").value("FieldError: familyName; must not be blank"));
    }

    @Test
    void updateUser_WhenValidRequest_ThenUpdateUser() throws Exception {
        UserDTO updatedUser = new UserDTO();
        updatedUser.setUserId("1");
        updatedUser.setEmail("updated@example.com");
        updatedUser.setGivenName("Updated");
        updatedUser.setFamilyName("User");

        UpdateUserRequest updateUserRequest = new UpdateUserRequest();
        updateUserRequest.setUserId("1");
        updateUserRequest.setEmail("updated@example.com");
        updateUserRequest.setGivenName("Updated");
        updateUserRequest.setFamilyName("User");
        updateUserRequest.setUserRole(UserRole.OWNER);
        updateUserRequest.setAddresses(Collections.emptyList());

        when(userManagementFacade.updateUser(any(UpdateUserRequest.class))).thenReturn(updatedUser);

        mockMvc.perform(put("/user-management/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"userId\":\"1\",\"email\":\"updated@example.com\",\"givenName\":\"Updated\",\"familyName\":\"User\",\"userRole\":\"OWNER\",\"addresses\":[]}")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.userId").value("1"))
                .andExpect(jsonPath("$.email").value("updated@example.com"))
                .andExpect(jsonPath("$.givenName").value("Updated"))
                .andExpect(jsonPath("$.familyName").value("User"));
    }

    @Test
    void updateUser_WhenValidationFails_ThenReturn400() throws Exception {
        UpdateUserRequest updateUserRequest = new UpdateUserRequest();
        updateUserRequest.setUserId("1");
        updateUserRequest.setEmail("invalid-email");
        updateUserRequest.setGivenName("jozo");
        updateUserRequest.setFamilyName("");
        updateUserRequest.setUserRole(UserRole.OWNER);
        updateUserRequest.setAddresses(Collections.emptyList());

        doThrow(new ValidationException("FieldError: familyName; must not be blank")).when(userManagementFacade).updateUser(any(UpdateUserRequest.class));

        mockMvc.perform(put("/user-management/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"userId\":\"1\",\"email\":\"invalid-email\",\"givenName\":\"jozo\",\"familyName\":\"\",\"userRole\":\"OWNER\",\"addresses\":[]}")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status").value("BAD_REQUEST"))
                .andExpect(jsonPath("$.message").value("FieldError: familyName; must not be blank"));
    }

    @Test
    void removeUser_WhenUserExists_ThenRemoveUser() throws Exception {
        UserDTO removedUser = new UserDTO();
        removedUser.setUserId("1");
        removedUser.setEmail("removed@example.com");
        removedUser.setGivenName("Removed");
        removedUser.setFamilyName("User");

        when(userManagementFacade.removeUser(anyString())).thenReturn(removedUser);

        mockMvc.perform(delete("/user-management/users/1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    void removeUser_WhenUserDoesNotExist_ThenReturn404() throws Exception {
        doThrow(new ResourceNotFoundException("User not found")).when(userManagementFacade).removeUser(anyString());

        mockMvc.perform(delete("/user-management/users/1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status").value("NOT_FOUND"))
                .andExpect(jsonPath("$.message").value("User not found"));
    }
}
