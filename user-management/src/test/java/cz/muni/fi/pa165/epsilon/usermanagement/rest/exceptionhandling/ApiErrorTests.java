package cz.muni.fi.pa165.epsilon.usermanagement.rest.exceptionhandling;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.*;

public class ApiErrorTests {

    @Test
    public void createApiError_WhenGivenCurrentTimestamp_ThenAllFieldsAreCorrect() {
        HttpStatus status = HttpStatus.NOT_FOUND;
        String message = "Resource not found";
        String path = "/user-management/users/1";
        ApiError apiError = new ApiError(status, message, path);

        assertAll("Verifying ApiError with current timestamp",
                () -> assertEquals(status, apiError.getStatus(), "Status should match"),
                () -> assertEquals(message, apiError.getMessage(), "Message should match"),
                () -> assertEquals(path, apiError.getPath(), "Path should match"),
                () -> assertNotNull(apiError.getTimestamp(), "Timestamp should not be null")
        );
    }

    @Test
    public void createApiError_WhenGivenSpecificTimestamp_ThenAllFieldsMatchIncludingTimestamp() {
        LocalDateTime timestamp = LocalDateTime.now(Clock.systemUTC());
        HttpStatus status = HttpStatus.BAD_REQUEST;
        String message = "Validation failed";
        String path = "/user-management/users";
        ApiError apiError = new ApiError(timestamp, status, message, path);

        assertAll("Verifying ApiError with specific timestamp",
                () -> assertEquals(timestamp, apiError.getTimestamp(), "Timestamp should match"),
                () -> assertEquals(status, apiError.getStatus(), "Status should match"),
                () -> assertEquals(message, apiError.getMessage(), "Message should match"),
                () -> assertEquals(path, apiError.getPath(), "Path should match")
        );
    }

    @Test
    public void setApiErrorFields_WhenUsingSetters_ThenAllFieldsUpdatedCorrectly() {
        LocalDateTime timestamp = LocalDateTime.now(Clock.systemUTC());
        HttpStatus status = HttpStatus.UNAUTHORIZED;
        String message = "Unauthorized access";
        String path = "/user-management/secure";

        ApiError apiError = new ApiError(HttpStatus.OK, "", "");
        apiError.setTimestamp(timestamp);
        apiError.setStatus(status);
        apiError.setMessage(message);
        apiError.setPath(path);

        assertAll("Verifying ApiError after using setters",
                () -> assertEquals(timestamp, apiError.getTimestamp(), "Timestamp should match"),
                () -> assertEquals(status, apiError.getStatus(), "Status should match"),
                () -> assertEquals(message, apiError.getMessage(), "Message should match"),
                () -> assertEquals(path, apiError.getPath(), "Path should match")
        );
    }

    @Test
    public void toString_WhenCalled_ThenReturnsExpectedString() {
        Clock systemUTC = Clock.systemUTC();
        LocalDateTime timestamp = LocalDateTime.now(systemUTC);
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        String message = "Unexpected error";
        String path = "/user-management/error";
        ApiError apiError = new ApiError(timestamp, status, message, path);

        // Custom formatter to ensure consistent nanoseconds display (up to 9 digits)
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSSS");
        String timestampFormatted = timestamp.format(formatter);

        String expectedToString = String.format("ApiError{timestamp=%s, status=%s, message='%s', path='%s'}",
                timestampFormatted, status, message, path);

        assertEquals(expectedToString, apiError.toString(), "toString should return the expected string representation.");
    }

    @Test
    public void createApiError_WhenUsingParameterizedConstructor_ThenAllFieldsMatch() {
        HttpStatus status = HttpStatus.CONFLICT;
        String message = "Conflict error";
        String path = "/user-management/conflict";
        ApiError apiError = new ApiError(status, message, path);

        assertAll("Verifying ApiError with specific values",
                () -> assertNotNull(apiError.getTimestamp(), "Timestamp should not be null"),
                () -> assertEquals(status, apiError.getStatus(), "Status should match"),
                () -> assertEquals(message, apiError.getMessage(), "Message should match"),
                () -> assertEquals(path, apiError.getPath(), "Path should match")
        );
    }
}
