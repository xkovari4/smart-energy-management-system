package cz.muni.fi.pa165.epsilon.usermanagement.rest.exceptionhandling;

import cz.muni.fi.pa165.epsilon.usermanagement.data.model.UserRole;
import cz.muni.fi.pa165.epsilon.usermanagement.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.epsilon.usermanagement.exceptions.ValidationException;
import cz.muni.fi.pa165.epsilon.usermanagement.facade.UserManagementFacade;
import cz.muni.fi.pa165.epsilon.usermanagement.rest.RegisterUserRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@WithMockUser(authorities = {"SCOPE_test_write", "SCOPE_test_read", "SCOPE_test_1", "SCOPE_test_2"})
@AutoConfigureMockMvc
@ExtendWith(MockitoExtension.class)
public class RestExceptionHandlingTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserManagementFacade userManagementFacade; // Mocking the facade/service layer instead

    @Test
    void getUserById_WhenResourceNotFound_ThenRespondWith404() throws Exception {
        doThrow(new ResourceNotFoundException("Resource not found")).when(userManagementFacade).findById(anyString());

        mockMvc.perform(get("/user-management/users/1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("{\"status\":\"NOT_FOUND\",\"message\":\"Resource not found\"}"));
    }

    @Test
    void registerUser_WhenValidationException_ThenRespondWith400() throws Exception {
        // Set up invalid RegisterUserRequest
        RegisterUserRequest registerUserRequest = new RegisterUserRequest();
        registerUserRequest.setEmail("");
        registerUserRequest.setGivenName("Test");
        registerUserRequest.setFamilyName("User");
        registerUserRequest.setUserRole(UserRole.OWNER);
        registerUserRequest.setAddresses(Collections.emptyList());

        doThrow(new ValidationException("FieldError: email; must not be blank")).when(userManagementFacade).registerUser(any(RegisterUserRequest.class));

        String invalidJson = "{\"email\":\"\",\"givenName\":\"Test\",\"familyName\":\"User\",\"userRole\":\"OWNER\",\"addresses\":[]}";

        mockMvc.perform(post("/user-management/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(invalidJson))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("{\"status\":\"BAD_REQUEST\",\"message\":\"FieldError: email; must not be blank\"}"));
    }

    @Test
    void listAllUsers_WhenInternalServerError_ThenRespondWith500() throws Exception {
        doThrow(new RuntimeException("Internal error")).when(userManagementFacade).listAll();

        mockMvc.perform(get("/user-management/users")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("{\"status\":\"INTERNAL_SERVER_ERROR\",\"message\":\"Internal error\"}"));
    }

    @Test
    void getUserById_WhenUnexpectedError_ThenRespondWith500() throws Exception {
        doThrow(new NullPointerException("Unexpected error")).when(userManagementFacade).findById(anyString());

        mockMvc.perform(get("/user-management/users/1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("{\"status\":\"INTERNAL_SERVER_ERROR\",\"message\":\"Unexpected error\"}"));
    }
}
