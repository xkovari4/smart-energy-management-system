package cz.muni.fi.pa165.epsilon.usermanagement.service;

import cz.muni.fi.pa165.epsilon.usermanagement.api.UserDTO;
import cz.muni.fi.pa165.epsilon.usermanagement.data.model.UserRole;
import cz.muni.fi.pa165.epsilon.usermanagement.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.epsilon.usermanagement.facade.UserManagementFacade;
import cz.muni.fi.pa165.epsilon.usermanagement.rest.RegisterUserRequest;
import cz.muni.fi.pa165.epsilon.usermanagement.rest.UpdateUserRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Collections;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@WithMockUser(authorities = {"SCOPE_test_write", "SCOPE_test_read", "SCOPE_test_1", "SCOPE_test_2"})
@AutoConfigureMockMvc
public class UserManagementRestControllerWebMvcTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserManagementFacade userManagementFacade;

    @Test
    public void findById_WhenValidUserId_ThenReturnsUser() throws Exception {
        UserDTO userDTO = new UserDTO();
        userDTO.setUserId("1");
        userDTO.setEmail("johndoe@gmail.com");
        userDTO.setGivenName("John");
        userDTO.setFamilyName("Doe");

        when(userManagementFacade.findById("1")).thenReturn(userDTO);

        mockMvc.perform(get("/user-management/users/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.userId").value("1"))
                .andExpect(jsonPath("$.email").value("johndoe@gmail.com"))
                .andExpect(jsonPath("$.givenName").value("John"))
                .andExpect(jsonPath("$.familyName").value("Doe"));

        verify(userManagementFacade, times(1)).findById("1");
    }

    @Test
    public void findById_WhenNonExistentUserId_ThenReturnsNotFound() throws Exception {
        String userId = "non-existent-user-id";
        when(userManagementFacade.findById(userId)).thenThrow(new ResourceNotFoundException("User not found"));

        mockMvc.perform(get("/user-management/users/{userId}", userId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        verify(userManagementFacade, times(1)).findById(userId);
    }

    @Test
    public void removeUser_WhenNonExistentUserId_ThenReturnsNotFound() throws Exception {
        String userId = "non-existent-user-id";
        when(userManagementFacade.removeUser(userId)).thenThrow(new ResourceNotFoundException("User not found"));

        mockMvc.perform(delete("/user-management/users/{userId}", userId)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        verify(userManagementFacade, times(1)).removeUser(userId);
    }

    @Test
    public void listAllUsers_WhenUsersAvailable_ThenSuccess() throws Exception {
        UserDTO user1 = new UserDTO();
        user1.setUserId("1");
        user1.setEmail("user1@example.com");
        user1.setGivenName("User1");
        user1.setFamilyName("One");
        UserDTO user2 = new UserDTO();
        user2.setUserId("2");
        user2.setEmail("user2@example.com");
        user2.setGivenName("User2");
        user2.setFamilyName("Two");

        when(userManagementFacade.listAll()).thenReturn(Arrays.asList(user1, user2));

        mockMvc.perform(get("/user-management/users")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(2))
                .andExpect(jsonPath("$[0].userId").value("1"))
                .andExpect(jsonPath("$[1].userId").value("2"));

        verify(userManagementFacade, times(1)).listAll();
    }

    @Test
    public void listAllUsers_WhenNoUsersAvailable_ThenEmptyList() throws Exception {
        when(userManagementFacade.listAll()).thenReturn(Arrays.asList());

        mockMvc.perform(get("/user-management/users")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(0));

        verify(userManagementFacade, times(1)).listAll();
    }

    @Test
    void registerUser_WhenValidUserDetails_ThenSuccess() throws Exception {
        UserDTO newUser = new UserDTO();
        newUser.setUserId("1");
        newUser.setEmail("newuser@example.com");
        newUser.setGivenName("New");
        newUser.setFamilyName("User");

        RegisterUserRequest registerUserRequest = new RegisterUserRequest();
        registerUserRequest.setEmail("newuser@example.com");
        registerUserRequest.setGivenName("New");
        registerUserRequest.setFamilyName("User");
        registerUserRequest.setUserRole(UserRole.OWNER);
        registerUserRequest.setAddresses(Collections.emptyList());

        when(userManagementFacade.registerUser(any(RegisterUserRequest.class))).thenReturn(newUser);

        mockMvc.perform(post("/user-management/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"email\":\"newuser@example.com\",\"givenName\":\"New\",\"familyName\":\"User\",\"userRole\":\"OWNER\",\"addresses\":[]}")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.userId").value("1"));

        verify(userManagementFacade, times(1)).registerUser(any(RegisterUserRequest.class));
    }

    @Test
    void registerUser_WhenInvalidUserDetails_ThenValidationError() throws Exception {
        RegisterUserRequest registerUserRequest = new RegisterUserRequest();
        registerUserRequest.setEmail("user@example.com");
        registerUserRequest.setGivenName("John");
        registerUserRequest.setFamilyName("");
        registerUserRequest.setUserRole(UserRole.OWNER);
        registerUserRequest.setAddresses(Collections.emptyList());

        mockMvc.perform(post("/user-management/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"email\":\"user@example.com\",\"givenName\":\"John\",\"familyName\":\"\",\"userRole\":\"OWNER\"}"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("FieldError: familyName; must not be blank"))
                .andExpect(jsonPath("$.status").value("BAD_REQUEST"));

        verify(userManagementFacade, never()).registerUser(any(RegisterUserRequest.class));
    }

    @Test
    void updateUser_WhenValidUserDetails_ThenSuccess() throws Exception {
        UserDTO updatedUser = new UserDTO();
        updatedUser.setUserId("1");
        updatedUser.setEmail("updated@example.com");
        updatedUser.setGivenName("Updated");
        updatedUser.setFamilyName("User");

        UpdateUserRequest updateUserRequest = new UpdateUserRequest();
        updateUserRequest.setUserId("1");
        updateUserRequest.setEmail("updated@example.com");
        updateUserRequest.setGivenName("Updated");
        updateUserRequest.setFamilyName("User");
        updateUserRequest.setUserRole(UserRole.OWNER);
        updateUserRequest.setAddresses(Collections.emptyList());

        when(userManagementFacade.updateUser(any(UpdateUserRequest.class))).thenReturn(updatedUser);

        mockMvc.perform(put("/user-management/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"userId\":\"1\",\"email\":\"updated@example.com\",\"givenName\":\"Updated\",\"familyName\":\"User\",\"userRole\":\"OWNER\",\"addresses\":[]}")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.email").value("updated@example.com"));

        verify(userManagementFacade, times(1)).updateUser(any(UpdateUserRequest.class));
    }

    @Test
    void updateUser_WhenInvalidUserDetails_ThenValidationError() throws Exception {
        UpdateUserRequest updateUserRequest = new UpdateUserRequest();
        updateUserRequest.setUserId("1");
        updateUserRequest.setEmail("updated@example.com");
        updateUserRequest.setGivenName("");
        updateUserRequest.setFamilyName("User");
        updateUserRequest.setUserRole(UserRole.OWNER);
        updateUserRequest.setAddresses(Collections.emptyList());

        mockMvc.perform(put("/user-management/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"userId\":\"1\",\"email\":\"updated@example.com\",\"givenName\":\"\",\"familyName\":\"User\",\"userRole\":\"OWNER\"}"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("FieldError: givenName; must not be blank"))
                .andExpect(jsonPath("$.status").value("BAD_REQUEST"));

        verify(userManagementFacade, never()).updateUser(any(UpdateUserRequest.class));
    }

    @Test
    void updateUser_WhenNonExistentUserId_ThenReturnsNotFound() throws Exception {
        String userId = "non-existent-user-id";

        UpdateUserRequest updateUserRequest = new UpdateUserRequest();
        updateUserRequest.setUserId(userId);
        updateUserRequest.setEmail("updated@example.com");
        updateUserRequest.setGivenName("Updated");
        updateUserRequest.setFamilyName("User");
        updateUserRequest.setUserRole(UserRole.OWNER);
        updateUserRequest.setAddresses(Collections.emptyList());

        when(userManagementFacade.updateUser(any(UpdateUserRequest.class))).thenThrow(new ResourceNotFoundException("User not found"));

        mockMvc.perform(put("/user-management/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"userId\":\"non-existent-user-id\",\"email\":\"updated@example.com\",\"givenName\":\"Updated\",\"familyName\":\"User\",\"userRole\":\"OWNER\",\"addresses\":[]}")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        verify(userManagementFacade, times(1)).updateUser(any(UpdateUserRequest.class));
    }

    @Test
    public void removeUser_WhenValidUserId_ThenSuccess() throws Exception {
        mockMvc.perform(delete("/user-management/users/{userId}", "1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        verify(userManagementFacade, times(1)).removeUser("1");
    }
}
