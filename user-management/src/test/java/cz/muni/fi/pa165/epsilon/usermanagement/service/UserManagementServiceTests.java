package cz.muni.fi.pa165.epsilon.usermanagement.service;

import cz.muni.fi.pa165.epsilon.usermanagement.data.model.Address;
import cz.muni.fi.pa165.epsilon.usermanagement.data.model.User;
import cz.muni.fi.pa165.epsilon.usermanagement.data.model.UserRole;
import cz.muni.fi.pa165.epsilon.usermanagement.data.repository.AddressRepository;
import cz.muni.fi.pa165.epsilon.usermanagement.data.repository.UserRepository;
import cz.muni.fi.pa165.epsilon.usermanagement.exceptions.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserManagementServiceTests {

    @Mock
    private UserRepository userRepository;

    @Mock
    private AddressRepository addressRepository;

    @InjectMocks
    private UserManagementService userManagementService;

    @Test
    void registerUser_WhenValidUserDetails_ThenSuccess() {
        String email = "test@example.com";
        String givenName = "Test";
        String familyName = "User";
        UserRole userRole = UserRole.OWNER;
        List<Address> addresses = List.of(new Address());
        User user = new User(email, givenName, familyName, userRole);
        user.setAddresses(addresses);

        when(userRepository.save(any(User.class))).thenReturn(user);

        User registeredUser = userManagementService.registerUser(user);

        assertNotNull(registeredUser);
        assertEquals(email, registeredUser.getEmail());

        verify(userRepository, times(1)).save(any(User.class));
    }

    @Test
    void registerUser_WhenSaveThrowsException_ThenThrowException() {
        String email = "test@example.com";
        String givenName = "Test";
        String familyName = "User";
        UserRole userRole = UserRole.OWNER;
        List<Address> addresses = List.of(new Address());
        User user = new User(email, givenName, familyName, userRole);
        user.setAddresses(addresses);

        when(userRepository.save(any(User.class))).thenThrow(new RuntimeException("Save failed"));

        assertThrows(RuntimeException.class,
                () -> userManagementService.registerUser(user),
                "Expecting RuntimeException for save failure"
        );

        verify(userRepository, times(1)).save(any(User.class));
    }

    @Test
    void updateUser_WhenExistingUserId_ThenSuccess() {
        String userId = UUID.randomUUID().toString();
        String email = "updated@example.com";
        String givenName = "Updated";
        String familyName = "User";
        UserRole userRole = UserRole.OWNER;
        List<Address> addresses = List.of(new Address());
        User updatedUser = new User(email, givenName, familyName, userRole);
        updatedUser.setUserId(userId);
        updatedUser.setAddresses(addresses);

        when(userRepository.findById(userId)).thenReturn(Optional.of(updatedUser));
        when(userRepository.save(any(User.class))).thenReturn(updatedUser);

        User result = userManagementService.updateUser(updatedUser);

        assertEquals(email, result.getEmail());

        verify(userRepository, times(1)).save(any(User.class));
    }

    @Test
    void updateUser_WhenNonExistentUserId_ThenExpectResourceNotFoundException() {
        String userId = UUID.randomUUID().toString();
        User updatedUser = new User();
        updatedUser.setUserId(userId);

        when(userRepository.findById(userId)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class,
                () -> userManagementService.updateUser(updatedUser),
                "Expecting ResourceNotFoundException for non-existent user ID"
        );

        verify(userRepository, never()).save(any(User.class));
    }

    @Test
    void removeUser_WhenExistingUserId_ThenSuccess() {
        String userId = UUID.randomUUID().toString();
        User user = new User("test@example.com", "Test", "User", UserRole.OWNER);
        user.setUserId(userId);

        when(userRepository.findById(userId)).thenReturn(Optional.of(user));

        User removedUser = userManagementService.removeUser(userId);

        assertEquals(userId, removedUser.getUserId());

        verify(userRepository).deleteById(userId);
    }

    @Test
    void removeUser_WhenNonExistentUserId_ThenExpectResourceNotFoundException() {
        when(userRepository.findById(anyString())).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class,
                () -> userManagementService.removeUser(UUID.randomUUID().toString()),
                "Expecting ResourceNotFoundException for non-existent user ID"
        );
    }

    @Test
    void removeUser_WhenUserHasAddresses_ThenAddressesDeleted() {
        String userId = UUID.randomUUID().toString();
        Address address1 = new Address();
        address1.setAddressId(UUID.randomUUID().toString());
        Address address2 = new Address();
        address2.setAddressId(UUID.randomUUID().toString());
        User user = new User("test@example.com", "Test", "User", UserRole.OWNER);
        user.setAddresses(Arrays.asList(address1, address2));
        user.setUserId(userId);

        when(userRepository.findById(userId)).thenReturn(Optional.of(user));

        User removedUser = userManagementService.removeUser(userId);

        assertEquals(userId, removedUser.getUserId());

        verify(userRepository, times(1)).deleteById(userId);
    }

    @Test
    void findById_WhenValidUserId_ThenUserReturned() {
        String userId = UUID.randomUUID().toString();
        User user = new User("test@example.com", "Test", "User", UserRole.OWNER);
        user.setUserId(userId);

        when(userRepository.findById(userId)).thenReturn(Optional.of(user));

        User foundUser = userManagementService.findById(userId);

        assertNotNull(foundUser);
        assertEquals(userId, foundUser.getUserId());

        verify(userRepository, times(1)).findById(userId);
    }

    @Test
    void findById_WhenNonExistentUserId_ThenExpectResourceNotFoundException() {
        when(userRepository.findById(anyString())).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class,
                () -> userManagementService.findById(UUID.randomUUID().toString()),
                "Expecting ResourceNotFoundException for non-existent user ID"
        );
    }

    @Test
    void listAll_WhenUsersExist_ThenUserListReturned() {
        List<User> users = Arrays.asList(
                new User("user1@example.com", "User1", "One", UserRole.OWNER),
                new User("user2@example.com", "User2", "Two", UserRole.COMPANY)
        );

        when(userRepository.findAll()).thenReturn(users);

        List<User> result = userManagementService.listAll();

        assertFalse(result.isEmpty(), "Users list should not be empty");
        assertEquals(2, result.size(), "Users list should contain 2 users");

        verify(userRepository, times(1)).findAll();
    }

    @Test
    void listAll_WhenNoUsersExist_ThenEmptyListReturned() {
        when(userRepository.findAll()).thenReturn(List.of());

        List<User> result = userManagementService.listAll();

        assertTrue(result.isEmpty(), "Users list should be empty");

        verify(userRepository, times(1)).findAll();
    }
}
